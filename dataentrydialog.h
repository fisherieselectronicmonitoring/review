#ifndef DATAENTRYDIALOG_H
#define DATAENTRYDIALOG_H

#include <QDialog>
#include <QValidator>


#include "dataentryeventdata.h"
#include "dataentrywidgetwrapper.h"
#include "dataentryfieldvalue.h"


namespace Ui {
class DataEntryDialog;
}

class DataEntryEventType;

class DataEntryDialog : public QDialog
{
    Q_OBJECT

public:
    enum class Mode
      {
         Create,
         Update
      };

    explicit DataEntryDialog(QWidget *parent = 0);
    ~DataEntryDialog();

    void addMultiSelectControl(int, QString, QString, bool, float, bool, QList<QPair<QString, QString>>, QList<QString>);
    void addSelectControl(int, QString, QString, bool, float, bool, QList<QPair<QString, QString>>, QString);
    void addDateTimeControl(int,  QString, QString, bool, float, bool);
    void addTextControl(int, QString, QString, bool, float, bool, int, QString = "", int = 0, QValidator * = 0);
    void addTextBoxControl(int, QString, QString, bool, float, bool, QString = "", int = 0, QValidator * = 0);

    void generateForm();

    void setControlValue(int, QVariant);

    DataEntryEventType *eventType;
    DataEntryEventData *currentEventData;


    //TODO make private when possible
    //all controls on this form
    QList<DataEntryWidgetWrapper *> controls;
    //list of pointers to requred controls
    QList<DataEntryWidgetWrapper *> requiredControls;

public slots:
    void initialize(DataEntryEventData*, Mode, bool = false, QList<DataEntryFieldValue> = QList<DataEntryFieldValue>());
    bool acceptDialog();
    void accept();
    void reject();


private:    
    Mode mode;
    Ui::DataEntryDialog *ui;
    QWidget* scrollAreaWidget;
    void closeEvent (QCloseEvent *);
    void addWidget(DataEntryWidgetWrapper *);
    //track the status of the previous entry
    bool lastEntrySaved;
    void toggleApplicationShortcuts(bool);

protected:
    //virtual void keyPressEvent(QKeyEvent *);
    virtual void show();
};

#endif // DATAENTRYDIALOG_H
