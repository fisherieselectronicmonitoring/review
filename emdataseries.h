#ifndef EMDATASERIES_H
#define EMDATASERIES_H

#include <QObject>
#include <QColor>
#include <QVector>
#include <QMap>

class EmDataSeries : public QObject
{
    Q_OBJECT
public:
    enum class LineType
      {
         Line,
         StepLine,
         Shade
      };


    explicit EmDataSeries(QObject *parent = 0);
    QString name;
    QColor color;
    LineType type;
    int thickness;
    QVector<double> seconds;
    QVector<double> values;
    int zOrder;
    int fieldIndex;
    double factor;
    QMap<QString, double> stringMap;
    QString expression;


    //used to sort series objects using qsort
    static const bool compareSeries(EmDataSeries* s1, EmDataSeries* s2) {
        return s2->zOrder > s1->zOrder;
    }


signals:

public slots:
};



#endif // EMDATASERIES_H
