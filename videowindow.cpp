#include <vlc/vlc.h>

#include <QFileDialog>
#include <QInputDialog>
#include <QDebug>
#include <QDesktopWidget>
#include <QPainter>
#include <QDateTime>
#include <QShortcut>

#include <VLCQtCore/Common.h>
#include <VLCQtCore/Instance.h>
#include <VLCQtCore/Media.h>
#include <VLCQtCore/MediaPlayer.h>
#include <VLCQtCore/Video.h>
#include <VLCQtCore/Enums.h>
#include <VLCQtCore/Error.h>

#include "reviewapplication.h"
#include "videowindow.h"
#include "ui_videowindow.h"
#include "constants.h"
#include "measurementwindow.h"

VideoWindow::VideoWindow(int c, QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::VideoWindow),
      media(0)
{
    ui->setupUi(this);

    playIcon = QIcon(":/images/Play.png");
    pauseIcon = QIcon(":/images/Pause.png");
    ui->btnPlay->setIcon(playIcon);
    ui->btnPlay->setIconSize(QSize(22,22));


    channel = c;



    setWindowTitle("Video");

    //setup video component
    QStringList args;
    args.append(VlcCommon::args());

    foreach(QString s, rApp->vlcAdditionalArgs)
        args.append(s);

    qDebug() << "VLC opts:" << args.join(" ");
    instance = new VlcInstance(args, this);
    player = new VlcMediaPlayer(instance);
    player->setVideoWidget(ui->video);
    ui->video->setMediaPlayer(player);
    ui->seek->setMediaPlayer(player);


    //create media

    foreach(EmDataVideoDuration *vd, rApp->emData->videosByChannel.values(channel))
    {
        VlcMedia *m = NULL;
        if(!rApp->videoFromUrl)
        {
            if(QFileInfo::exists(rApp->currentEmDataDirectory + "/" + vd->fileName))
                m = new VlcMedia(rApp->currentEmDataDirectory + "/" + vd->fileName, true, instance);
        }
        else
            m = new VlcMedia(vd->fileName, false, instance);

        //did we find anything
        if(m != NULL)
        {
            if(!rApp->vlcHwAcceleration.isEmpty())
                libvlc_media_add_option(m->core(), QString(":avcodec-hw=%1").arg(rApp->vlcHwAcceleration).toStdString().c_str());
            m->parse();
            mediaByName.insert(vd->fileName, m);
            qDebug() << "channel" << channel << "added" << vd->fileName;
        }
    }


    //TODO - icons
    connect(ui->btnSaveImage, &QPushButton::clicked, this, &VideoWindow::saveImage);
    connect(ui->btnMeasure, &QPushButton::clicked, this, &VideoWindow::measure);
    connect(ui->btnCalibrate, &QPushButton::clicked, this, &VideoWindow::calibrateCamera);
    connect(player, &VlcMediaPlayer::end, this, &VideoWindow::complete);
    connect(ui->chkLockPlayback, &QCheckBox::toggled, this, &VideoWindow::toggleLockPlayback);
    connect(ui->btnPlay, &QPushButton::clicked, this, &VideoWindow::togglePause);
    ui->video->setCursor(QCursor(Qt::ArrowCursor));

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));

    timer->start(rApp->videoWindowRefreshMillis);

    //enable or diable the measurement button
     ui->btnMeasure->setVisible(rApp->measurementModeEnabled);
     ui->btnCalibrate->setVisible(rApp->measurementModeEnabled);

     //start with video locked
     ui->chkLockPlayback->setChecked(true);
     toggleLockPlayback(true);

    //start with the video widget hidden
    hideVideo();


    leftCrop = 0;
    rightCrop = 0;
    topCrop = 0;
    bottomCrop = 0;

    filtered = false;

    broken = false;

    //for now, hide the filter
    ui->btnFilter->hide();
}

void VideoWindow::togglePause()
{
    playing = !playing;
    qDebug() << playing;
}




void VideoWindow::toggleLockPlayback(bool locked)
{    

    //qDebug() << locked;
    ui->seek->setEnabled(!locked);
    ui->btnPlay->setVisible(!locked);
    if(!locked)
        ui->lblCurrentDateTime->setStyleSheet("color: red");
}

/*
void VideoWindow::filter()
{
    //example of changing VLC filters, reconnecting to media (experimental)
    resetCrop();

    ui->setupUi(this);
    QStringList args;
    args.append(VlcCommon::args());
    if(!filtered)
    {
        //TODO get from dropdown list of checkboxable options
        filtered = true;
        args.append("--video-filter=transform --transform-type=180");

    }
    else
    {
        filtered = false;
    }

    qDebug() << "Filter VLC opts:" << args.join(" ");

    delete instance;
    instance = new VlcInstance(args, this);
    player = new VlcMediaPlayer(instance);
    media = new VlcMedia(currentFile, true, instance);
    player->setVideoWidget(ui->video);
    ui->video->setMediaPlayer(player);
    ui->seek->setMediaPlayer(player);
    player->open(media);

    //todo - move this to UI setup method, consolodate with constructor
    connect(ui->btnSaveImage, &QPushButton::clicked, this, &VideoWindow::saveImage);
    connect(ui->btnMeasure, &QPushButton::clicked, this, &VideoWindow::measure);
    connect(player, &VlcMediaPlayer::end, this, &VideoWindow::complete);
    connect(ui->chkLockPlayback, &QCheckBox::toggled, this, &VideoWindow::toggleLockPlayback);
    connect(ui->btnPlay, &QPushButton::clicked, this, &VideoWindow::togglePause);
    connect(ui->btnFilter, &QPushButton::clicked, this, &VideoWindow::filter);
    ui->video->setCursor(QCursor(Qt::ArrowCursor));

    //enable or diable the measurement button
    ui->btnMeasure->setVisible(rApp->measurementModeEnabled);

    //setup the check state
    ui->chkLockPlayback->setChecked(true);
    toggleLockPlayback(true);

}
*/

void VideoWindow::wheelEvent(QWheelEvent *event)
{

    int videoWidth = player->video()->size().width();
    int videoHeight = player->video()->size().height();

    //qDebug() << "height width" << videoHeight << videoWidth;

    //TODO constants
    float widthFactor = videoWidth * .1;
    float heightFactor = videoHeight * .1;

    if(event->delta() < 0) //zoom out based on ratio of crops
    {
        float xRatio = leftCrop + rightCrop  == 0 ? 0 : leftCrop/(leftCrop+rightCrop);
        float yRatio = topCrop + bottomCrop == 0 ? 0 : topCrop/(topCrop+bottomCrop);
        //qDebug() << "zoom OUT x y ratios" << xRatio << yRatio;
        leftCrop -= widthFactor * xRatio;
        rightCrop -= widthFactor * (1 - xRatio);
        topCrop -= heightFactor * yRatio;
        bottomCrop -= heightFactor * (1 - yRatio);

        //don't allow zoom beyond the edges of the frame
        leftCrop = limitZero(leftCrop);
        rightCrop = limitZero(rightCrop);
        topCrop = limitZero(topCrop);
        bottomCrop = limitZero(bottomCrop);
    }
    else if (leftCrop + rightCrop < videoWidth && topCrop + bottomCrop < videoHeight) //zoom in based on mouse position, but don't zoom in too far
    {
        //we can get mouse events from outside the video area so correct to inside the frame by limiting to 0-1
        float xRatio = limitZeroOne(event->x()*1.0f/ui->video->width());
        float yRatio = limitZeroOne(event->y()*1.0f/ui->video->height());
        //qDebug() << "zoom IN x y ratios" << xRatio << yRatio;
        leftCrop += widthFactor * xRatio;
        rightCrop += widthFactor * (1 - xRatio);
        topCrop += heightFactor * yRatio;
        bottomCrop += heightFactor * (1 - yRatio);
    }

    //map crops to coordinates
    int left = leftCrop;
    int top = topCrop;
    int right = videoWidth - rightCrop;
    int bottom = videoHeight - bottomCrop;

    qDebug() << "crop to coords: left right top bottom" << left << right << top << bottom;
    if(right > left && bottom > top)
        libvlc_video_set_crop_geometry(player->core(), QString("%1x%2+%3+%4").arg(right).arg(bottom).arg(left).arg(top).toStdString().c_str());

    if(!playing)
    {
       player->setTime(masterOffsetMillis);
    }

}

float VideoWindow::limitZero(float n)
{
    return n < 0 ? 0 : n;
}

float VideoWindow::limitZeroOne(float n)
{
    return n < 0 ? 0 : (n > 1 ? 1 : n);
}




VideoWindow::~VideoWindow()
{
    delete player;
    delete media;
    delete instance;
    delete ui;
}

double VideoWindow::getTime()
{
    return startSeconds + ((double)player->time())/1000;
}

bool VideoWindow::hasVout()
{
    return player->hasVout();
}


void VideoWindow::update()
{
      //first see if there is anything to do at all
      if(currentFile.isEmpty() || idle || !this->isVisible())
          return;

      if(playing)
        ui->btnPlay->setIcon(pauseIcon);
      else
        ui->btnPlay->setIcon(playIcon);

      //update player state and time display
      ui->lblCurrentDateTime->setText(currentTimeString());

      //make VLC control match our desired state
      if(!broken && playing && player->state() != Vlc::State::Playing)
      {
         player->play();
         qDebug() << "playing";
      }
      else if(!playing && player->state() != Vlc::State::Paused)
         player->pause();

      //if we aren't locked to the time line, we're done here
      if(!ui->chkLockPlayback->checkState())
          return;

      qint64 lagLead = masterOffsetMillis - player->time();

      /*
      //rolling average of laglead
      lagLeadBuffer.insert(0, lagLead);
      if(lagLeadBuffer.size() > rApp->videoSyncSamples)
          lagLeadBuffer.resize(rApp->videoSyncSamples);
      qint64 avgLagLead = 0;

      foreach(int i, lagLeadBuffer)
          avgLagLead += i;

      avgLagLead = avgLagLead/lagLeadBuffer.size();
      */


      //presuming the offset requested is inside the file, if the position just changed or latest instantanous lag lead value is too great set the time on the video
           if(rApp != NULL && masterOffsetMillis < media->duration() && (positionChanged
                   || (playing && qAbs(lagLead) > masterSpeedRatio*rApp->videoSyncPlayToleranceMillis)
                   || (!playing && qAbs(lagLead) > rApp->videoSyncPauseToleranceMillis)))
           {
               player->setTime(masterOffsetMillis);
               ui->lblCurrentDateTime->setStyleSheet("color: red");
               positionChanged = false;
           }
           else
               ui->lblCurrentDateTime->setStyleSheet("color: black");


      float newSpeed = 0;
      /*
      //adjust the speed of playback based on the lag or lead - use big steps for larger lag/leads
      if(playing && avgLagLead > Constants::PLAYING_SPEED_BIG_STEP_THRESH_MILLIS * masterSpeedRatio)
          newSpeed = masterSpeedRatio + Constants::PLAYING_SPEED_BIG_STEP * masterSpeedRatio;
      else if(playing && avgLagLead < -1 * Constants::PLAYING_SPEED_BIG_STEP_THRESH_MILLIS * masterSpeedRatio)
          newSpeed = masterSpeedRatio - Constants::PLAYING_SPEED_BIG_STEP * masterSpeedRatio;
      else if(playing && avgLagLead > Constants::PLAYING_SPEED_SMALL_STEP_THRESH_MILLIS * masterSpeedRatio)
          newSpeed = masterSpeedRatio + Constants::PLAYING_SPEED_SMALL_STEP * masterSpeedRatio;
      else if(playing && avgLagLead < -1 * Constants::PLAYING_SPEED_SMALL_STEP_THRESH_MILLIS * masterSpeedRatio)
          newSpeed = masterSpeedRatio - Constants::PLAYING_SPEED_SMALL_STEP * masterSpeedRatio;
     */
      if(rApp != NULL && playing)
      {
          newSpeed = masterSpeedRatio + masterSpeedRatio * lagLead * rApp->videoSyncStepFactor;
          newSpeed = std::roundf(newSpeed*100)/100;

          if(newSpeed > masterSpeedRatio * 1.333)
              newSpeed =  masterSpeedRatio * 1.333;
      }


     //qDebug() << currentFile << "desired speed:"  << masterSpeedRatio << "current speed:" << newSpeed << "milliseconds lag+/lead-:" << lagLead;

     if(newSpeed > 0 && libvlc_media_player_get_rate(player->core()) != newSpeed){
         libvlc_media_player_set_rate(player->core(), newSpeed);
     }

     //if we have no video or we are trying to display a time beyond the end of the file, don't try to show the video
     if(!player->hasVout() || masterOffsetMillis > media->duration())
        hideVideo();
     else
        showVideo();

     //can capture stats using media stats
     //libvlc_media_stats_t stats;
     //libvlc_media_get_stats(media->core(), &stats);
     //qDebug() << currentFile << "dropped frames:" << stats.i_lost_pictures << "input bitrate:" << stats.f_input_bitrate;


}

void VideoWindow::positionChange()
{
    positionChanged = true;
}


void VideoWindow::setIdle()
{
    pause();
    setWindowTitle("Video");
    hideVideo();
    idle = true;
    currentFile.clear();
    ui->chkLockPlayback->setChecked(true);
}

QString VideoWindow::currentTimeString()
{
    //TODO date format constants
    return QDateTime::fromMSecsSinceEpoch(startSeconds*1000+player->time()).toUTC().toString("MM/dd/yyyy hh:mm:ss Z");
}

QString VideoWindow::currentFilenameTimeString()
{
    //TODO date format constants
    return QDateTime::fromMSecsSinceEpoch(startSeconds*1000+player->time()).toUTC().toString("yyyyMMdd-hhmmssZ");
}

//TODO move array of video file references here?
void VideoWindow::setState(QString fileName, double seconds, double offsetSeconds, float speedRatio)
{
    //TODO refactor into state enum
    if(!ui->chkLockPlayback->checkState())
        return;

    idle = false;

    if(fileName != currentFile)
    {
        //qDebug() << "looking for" << fileName << "for channel" << channel;
        if(mediaByName.contains(fileName)) {
            qDebug() << "opening new file: " << fileName << " current file: " << currentFile;
            setWindowTitle(QString("Video: %1").arg(fileName));
            media = mediaByName.value(fileName);
            player->open(media);
            currentFile = fileName;
            startSeconds = seconds;
            qDebug() << currentFile << "starting at" << QDateTime::currentMSecsSinceEpoch();
            broken = false;
        }
        else
        {
             setWindowTitle(QString("Video: %1 (file missing)").arg(fileName));
             player->stop();
             broken = true;

        }
    }

    //offset is in seconds, converted to millis
    masterOffsetMillis = (offsetSeconds - startSeconds) * 1000;
    masterSpeedRatio = speedRatio;



    //qDebug() << "startSeconds: " << startSeconds;
    //qDebug() << "masterOffsetMillis: " << masterOffsetMillis;

    //if we aren't visible, don't play the file
    if(!this->isVisible())
        player->pause();

}



void VideoWindow::closeEvent (QCloseEvent* event)
{
   event->ignore();
   rApp->timeline->closeVideoChannel(channel);

}







/*
void VideoWindow::setMrl(QString mrl)
{
        media = new VlcMedia(mrl, false, instance);
        player->openOnly(media);

}

*/



void VideoWindow::play()
{
    //TODO refactor in to state enum
    if(ui->chkLockPlayback->checkState())
       playing = true;
}

void VideoWindow::pause()
{
    //TODO refactor in to state enum
    if(ui->chkLockPlayback->checkState())
       playing = false;
}

void VideoWindow::setInUse(bool active)
{
    this->setVisible(active);

}



void VideoWindow::hideVideo()
{
    ui->centralwidget->hide();
}

void VideoWindow::showVideo()
{
     ui->centralwidget->show();
}


void VideoWindow::complete()
{

    qDebug() << currentFile << "complete at" << QDateTime::currentMSecsSinceEpoch();
    setIdle();

}




void VideoWindow::measure()
{
    resetCrop();
    QString path = QString("%1/measurement_%2_%3_channel-%4.png")
            .arg(rApp->currentEmDataDirectory)
            .arg(QString::number(getTime(),'f',2).replace(".","-"))
            .arg(currentFilenameTimeString())
            .arg(channel+1);

    player->video()->takeSnapshot(path);

    rApp->measurement->initialize(path, startSeconds + player->time()/1000.0);
    rApp->measurement->show();

}

void VideoWindow::calibrateCamera()
{
    resetCrop();
    QString path = QString("%1/calibration_%2_%3_channel-%4.png")
            .arg(rApp->currentEmDataDirectory)
            .arg(QString::number(getTime(),'f',2).replace(".","-"))
            .arg(currentFilenameTimeString())
            .arg(channel+1);

    player->video()->takeSnapshot(path);

    rApp->measurement->initializeCameraCalibration(path);
    rApp->measurement->calibrateCamera();
    rApp->measurement->show();


}






void VideoWindow::resetCrop()
{
     libvlc_video_set_crop_geometry(player->core(), NULL);
     leftCrop = 0;
     rightCrop = 0;
     topCrop = 0;
     bottomCrop = 0;

     //TODO replace saveImage export with ffmpeg call
     QThread::msleep(100);
}


void VideoWindow::saveImage()
{
    resetCrop();


    QString path = QString("%1/user-image_%2_%3_channel-%4.png")
            .arg(rApp->currentEmDataDirectory)
            .arg(QString::number(getTime(),'f',2).replace(".","-"))
            .arg(currentFilenameTimeString())
            .arg(channel+1);

    player->video()->takeSnapshot(path);
    statusBar()->showMessage(QString("image saved to: %1").arg(path));
}








