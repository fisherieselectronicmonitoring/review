#include "treeviewwindow.h"
#include "ui_treeviewwindow.h"
#include "reviewapplication.h"

TreeViewWindow::TreeViewWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TreeViewWindow)
{
    ui->setupUi(this);
    setWindowTitle("Outline");

    restoreGeometry(rApp->settings->value("geometry_tree").toByteArray());

    connect(ui->treeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)),this,SLOT(itemClicked(QTreeWidgetItem*, int)));
}

TreeViewWindow::~TreeViewWindow()
{
    delete ui;
}


void TreeViewWindow::selectItem(DataEntryEventData *d)
{
    ui->treeWidget->selectionModel()->clearSelection();
    foreach(QTreeWidgetItem *w, ui->treeWidget->findItems(d->getTreeViewSummary(), Qt::MatchExactly | Qt::MatchRecursive))
        if(w->data(0,STORAGE_ID) == d->storageId)
            w->setSelected(true);

}

void TreeViewWindow::itemClicked(QTreeWidgetItem* i, int col)
{

        QVariant id = i->data(0,STORAGE_ID);
        if(id.type() == QVariant::Type::Int)
            rApp->timeline->selectDataEntryEvent(id.toInt());


}


bool TreeViewWindow::chronologicalCompare(QTreeWidgetItem *i, QTreeWidgetItem *ii)
{
    return i->data(0,TreeViewWindow::START_SECONDS) < ii->data(0,TreeViewWindow::START_SECONDS);
}


void TreeViewWindow::load()
{

    //short circut this if the window is not visible
    if(!this->isVisible())
        return;

    items.clear();
    ui->treeWidget->clear();


    foreach(DataEntryEventData *d, rApp->eventData)
        createItem(d);

    std::sort(items.begin(),items.end(),chronologicalCompare);

    foreach(QTreeWidgetItem *i, items)
        if(i->data(0, PARENT_STORAGE_ID) == -1)
        {
            ui->treeWidget->addTopLevelItem(i);
            qDebug() << "root -->" << i->text(0);
            addChildren(i);
        }

    ui->treeWidget->expandAll();


}

void TreeViewWindow::createItem(DataEntryEventData *d)
{
    QStringList desc;
    desc.append(d->getTreeViewSummary());
    QTreeWidgetItem *itm = new QTreeWidgetItem(desc,0);
    itm->setData(0,STORAGE_ID, d->storageId);
    itm->setData(0,PARENT_STORAGE_ID, getParent(d));
    itm->setData(0,START_SECONDS, d->getMarkerStartSeconds());
    items.append(itm);
}

void TreeViewWindow::addChildren(QTreeWidgetItem *p)
{

    foreach(QTreeWidgetItem* child, getChildren(p))
    {
        p->addChild(child);
        qDebug() << p->text(0) << "-->" << child->text(0);
        addChildren(child);

    }


}


QList<QTreeWidgetItem*> TreeViewWindow::getChildren(QTreeWidgetItem *d)
{
    QList<QTreeWidgetItem *> ret;

    foreach(QTreeWidgetItem *possible, items)
    {
         //qDebug() << possible->data(0,PARENT_STORAGE_ID) << d->data(0,STORAGE_ID);
        if(possible->data(0,PARENT_STORAGE_ID) == d->data(0,STORAGE_ID))
        {
            ret.append(possible);
        }
    }


    return ret;
}


int TreeViewWindow::getParent(DataEntryEventData *d)
{
    int id = -1;
    DataEntryEventData *p = d->getParent();
    if(p != NULL)
        id = p->storageId;
    return id;
}








void TreeViewWindow::updateRow(DataEntryEventData *d)
{
    /*
    ui->tblEvent->setSortingEnabled(false);

    for(int z = 0; z < ui->tblEvent->rowCount(); z++)
    {
        //check the user data in column 0
        QTableWidgetItem *i = ui->tblEvent->item(z,0);
        if(i && i->data(Qt::UserRole+1).toInt() == d->storageId)
        {
            QList<QVariant> r = d->toVariantList();
            for(int x = 0; x < r.count(); x++)
            {
                 QTableWidgetItem *itm = new QTableWidgetItem(r.at(x).toString());
                 itm->setData(Qt::UserRole, r.at(x));
                 itm->setData(Qt::UserRole+1, d->storageId);
                 ui->tblEvent->setItem(z, x, itm);
            }
            break;
        }
    }

    ui->tblEvent->setSortingEnabled(true);
    */
}




void TreeViewWindow::deleteRow(DataEntryEventData *d)
{
    /*
    ui->tblEvent->setSortingEnabled(false);

    for(int z = ui->tblEvent->rowCount(); z >= 0; z--)
    {
        QTableWidgetItem *i = ui->tblEvent->item(z,0);

        if(i && i->data(Qt::UserRole+1).toInt() == d->storageId)
        {
            ui->tblEvent->removeRow(z);
            break;
        }
    }
    ui->tblEvent->setSortingEnabled(true);
   */
}


