#include "stillimagewindow.h"
#include "ui_stillimagewindow.h"
#include "measurementscene.h"

#include <QDebug>

StillImageWindow::StillImageWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StillImageWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene();
    scene->setBackgroundBrush(QBrush(Qt::black, Qt::SolidPattern));
    view = new StillImageView();
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    view->setRenderHints(QPainter::Antialiasing);
    ui->gridLayout->addWidget(view,0,0,1,1);
    view->setScene(scene);
    //this will only work if we don't allow dragging
    view->setCursor(QCursor(Qt::CrossCursor));
    setWindowTitle("Image");

}


void StillImageWindow::setImage(QString path)
{
    if(currentPath != path)
    {
        qDebug() << "setting image to " << path;
        clear();
        currentPath = path;
        setWindowTitle("Image: " + currentPath);
        QGraphicsPixmapItem* baseImage = scene->addPixmap(QPixmap(currentPath));
        scene->setSceneRect(baseImage->boundingRect());
        view->fitInView(baseImage->boundingRect(), Qt::KeepAspectRatio);
        ui->centralwidget->show();
    }
}

void StillImageWindow::clear()
{
    if(scene->items().length() > 0) {
        scene->clear();
        setWindowTitle("Image");
    }
    ui->centralwidget->hide();

}

void StillImageWindow::closeEvent (QCloseEvent* event)
{
  event->ignore();
}

StillImageWindow::~StillImageWindow()
{
    delete ui;
}
