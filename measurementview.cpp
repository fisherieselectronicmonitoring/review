#include "measurementview.h"
#include <QWheelEvent>

MeasurementView::MeasurementView(QWidget *parent) : QGraphicsView(parent)
{
    setDragMode(QGraphicsView::ScrollHandDrag);
}


void MeasurementView::wheelEvent(QWheelEvent* event) {
    //zoom rate
    double factor = 1.05;
    double scaleTo = 1;
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    if(event->delta() > 0)
        scaleTo = factor;
    else if(event->delta() < 0)
        scaleTo = 1/factor;
    scale(scaleTo,scaleTo);

}


    void MeasurementView::enterEvent(QEvent *event)
    {
        QGraphicsView::enterEvent(event);
        viewport()->setCursor(Qt::CrossCursor);
    }

    void MeasurementView::mousePressEvent(QMouseEvent *event)
    {
        QGraphicsView::mousePressEvent(event);
        viewport()->setCursor(Qt::CrossCursor);
    }

    void MeasurementView::mouseReleaseEvent(QMouseEvent *event)
    {
        QGraphicsView::mouseReleaseEvent(event);
        viewport()->setCursor(Qt::CrossCursor);
    }
