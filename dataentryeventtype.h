#ifndef DATAENTRYEVENTTYPE_H
#define DATAENTRYEVENTTYPE_H

#include <QObject>
#include <QColor>
#include <QMap>

class DataEntryDialog;

class DataEntryEventType : public QObject
{
    Q_OBJECT
public:
    explicit DataEntryEventType(QObject *parent = 0);
    enum class MarkerType
      {
         Duration,
         Point
      };
    MarkerType markerType;

    enum class DurationMarkerStyle
      {
         Shade,
         Line
      };
    DurationMarkerStyle durationMarkerStyle;

    float markerTop;
    float markerBottom;
    QColor color;
    int width;
    Qt::PenStyle LineStyle;
    QString name;
    int id;
    bool parentRequired;
    int parentEventId;

    DataEntryDialog *dialog;

    QMap<int, QString> fieldNamesById;

    //multiselects
    QList<int> multiEnumeratedValueFields;

    //selects
    QList<int> singleEnumeratedValueFields;

    //datetimes
    QList<int> dateTimes;

    //for each field using enumerated values, lookup for string values by key
    QMap<int, QMap<QString, QString>> enumeratedValuesById;



signals:

public slots:
};

#endif // DATAENTRYEVENTTYPE_H
