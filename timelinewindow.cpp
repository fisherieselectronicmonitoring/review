#include "timelinewindow.h"
#include "ui_timelinewindow.h"
#include "reviewapplication.h"
#include "emdataload.h"
#include "constants.h"
#include "emdatasensordurationevent.h"
#include "databaseutility.h"
#include "databaseselectorcreatefiledialog.h"

#include <QElapsedTimer>
#include <QFileInfo>
#include <QRegularExpression>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QAction>
#include <QShortcut>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QInputDialog>
#include <QMutableListIterator>
#include <QJsonDocument>
#include <QJsonObject>

#include <vlc/vlc.h>

#include <VLCQtCore/Common.h>
#include <VLCQtCore/Instance.h>
#include <VLCQtCore/Media.h>
#include <VLCQtCore/MediaPlayer.h>

#include "inja/inja.hpp"




TimelineWindow::TimelineWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::TimelineWindow)
{
    ui->setupUi(this);

    playIcon = QIcon(":/images/Play.png");
    pauseIcon = QIcon(":/images/Pause.png");
    ui->btnPlay->setIcon(playIcon);
    ui->btnPlay->setIconSize(QSize(22,22));

    //set positions
    if(rApp->settings->contains("geometry_timeline"))
        restoreGeometry(rApp->settings->value("geometry_timeline").toByteArray());
    else
    {
        QDesktopWidget* desktop = QApplication::desktop();
        int screenWidth = desktop->width();
        int screenHeight = desktop->height();
        int x = (screenWidth - WIDTH) / 2;
        int y = (screenHeight - HEIGHT) / 2;
        this->resize(WIDTH, HEIGHT);
        this->move(x, y);
    }

    //set opactiy 50%
    //TODO add control
    //this->setWindowOpacity(.5);

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &TimelineWindow::update);

    //file menu
    connect(ui->actionOpen, &QAction::triggered, this, &TimelineWindow::openEmFilesAndValidate);
    connect(ui->actionSet_Template, &QAction::triggered, this, &TimelineWindow::setTemplate);
    connect(ui->actionQuit, &QAction::triggered, this, &TimelineWindow::close);

    //data menu
    connect(ui->actionShow_Tables, &QAction::triggered, this, &TimelineWindow::showHideTables);
    connect(ui->actionShow_Tree_View, &QAction::triggered, this, &TimelineWindow::showHideTree);
    connect(ui->actionExport_Event_Data, &QAction::triggered, this, &TimelineWindow::exportData);
    connect(ui->actionExport_Dataset, &QAction::triggered, this, &TimelineWindow::exportDataset);
    //connect(ui->actionBatch_Export, &QAction::triggered, this, &TimelineWindow::exportDataBatch);
    connect(ui->actionImport_Event_Data, &QAction::triggered, this, &TimelineWindow::importData);
    connect(ui->actionExport_Event_Images, &QAction::triggered, this, &TimelineWindow::exportStillImages);
    connect(ui->actionDelete_All_Imported_Events, &QAction::triggered, this, &TimelineWindow::deleteAllImportedData);
    ui->actionShow_Tables->setShortcutContext(Qt::ApplicationShortcut);
    connect(ui->actionShow_Hide_UneventfulTimeline, &QAction::triggered, this, &TimelineWindow::showHideUneventfulTimeline);
    ui->actionShow_Hide_UneventfulTimeline->setShortcutContext(Qt::ApplicationShortcut);

    //help menu
    connect(ui->actionAbout, &QAction::triggered, this, &TimelineWindow::about);
    connect(ui->actionCurrent_Data_Template, &QAction::triggered, this, &TimelineWindow::templateInfo);
    connect(ui->actionOpen_Online_Documentation, &QAction::triggered, this, &TimelineWindow::openOnlineDoc);

    //timeline controls
    connect(ui->btnPlay, &QPushButton::clicked, this, &TimelineWindow::togglePause);
    connect(ui->btnNextEvent, &QPushButton::clicked, this, &TimelineWindow::nextDataEntryEvent);
    connect(ui->btnPreviousEvent, &QPushButton::clicked, this, &TimelineWindow::previousDataEntryEvent);
    connect(ui->btnCenterCursor, &QPushButton::clicked, this, &TimelineWindow::centerCursor);
    connect(ui->btnJumpTo, &QPushButton::clicked, this, &TimelineWindow::jumpTo);
    connect(ui->chkLockCursor, &QCheckBox::clicked, this, &TimelineWindow::lockCursor);
    connect(ui->btnCreateEvent, &QPushButton::clicked, this, &TimelineWindow::addPointMarker);
    connect(ui->btnDeleteEvent, &QPushButton::clicked, this, &TimelineWindow::deleteEvent);
    connect(ui->btnOpenEvent, &QPushButton::clicked, this, &TimelineWindow::openEvent);
    connect(ui->btnNextFrame, &QPushButton::clicked, this, &TimelineWindow::nextFrame);
    connect(ui->btnPreviousFrame, &QPushButton::clicked, this, &TimelineWindow::previousFrame);
    connect(ui->btnZoomIn, &QPushButton::clicked, this, &TimelineWindow::zoomIn);
    connect(ui->btnZoomOut, &QPushButton::clicked, this, &TimelineWindow::zoomOut);
    connect(ui->sldrPlaybackSpeed, &QSlider::valueChanged, this, &TimelineWindow::setPlaybackSpeed);

    //connect clicks on the map to the map
    QObject::connect(rApp->map, SIGNAL(coordinateClick(double, double)), this, SLOT(setPostionFromMap(double, double)));

    //data entry - only available when Timeline control has focus
    //add to list so that we can disable at will
    shortcuts.append(new QShortcut(Qt::Key_Delete, this, SLOT(deleteEvent())));  //delete selected event
    shortcuts.append(new QShortcut(Qt::Key_Return, this, SLOT(openEvent()),SLOT(openEvent()),Qt::ApplicationShortcut)); //open selected event
    shortcuts.append(new QShortcut(Qt::Key_Enter, this, SLOT(openEvent()),SLOT(openEvent()),Qt::ApplicationShortcut));  //open selected event
    shortcuts.append(new QShortcut(Qt::Key_Slash, this, SLOT(addPointMarker())));  //add point in time marker
    shortcuts.append(new QShortcut(Qt::Key_Up, this, SLOT(previousDataEntryEvent())));  //jump to previous data entry event
    shortcuts.append(new QShortcut(Qt::Key_Down, this, SLOT(nextDataEntryEvent())));  //jump to next data entry event
    shortcuts.append(new QShortcut(Qt::Key_Left, this, SLOT(previousSensorEvent())));  //jump to next sensor event (RFID read)
    shortcuts.append(new QShortcut(Qt::Key_Right, this, SLOT(nextSensorEvent())));  //jump to previous sensor event
    shortcuts.append(new QShortcut(Qt::Key_Escape, this, SLOT(cancelSelectOrDraw())));  //deselect everything cancel drawing
    shortcuts.append(new QShortcut(Qt::Key_A, this, SLOT(adjustDurationMarkerLeft())));  //adjust the left boundary of selected duration
    shortcuts.append(new QShortcut(Qt::Key_S, this, SLOT(adjustDurationMarkerRight())));  //adjust the left boundary of selected duration
    shortcuts.append(new QShortcut(Qt::Key_Apostrophe, this, SLOT(adjustPointEvent())));  //adjust the position of selected event
    shortcuts.append(new QShortcut(Qt::Key_Period, this, SLOT(createDurationMarkerKeyboard())));  //start and stop creation of duration event


    //application wide timeline shortcuts

    //playback speed
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_Space, this, SLOT(togglePause()), SLOT(togglePause()), Qt::ApplicationShortcut));  //play pause
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_R, this, SLOT(speedHalf()),SLOT(speedHalf()),Qt::ApplicationShortcut));  //.5x
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_T, this, SLOT(speedNormal()),SLOT(speedNormal()),Qt::ApplicationShortcut));  //1x
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_Y, this, SLOT(speedDouble()),SLOT(speedDouble()),Qt::ApplicationShortcut));  //2x


    //jump
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_1, this, SLOT(jumpBack5()),SLOT(jumpBack5()),Qt::ApplicationShortcut));  //jump back 5 seconds
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_2, this, SLOT(jumpBack3()),SLOT(jumpBack3()),Qt::ApplicationShortcut));   //jump back 3 seconds
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_3, this, SLOT(jumpBack1()),SLOT(jumpBack1()),Qt::ApplicationShortcut));   //jump back 1 second
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_4, this, SLOT(jumpBackHalf()),SLOT(jumpBackHalf()),Qt::ApplicationShortcut));  //jump back 1/2 second
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_5, this, SLOT(previousFrame()),SLOT(previousFrame()),Qt::ApplicationShortcut));  //previous frame
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_6, this, SLOT(nextFrame()),SLOT(nextFrame()),Qt::ApplicationShortcut));   //next frame
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_7, this, SLOT(jumpForwardHalf()),SLOT(jumpForwardHalf()),Qt::ApplicationShortcut)); //jump forward 1/2 second
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_8, this, SLOT(jumpForward1()),SLOT(jumpForward1()),Qt::ApplicationShortcut));  //jump forward 1 second
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_9, this, SLOT(jumpForward3()),SLOT(jumpForward3()),Qt::ApplicationShortcut));  //jump forward 3 seconds
    rApp->applicationShortcuts.append(new QShortcut(Qt::Key_0, this, SLOT(jumpForward5()),SLOT(jumpForward5()),Qt::ApplicationShortcut));  //jump forward 5 seconds

    ui->btnDeleteEvent->setEnabled(false);
    ui->btnOpenEvent->setEnabled(false);
    ui->menuData->setEnabled(false);
    ui->menuVideo->setEnabled(false);
    toggleShortcuts(false);

}

void TimelineWindow::templateInfo()
{
    QString msg;
    if(rApp->currentTemplate.length() > 0)
    {
        msg.append(QString("File Path: %1\n").arg(rApp->currentTemplate));
        if(rApp->templateName.length() > 0)
            msg.append(QString("Name: %1\n").arg(rApp->templateName));
        if(rApp->templateVersion.length() > 0)
            msg.append(QString("Version: %1\n").arg(rApp->templateVersion));
    }
    else
        msg.append("None");

    QMessageBox::information(this, "Current Template Details", msg);
}

void TimelineWindow::about()
{
    rApp->splash->show();
    rApp->splash->raise();
}


void TimelineWindow::toggleShortcuts(bool on)
{
    foreach(QShortcut *q, shortcuts)
        q->setEnabled(on);
}

void TimelineWindow::selectionStateChanged()
{
    ui->btnDeleteEvent->setEnabled(false);
    ui->btnOpenEvent->setEnabled(false);

    foreach(DataEntryEventData *data, rApp->eventData)
        if(data->marker->selected())
        {
            ui->btnDeleteEvent->setEnabled(true);
            ui->btnOpenEvent->setEnabled(true);
            break;
        }

    foreach(EmDataSensorEvent *data, rApp->emData->sensorEvents)
        if(data->marker->selected())
        {
            ui->btnOpenEvent->setEnabled(true);
            break;
        }

    foreach(EmDataEnumEvent *data, rApp->emData->enumEvents)
        if(data->marker->selected())
        {
            ui->btnOpenEvent->setEnabled(true);
            break;
        }
}




void TimelineWindow::setTemplate()
{
    //default to the current template
    QString temp = QFileDialog::getOpenFileName(this, "Select Template", rApp->currentTemplate, "Template Files (*.ini)");
    qDebug() << "template set to" << temp;
    if(!temp.isEmpty()) {
        rApp->currentTemplate = temp;
        rApp->settings->setValue("template_path", rApp->currentTemplate);
    }

}


void TimelineWindow::importData()
{

    QString imp = QFileDialog::getOpenFileName(this, "Select Event Data", rApp->currentEmDataDirectory, "Event Data JSON Files (*.json)");
    if(imp.isEmpty())
        return;

    QFile file(imp);
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        QString message = QString("Data import file could not be opened");
        msgBox.setText(message);
        msgBox.exec();
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    QJsonDocument events = QJsonDocument::fromJson(file.readAll());
    QJsonArray array = events.array();

    //qDebug() << events.toJson();


    QList<DataEntryEventData*> newEvents;
    for(int n = 0; n < array.count(); n++)
    {
        DataEntryEventData *event = new DataEntryEventData();
        //qDebug() << QJsonDocument(array.at(n).toObject()).toJson();
        if(event->fromJson(QJsonDocument(array.at(n).toObject())))
        {
            rApp->eventData.append(event);

            //add to list for database storage
            newEvents.append(event);

            //set the automation flag
            event->createdByAutomation = true;

            //add to the timeline
            if(event->eventType->markerType == DataEntryEventType::MarkerType::Duration)
                event->marker = addDurationMarkerAt(event->getStorageStartSeconds(), event->getStorageEndSeconds());
            else if(event->eventType->markerType == DataEntryEventType::MarkerType::Point)
                event->marker = addPointMarkerAt(event->getStorageSeconds());
            event->setMarkerStyle();

            showMessage(QString("imported %1 events").arg(n));
        }
    }

    //store in the database
    QList<QJsonDocument> jsonDocs;
    foreach(DataEntryEventData* evt, newEvents)
        jsonDocs.append(evt->toInternalJson());

    QList<int> eventIds = DatabaseUtility::batchStore(jsonDocs);

    //set the storage ids
    for(int n = 0; n < newEvents.length(); n++)
        newEvents.at(n)->storageId = eventIds.at(n);


    //reload data tables
    if(rApp->dataTableTabs->isVisible())
    {
        foreach(EventDataTable *w, rApp->tables)
            w->load();
    }

    QApplication::restoreOverrideCursor();

    file.close();
}


nlohmann::json TimelineWindow::buildJsonTree(DataEntryEventData *event)
{
    //make this event into json
    nlohmann::json obj;
    //std::string eventType = event->eventType->name.toStdString();
    obj = nlohmann::json::parse(event->toExportJson().toJson().toStdString());

    //if this is a duration, create arrays by distinct types of child events
    if(event->eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {

        foreach(DataEntryEventData *child, rApp->eventData)
        {
            //skip the same event
            if(child == event)
                continue;

            //is the possible child contained within the event?
            if((child->eventType->markerType == DataEntryEventType::MarkerType::Duration &&
                child->getMarkerStartSeconds() >= event->getMarkerStartSeconds() && child->getMarkerEndSeconds() <= event->getMarkerEndSeconds())
                    ||
                (child->eventType->markerType == DataEntryEventType::MarkerType::Point &&
                 child->getMarkerStartSeconds() >= event->getMarkerStartSeconds() && child->getMarkerStartSeconds() <= event->getMarkerEndSeconds()))
            {
                //confirm that event is the shortest enclosing
                DataEntryEventData *parent = child->getParent();

                if(parent!= event)
                    continue;

                QString sanitizedChildType = child->eventType->name;
                sanitizedChildType.remove(QRegularExpression("[^a-zA-Z\\d\\s]"));
                sanitizedChildType.replace(QRegularExpression("[\\s]"),"_");

                //if we don't already have an array for this type, create one
                if(!obj.contains(sanitizedChildType.toStdString()))
                    obj[sanitizedChildType.toStdString()] = nlohmann::json::array();

                //recursive add
                obj[sanitizedChildType.toStdString()].insert(obj[sanitizedChildType.toStdString()].end(),
                        buildJsonTree(child));
            }
        }
    }

    return obj;
}


void TimelineWindow::exportDataset()
{
    //is there an open dataset
    if(rApp->currentEmDataDirectory.isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText( QString("No dataset is open"));
        msgBox.exec();
        return;
    }

    QDir originDirectory(rApp->currentEmDataDirectory);


    QString destDirPath = QFileDialog::getExistingDirectory(this, "Select dataset export directory", rApp->currentEmDataDirectory);
    if(destDirPath.isEmpty())
        return;


    QDir destinationDirectory(destDirPath + "/" + originDirectory.dirName());


    if(destinationDirectory.exists())
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText( QString("Cannot overwrite directory that already exists"));
        msgBox.exec();
        return;
    }


    //can't export to a subdirectory of the dataset
    if(destinationDirectory.absolutePath().contains(originDirectory.absolutePath()))
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText( QString("Cannot export to a subdirectory of the dataset"));
        msgBox.exec();
        return;
    }




    QDir().mkpath(destinationDirectory.absolutePath());

    foreach (QString fileName, originDirectory.entryList(QDir::Files))
    {
        QFile::copy(originDirectory.absolutePath() + "/" + fileName, destinationDirectory.absolutePath() + "/" + fileName);
        showMessage(QString("copying " + fileName));
    }

    /* Possible race-condition mitigation? */
    /*

    QDir finalDestination(destinationDir);
    finalDestination.refresh();

    if(finalDestination.exists())
    {
        return true;
    }
    */

    showMessage(QString("copied " + originDirectory.absolutePath() + " to " + destinationDirectory.absolutePath()));


}




void TimelineWindow::exportData()
{

    ////////////
    //JSON EXPORT
    ////////////

    if(rApp->exportFormat == Constants::ExportFormat::JSON)
    {

        //find the selected event
        QList<DataEntryEventData *> events;

        foreach(DataEntryEventData *data, rApp->eventData)
            if(data->marker->selected())
            {
                events.append(data);
                break;
            }


        if(events.size() < 1) //no selected event
        {

            //find all top level events
            foreach(DataEntryEventData *data, rApp->eventData)
                if(data->getParent() == NULL)
                    events.append(data);

            /* or fail?
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setText( QString("Please select an event to export"));
            msgBox.exec();
            return;
            */
        }


        QString expBaseDir = QFileDialog::getExistingDirectory(this, "Select data export directory", rApp->currentEmDataDirectory);
        if(expBaseDir.isEmpty())
            return;



        //make an export directory
        QString expDir = QString("%1/data-export_software-%2_template-%3-%4_%5")
                .arg(expBaseDir)
                .arg(rApp->version)
                .arg(rApp->templateName.replace(" ","-"))
                .arg(rApp->templateVersion.replace(" ","-"))
                .arg(QDateTime::currentDateTimeUtc().toString(Constants::FILE_DATE_TIME_FORMAT));

        qDebug() << "exporting to: " << expDir;

        if(!QDir(expBaseDir).mkdir(expDir))
            return;


        //find and load the template if one is provided
        QString exptTmplt;
        if(!rApp->exportTemplateFile.isEmpty())
        {
            QString tmpltPath = QFileInfo(rApp->currentTemplate).absoluteDir().path();
            QString exptTmpltPath = tmpltPath.append(QDir::separator()).append(rApp->exportTemplateFile);
            QFile exptTmpltFile(exptTmpltPath);

            if(!exptTmpltFile.open(QIODevice::ReadOnly)) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setText( QString("Export template "+exptTmpltFile.fileName()+" could not be opened"));
                msgBox.exec();
                return;
            }

            exptTmplt = exptTmpltFile.readAll();
            exptTmpltFile.close();
        }

        foreach(DataEntryEventData* event, events)
        {

            //qDebug() << exptTmplt;
            nlohmann::json eventJson = buildJsonTree(event);

            //if we have a template, use it
            QString expt, exptRaw;
            if(!exptTmplt.isEmpty())
            {
                try
                {
                    inja::Environment env;
                    env.add_callback("len", 1, [](inja::Arguments& args) {
                        std::string s = args.at(0)->get<std::string>(); // Adapt the index and type of the argument
                        return s.length();
                    });
                    expt = QString::fromStdString(env.render(exptTmplt.toStdString(), eventJson));

                    //hack around bug in pantor/inja++ library - remove trailing commas
                    QRegExp exp("\\}\\s*,\\s*\\]");
                    expt.replace(exp,"}\n]");
                }
                catch(std::runtime_error e)
                {
                    //qDebug() <<  QString::fromStdString(eventJson.dump(2,' ',true));
                    qCritical() << QString::fromStdString(e.what());
                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.setText(QString("Export failed due to %1").arg(e.what()));
                    msgBox.exec();
                    return;
                }



            }
            //dump the JSON also
            exptRaw = QString::fromStdString(eventJson.dump(2,' ',true));



            //TODO better file name
            QFile file(QString("%1/%2-%3-%4.%5")
                       .arg(expDir)
                       .arg(event->eventType->name)
                       .arg(QDateTime::fromMSecsSinceEpoch(event->getMarkerStartSeconds()*1000).toUTC().toString(Constants::FILE_DATE_TIME_FORMAT))
                       .arg(QDateTime::fromMSecsSinceEpoch(event->getMarkerEndSeconds()*1000).toUTC().toString(Constants::FILE_DATE_TIME_FORMAT))
                       .arg(rApp->exportFileExtension));
            if(!file.open(QIODevice::WriteOnly))
                return;

            QTextStream out(&file);
            out << expt;
            //qDebug() << expt;
            file.close();

            QFile fileRaw(QString("%1/%2-%3-%4-raw_data.%5")
                       .arg(expDir)
                       .arg(event->eventType->name)
                       .arg(QDateTime::fromMSecsSinceEpoch(event->getMarkerStartSeconds()*1000).toUTC().toString(Constants::FILE_DATE_TIME_FORMAT))
                       .arg(QDateTime::fromMSecsSinceEpoch(event->getMarkerEndSeconds()*1000).toUTC().toString(Constants::FILE_DATE_TIME_FORMAT))
                       .arg(rApp->exportFileExtension));
            if(!fileRaw.open(QIODevice::WriteOnly))
                return;

            QTextStream outRaw(&fileRaw);
            outRaw << exptRaw;
            fileRaw.close();



        }
        showMessage(QString("export to %1 complete").arg(expDir));

    }

    ////////////
    //CSV EXPORT
    ////////////
    else if(rApp->exportFormat == Constants::ExportFormat::CSV)
    {

        QString expBaseDir = QFileDialog::getExistingDirectory(this, "Select data export directory", rApp->currentEmDataDirectory);
        if(expBaseDir.isEmpty())
            return;

        //make an export directory
        QString expDir = QString("%1/data-export_software-%2_template-%3-%4_%5")
                .arg(expBaseDir)
                .arg(rApp->version)
                .arg(rApp->templateName.replace(" ","-"))
                .arg(rApp->templateVersion.replace(" ","-"))
                .arg(QDateTime::currentDateTimeUtc().toString(Constants::FILE_DATE_TIME_FORMAT));

        qDebug() << "exporting to: " << expDir;

        if(!QDir(expBaseDir).mkdir(expDir))
            return;

        int x = 0;
        foreach(DataEntryEventType *type, rApp->eventTypes.values())
        {
            //TODO error handling for full disk, failed write due to permissions etc.
            QFile file(QString("%1/%2.%3").arg(expDir).arg(type->name).arg(rApp->exportFileExtension));
            if(!file.open(QIODevice::WriteOnly))
                return;

            QTextStream out(&file);

            bool headerWritten = false;
            uint row = 0;

            std::sort(rApp->eventData.begin(), rApp->eventData.end(), chronologicalCompare);

            foreach(DataEntryEventData *event, rApp->eventData)
                if(event->eventType == type)
                {

                    //headers
                    if(!headerWritten)
                    {
                        if(rApp->exportRowNums)
                            out << "Row,";
                        out << event->getHeader() << "\n";
                        headerWritten = true;
                    }

                    if(rApp->exportRowNums)
                        out << ++row << ",";
                    out << event->toCsv() << "\n";
                    showMessage(QString("exporting event %1").arg(++x));

                }

            file.close();
            qDebug() << "export file complete: " << file.fileName();
        }
        showMessage(QString("export of %1 events complete").arg(x));
    }

}

bool TimelineWindow::chronologicalCompare(DataEntryEventData *i, DataEntryEventData *ii)
{

    return i->getMarkerStartSeconds() < ii->getMarkerStartSeconds();
}


void TimelineWindow::exportStillImages()
{
    QString expBaseDir = QFileDialog::getExistingDirectory(this, "Select image export directory", rApp->currentEmDataDirectory);
    if(expBaseDir.isEmpty())
        return;

    //make an export directory
    QString expDir = QString("%1/image-export_software-%2_template-%3-%4_%5")
            .arg(expBaseDir)
            .arg(rApp->version)
            .arg(rApp->templateName.replace(" ","-"))
            .arg(rApp->templateVersion.replace(" ","-"))
            .arg(QDateTime::currentDateTimeUtc().toString(Constants::FILE_DATE_TIME_FORMAT));

    qDebug() << "exporting images to: " << expDir;

    if(!QDir(expBaseDir).mkdir(expDir))
        return;

    //pause the playback so that we can get enough CPU to export
    playing = false;

    int x = 0;
    //qDebug() << rApp->eventData.count();
    foreach(DataEntryEventData *event, rApp->eventData)
    {
        double t;

        //get event time
        if(event->eventType->markerType == DataEntryEventType::MarkerType::Duration)
            t = qobject_cast<QCPItemRect*>(event->marker)->topLeft->coords().x();
        else
            t = qobject_cast<QCPItemLine*>(event->marker)->start->coords().x();

        //for each video file at time t
        foreach(EmDataVideoDuration* vd, rApp->emData->videosByName)
        {

            if(t >= vd->startSeconds && t <= vd->endSeconds)
            {
                QString ffmpegPath = rApp->applicationDirPath() + "/ffmpeg.exe";
                QProcess process;
                process.setProcessChannelMode(QProcess::MergedChannels);
                double offsetSeconds = t - vd->startSeconds;
                QString timeStr = QDateTime::fromMSecsSinceEpoch(t*1000).toUTC().toString("yyyyMMdd-hhmmssZ");
                QString inputFilePath = rApp->currentEmDataDirectory + "/" + vd->fileName;
                QString outputFilePath = QString("%1/%2_%3_%4_channel-%5.png")
                        .arg(expDir)
                        .arg(event->eventType->name.replace(" ","-"))
                        .arg(QString::number((double)t,'f',2).replace(".","-"))
                        .arg(timeStr)
                        .arg(vd->videoChannel+1);
                QStringList args;
                args << "-y"  << "-ss" << QString::number(offsetSeconds) << "-i" << inputFilePath << "-vframes" << "1" << "-f" << "image2" << outputFilePath;
                qDebug() << "calling" << ffmpegPath << args;
                process.start(ffmpegPath, args);
                if(!process.waitForStarted(3000))
                    qWarning() << "  failed to start ffmpeg export process";
                //qDebug() << "ffmpeg output" << process.readAll();
                if(!process.waitForFinished(10000))
                    qWarning() << "  ffmpeg export process failed to complete";
            }
        }
        showMessage(QString("exporting event %1").arg(++x));
         //rApp->processEvents();
    }


    showMessage(QString("export of %1 events complete").arg(x));
}





void TimelineWindow::closeEvent (QCloseEvent* event)
{

    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "Quit",
                                                                "Are you sure?",
                                                                QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes)
        event->ignore();
    else
    {


        rApp->settings->setValue("geometry_timeline", saveGeometry());
        rApp->settings->setValue("geometry_measurement", rApp->measurement->saveGeometry());
        rApp->settings->setValue("geometry_data_tables", rApp->dataTableTabs->saveGeometry());
        rApp->settings->setValue("geometry_tree", rApp->tree->saveGeometry());
        //rApp->settings->setValue("geometry_map", ((QWidget*)rApp->map)->saveGeometry());
        //foreach(int x, rApp->tables.keys())
        //    rApp->settings->setValue(QString("geometry_data_table_tab_%1").arg(x), rApp->tables.value(x)->saveGeometry());
        foreach(int x, rApp->players.keys())
            rApp->settings->setValue(QString("geometry_video_%1").arg(x), rApp->players.value(x)->saveGeometry());
        foreach(int x, rApp->stillImageViewers.keys())
            rApp->settings->setValue(QString("geometry_still_%1").arg(x), rApp->stillImageViewers.value(x)->saveGeometry());
        foreach(int x, rApp->dataDialogs.keys())
            rApp->settings->setValue(QString("geometry_data_dialog_%1").arg(x), rApp->dataDialogs.value(x)->saveGeometry());
        rApp->settings->sync();
        QApplication::quit();
    }


}



TimelineWindow::~TimelineWindow()
{
    delete ui;
}


void TimelineWindow::nextFrame()
{
    playing = false;
    cursor += (1/(float)rApp->defaultFps);

}

void TimelineWindow::previousFrame()
{
    playing = false;
    cursor -= (1/(float)rApp->defaultFps);

}

bool TimelineWindow::cursorIsVisible()
{
    return ui->customPlot->xAxis->range().contains(cursor);
}


void TimelineWindow::nextDataEntryEvent()
{
    double distance = -1;
    QCPAbstractItem *target;
    foreach(DataEntryEventData *data, rApp->eventData)
    {
        if(data->eventType->markerType == DataEntryEventType::MarkerType::Duration)
        {
            QCPItemRect *duration = qobject_cast<QCPItemRect*>(data->marker);
            double nextDistance = duration->topLeft->coords().x() - cursor;
            if(nextDistance > 0 && (distance == -1 || nextDistance < distance))
            {
                target = duration;
                distance = nextDistance;
            }

            nextDistance = duration->bottomRight->coords().x() - cursor;
            if(nextDistance > 0 && (distance == -1 || nextDistance < distance))
            {
                target = duration;
                distance = nextDistance;
            }
        }
        else
        {
            QCPItemLine *point = qobject_cast<QCPItemLine*>(data->marker);
            double nextDistance = point->start->coords().x() - cursor;
            if(nextDistance > 0 && (distance == -1 || nextDistance < distance))
            {
                target = point;
                distance = nextDistance;
            }
        }
    }

    if(distance > 0)
    {
        cursor += distance;
        selectItem(target);

        if(!cursorIsVisible() || cursorIsLocked())
            centerCursor();
    }
    else
    {
        selectNone();
        cursor = secondsRange.upper;
    }
}



void TimelineWindow::selectDataEntryEvent(int id)
{
    foreach(DataEntryEventData *data, rApp->eventData)
    {
        if(data->storageId == id)
        {
            selectItem(data->marker);

            if(!cursorIsVisible() || cursorIsLocked())
                centerCursor();

            break;
        }
    }
}


void TimelineWindow::nextSensorEvent()
{
    //qDebug() << "in nextSensorEvent()";

    double distance = -1;
    QCPAbstractItem *target;
    foreach(EmDataSensorEvent *data, rApp->emData->sensorEvents)
    {
        //qDebug() << "data " << data->displayData.value("Tag Id");
        if(qobject_cast<EmDataSensorDurationEvent *>(data))
        {
            EmDataSensorDurationEvent *e = qobject_cast<EmDataSensorDurationEvent *>(data);

            double nextDistance = e->beginSeconds - cursor;
            //qDebug() << "  nextDistance " << nextDistance ;
            if(nextDistance > 0 && (distance == -1 || nextDistance < distance))
            {
                target = e->marker;
                //qDebug() << "  target " << target ;
                distance = nextDistance;
            }

            nextDistance = e->endSeconds - cursor;
            //qDebug() << "  nextDistance " << nextDistance ;
            if(nextDistance > 0 && (distance == -1 || nextDistance < distance))
            {
                target = e->marker;
                distance = nextDistance;
            }

        }
        else
        {

            QCPItemLine *point = qobject_cast<QCPItemLine*>(data->marker);
            double nextDistance = point->start->coords().x() - cursor;
            if(nextDistance > 0 && (distance == -1 || nextDistance < distance))
            {
                target = point;
                distance = nextDistance;
            }

        }
    }



    //qDebug() << "distance " << distance;
    if(distance > 0)
    {
        cursor += distance;
        //qDebug() << "cursor set " << cursor;
        selectItem(target);
        //qDebug() << "item selected";

        if(!cursorIsVisible() || cursorIsLocked())
            centerCursor();
         //qDebug() << "cursor centered";
    }
    else
    {
        selectNone();
        cursor = secondsRange.upper;
    }


}


void TimelineWindow::jumpTo()
{
    bool ok;
       QString text = QInputDialog::getText(this, "Jump To",
                                            "UTC Date Time", QLineEdit::Normal,
                                            QDateTime::fromMSecsSinceEpoch(cursor*1000).toUTC().toString("MM/dd/yyyy hh:mm:ss"), &ok);
       if (ok && !text.isEmpty())
       {
           //first try seconds since the epoch
           bool parseable;
           double seconds = text.toDouble(&parseable);
           if(parseable && seconds <= secondsRange.upper && seconds >= secondsRange.lower)
               cursor = seconds;
           else //try a text date
           {
               //TODO constants
               QDateTime dt = QDateTime::fromString(text,"MM/dd/yyyy hh:mm:ss");
               dt.setTimeSpec(Qt::UTC);
               jumpToDateTime(dt);
           }


       }

}


void TimelineWindow::jumpToDateTime(QDateTime dt)
{
    if(dt.isValid() && dt.toTime_t() <= secondsRange.upper && dt.toTime_t() >= secondsRange.lower)
    {
        cursor = dt.toMSecsSinceEpoch()/1000.0;
        qDebug() << "jumped to" << QString::number(dt.toMSecsSinceEpoch()/1000.0,'f',6);
    }
}



void TimelineWindow::cancelSelectOrDraw()
{
    selectNone();

    if(activeDuration && (mode == DrawingMode::CreatingDuration || mode == DrawingMode::CreatingDurationKeyboard))
        deleteMarker(activeDuration);

    mode = DrawingMode::None;
}

void TimelineWindow::selectItem(QCPAbstractItem *target)
{
    selectNone();
    target->setSelected(true);
    itemSelected(target);
}

void TimelineWindow::selectNone()
{
    foreach(DataEntryEventData *data, rApp->eventData)
        data->marker->setSelected(false);
    //qDebug() << "user events deselected";
    foreach(EmDataSensorEvent *sensor, rApp->emData->sensorEvents)
        sensor->marker->setSelected(false);
    //qDebug() << "sensor events deselected";
    foreach(EmDataEnumEvent *data, rApp->emData->enumEvents)
        data->marker->selection().clear();
}









void TimelineWindow::previousDataEntryEvent()
{

    double distance = -1;
    QCPAbstractItem *target;
    foreach(DataEntryEventData *data, rApp->eventData)
    {
        if(data->eventType->markerType == DataEntryEventType::MarkerType::Duration)
        {
            QCPItemRect *duration = qobject_cast<QCPItemRect*>(data->marker);
            double nextDistance = -1 * (duration->bottomRight->coords().x() - cursor);
            //TODO tolerance in constants
            if(nextDistance > .5 && (distance == -1 || nextDistance < distance))
            {
                target = duration;
                distance = nextDistance;
            }

            nextDistance = -1 * (duration->topLeft->coords().x() - cursor);
            //TODO tolerance in constants
            if(nextDistance > .5 && (distance == -1 || nextDistance < distance))
            {
                target = duration;
                distance = nextDistance;
            }

        }
        else
        {
            QCPItemLine *point = qobject_cast<QCPItemLine*>(data->marker);
            double nextDistance = -1 * (point->start->coords().x() - cursor);
            //TODO tolerance in constants
            if(nextDistance > .5 && (distance == -1 || nextDistance < distance))
            {
                target = point;
                distance = nextDistance;
            }

        }
    }

    if(distance > 0)
    {
        cursor -= distance;
        selectItem(target);
        if(!cursorIsVisible() || cursorIsLocked())
            centerCursor();
    }
    else
    {
        cursor = secondsRange.lower;
        selectNone();
    }


}




void TimelineWindow::openOnlineDoc()
{
    QDesktopServices::openUrl(QUrl(rApp->settings->value("help_url").toString()));

}


void TimelineWindow::previousSensorEvent()
{

    double distance = -1;
    QCPAbstractItem *target;
    foreach(EmDataSensorEvent *data, rApp->emData->sensorEvents)
    {
        if(qobject_cast<EmDataSensorDurationEvent *>(data))
        {

            EmDataSensorDurationEvent *e = qobject_cast<EmDataSensorDurationEvent *>(data);

            double nextDistance = -1 * (e->beginSeconds - cursor);
            //TODO tolerance in constants
            if(nextDistance > .5 && (distance == -1 || nextDistance < distance))
            {
                target = e->marker;
                distance = nextDistance;
            }

            nextDistance = -1 * (e->endSeconds - cursor);
            //TODO tolerance in constants
            if(nextDistance > .5 && (distance == -1 || nextDistance < distance))
            {
                target = e->marker;
                distance = nextDistance;
            }


        }
        else
        {
            /*
            QCPItemLine *point = qobject_cast<QCPItemLine*>(data->marker);
            double nextDistance = -1 * (point->start->coords().x() - cursor);
            //TODO tolerance in constants
            if(nextDistance > 3.0123 && (distance == -1 || nextDistance < distance))
            {
                target = point;
                distance = nextDistance;
            }
*/
        }
    }




    if(distance > 0)
    {
        cursor -= distance;
        selectItem(target);
        if(!cursorIsVisible() || cursorIsLocked())
            centerCursor();
    }
    else
    {
        cursor = secondsRange.lower;
        selectNone();
    }


}



bool TimelineWindow::cursorIsLocked()
{
   return ui->chkLockCursor->checkState() == Qt::Checked;
}



void TimelineWindow::togglePause()
{
    playing = !playing;

}



void TimelineWindow::setPlaybackSpeed()
{
    //set the playback speed 2^sliderValue (-2,-1,0,1,2,etc)
    speedRatio = qPow(2,ui->sldrPlaybackSpeed->value()/rApp->speedSteps);
    if(rApp->speedSteps > 1)
    {
        QString s = QString::number(speedRatio,'f',3);
        s = s.left(4);
        while (s.endsWith('0'))
            s = s.left(s.length()-1);
        while (s.endsWith('.'))
            s = s.left(s.length()-1);

        ui->lblPlaybackSpeed->setText(QString("%1X").arg(s));
    }
    else
        ui->lblPlaybackSpeed->setText(QString("%1X").arg(speedRatio));
    qDebug() << "playback speed set to " << speedRatio;
}




void TimelineWindow::setupPlot()
{

   QCustomPlot* customPlot = ui->customPlot;

   // configure bottom axis to show date and time instead of number
   QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
   dateTicker->setDateTimeFormat("hh:mm:ss Z\nMM/dd/yyyy");
   dateTicker->setDateTimeSpec(Qt::UTC);
   dateTicker->setTickCount(10);
   customPlot->xAxis->setTicker(dateTicker);

   // set a more compact font size for bottom and left axis tick labels
   customPlot->xAxis->setTickLabelFont(QFont(QFont().family(), 8));
   customPlot->yAxis->setTickLabelFont(QFont(QFont().family(), 8));
   //customPlot->xAxis->setAutoTickStep(true);


   // set axis labels
   //customPlot->xAxis->setLabel("Time/Date");
   customPlot->setInteractions(QCP::iRangeDrag |  QCP::iRangeZoom | QCP::iSelectPlottables | QCP::iSelectItems);
   customPlot->axisRect()->setRangeDrag(Qt::Horizontal);
   customPlot->axisRect()->setRangeZoom(Qt::Horizontal);

   // set axis ranges to show all data
   customPlot->xAxis->setRange(secondsRange.lower,secondsRange.upper);
   customPlot->yAxis->setRange(valuesRange.lower,valuesRange.upper);

   customPlot->legend->setVisible(true);
   customPlot->legend->setSelectableParts(QCPLegend::spItems);

   customPlot->addLayer("duration",customPlot->layer("main"));
   customPlot->addLayer("point",customPlot->layer("duration"));
   customPlot->addLayer("cursor",customPlot->layer("point"));


   //TODO use new connect syntax
   connect(customPlot, SIGNAL(mouseDoubleClick(QMouseEvent*)), this, SLOT(positionChange(QMouseEvent*)));
   connect(customPlot, SIGNAL(mouseDoubleClick(QMouseEvent*)), this, SLOT(addDurationMarker(QMouseEvent*)));
   connect(customPlot, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(mouseMove(QMouseEvent*)));
   connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(setRange(QCPRange)));
   //connect(customPlot, SIGNAL(itemClick(QCPAbstractItem*, QMouseEvent*)), this, SLOT(itemSelected(QCPAbstractItem*)));
   //connect(customPlot, SIGNAL(legendClick(QCPLegend*, QCPAbstractLegendItem*, QMouseEvent*)), this, SLOT(legendClicked(QCPLegend*,QCPAbstractLegendItem*,QMouseEvent*)));
   connect(customPlot, SIGNAL(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*,QMouseEvent*)), this, SLOT(legendDoubleClicked(QCPLegend*,QCPAbstractLegendItem*,QMouseEvent*)));
   //connect(customPlot, SIGNAL(selectionChangedByUser()), this, SLOT(selectionStateChanged()));
   connect(customPlot, SIGNAL(selectionChangedByUser()), this, SLOT(isItemSelected()));

   //we are not drawing anything to begin with
   mode = DrawingMode::None;

   //default to start of data
   cursor = secondsRange.lower;

   //enable openGL
   customPlot->setOpenGl(true, 16);

   //customPlot->setSelectionTolerance(1);


}

void TimelineWindow::showHideUneventfulTimeline()
{
     QCustomPlot* customPlot = ui->customPlot;

     if(customPlot->xAxis->range() == secondsRange)
     {

        //start at the end and work backwards
        double lowerSeconds = secondsRange.upper;
        double upperSeconds = secondsRange.lower;


        foreach(DataEntryEventData *data, rApp->eventData)
        {
            if(data->getMarkerStartSeconds() < lowerSeconds)
                lowerSeconds = data->getMarkerStartSeconds();
            if(data->getMarkerEndSeconds() > upperSeconds)
                upperSeconds = data->getMarkerEndSeconds();
        }

        //did we find any events, other than a single point (upper and lower would be equal)
        if(upperSeconds > lowerSeconds)
            customPlot->xAxis->setRange(lowerSeconds, upperSeconds);

     }
     else
     {
         customPlot->xAxis->setRange(secondsRange);
     }
}





void TimelineWindow::isItemSelected()
{
    //qDebug() << "items selected:" << ui->customPlot->selectedItems().count();
    if(ui->customPlot->selectedItems().count() > 0)
        itemSelected(ui->customPlot->selectedItems().at(0));

    if(ui->customPlot->selectedGraphs().count() > 0)
        graphSelected(ui->customPlot->selectedGraphs().at(0));

}

void TimelineWindow::graphSelected(QCPAbstractPlottable* p)
{
    foreach(EmDataEnumEvent *evt, rApp->emData->enumEvents)
         if(evt->marker == p)
         {
             qDebug() << evt->getSummary();
             showMessage(evt->getSummary());
         }
}



void TimelineWindow::beginPlayback()
{
    //set sliderPlayback speed at realtime
    speedRatio = 1;
    ui->sldrPlaybackSpeed->setValue(0);
    setPlaybackSpeed();

    //start playback
    if(!timer->isActive()){
         timer->start(UPDATE_INTERVAL_MS);
         playing = true;
    }


}

void TimelineWindow::legendDoubleClicked(QCPLegend* legend, QCPAbstractLegendItem* item, QMouseEvent* event)
{


    if (item) // only react if item was clicked (user could have clicked on border padding of legend where there is no item, then item is 0)
    {
        QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);

        //toggle visibility
        plItem->plottable()->setVisible(!plItem->plottable()->visible());
        plItem->setTextColor(!plItem->plottable()->visible() ? QColor(0,0,0,100) : QColor(0,0,0,255));

        ui->customPlot->replot();

        qDebug() << "legend item toggled";

    }




}

/*
void TimelineWindow::legendDoubleClicked(QCPLegend* legend, QCPAbstractLegendItem* item, QMouseEvent* event)
{
   // qDebug() << "legendDoubleClicked";
   // event->ignore();
}
*/

void TimelineWindow::zoomIn()
{
    QCPRange currentRange = ui->customPlot->xAxis->range();
    double quarterSpan = (currentRange.upper - currentRange.lower)/4;
    ui->customPlot->xAxis->setRangeLower(currentRange.lower+quarterSpan);
    ui->customPlot->xAxis->setRangeUpper(currentRange.upper-quarterSpan);
}


void TimelineWindow::zoomOut()
{
    QCPRange currentRange = ui->customPlot->xAxis->range();
    double quarterSpan = (currentRange.upper - currentRange.lower)/4;
    ui->customPlot->xAxis->setRangeLower(currentRange.lower-quarterSpan);
    ui->customPlot->xAxis->setRangeUpper(currentRange.upper+quarterSpan);
}

void TimelineWindow::addSeriesLine(QString name, QVector<double> x, QVector<double> y, QPen p)
{
   QCustomPlot* customPlot = ui->customPlot;
   customPlot->addGraph();
   customPlot->graph()->setData(x, y);
   customPlot->graph()->setPen(p);
   customPlot->graph()->setName(name);
   customPlot->graph()->setSelectable(QCP::SelectionType::stNone);
}

//TODO may not work with very large plots, and may be a performance problem
void TimelineWindow::addSeriesShaded(QString name, QVector<double> x, QVector<double> y, QBrush p)
{
   QCustomPlot* customPlot = ui->customPlot;
   customPlot->addGraph();
   customPlot->graph()->setLineStyle(QCPGraph::lsStepRight);
   customPlot->graph()->setData(x, y);
   customPlot->graph()->setName(name);
   customPlot->graph()->setBrush(p);
   customPlot->graph()->setPen(Qt::NoPen);
   customPlot->graph()->setSelectable(QCP::SelectionType::stNone);
}

void TimelineWindow::addSeriesStepLine(QString name, QVector<double> x, QVector<double> y, QPen p)
{
   QCustomPlot* customPlot = ui->customPlot;
   customPlot->addGraph();
   customPlot->graph()->setLineStyle(QCPGraph::lsStepRight);
   customPlot->graph()->setData(x, y);
   customPlot->graph()->setPen(p);
   customPlot->graph()->setName(name);
   customPlot->graph()->setSelectable(QCP::SelectionType::stNone);
}


QCPGraph* TimelineWindow::addEnumEvent(double seconds, float pos, QColor c, EmDataEnumEvent::Symbol sym, float size)
{
   QVector<double> x, y;
   x.append(seconds);
   y.append(pos > 0 ? rApp->emData->maxValue * pos : abs(rApp->emData->minValue) * pos);
   QCustomPlot* customPlot = ui->customPlot;
   QCPGraph* g = customPlot->addGraph();
   g->setLineStyle(QCPGraph::lsNone);
   g->setData(x, y);
   g->setPen(QPen(c,2));
   QCPScatterStyle::ScatterShape s;
   if(sym == EmDataEnumEvent::Symbol::Disc)
        s = QCPScatterStyle::ssDisc;
   else if(sym == EmDataEnumEvent::Symbol::Square)
       s = QCPScatterStyle::ssSquare;
   else if(sym == EmDataEnumEvent::Symbol::Circle)
       s = QCPScatterStyle::ssCircle;
   else if(sym == EmDataEnumEvent::Symbol::Triangle)
       s = QCPScatterStyle::ssTriangle;
   else if(sym == EmDataEnumEvent::Symbol::TriangleInverted)
       s = QCPScatterStyle::ssTriangleInverted;
   else if(sym == EmDataEnumEvent::Symbol::Dimond)
       s = QCPScatterStyle::ssDiamond;

   g->setScatterStyle(QCPScatterStyle(s, size));
   g->setSelectable(QCP::SelectionType::stWhole);
   g->removeFromLegend();
   return g;

}


QCPItemRect* TimelineWindow::addSensorDurationEvent(double beginSeconds, double endSeconds, double value)
{
    QCustomPlot *customPlot = ui->customPlot;
    QCPItemRect *newDuration = new QCPItemRect(customPlot);
    newDuration->topLeft->setCoords(beginSeconds, value);
    newDuration->bottomRight->setCoords(endSeconds, valuesRange.lower);

    return newDuration;

}




void TimelineWindow::deleteEvent()
{
    foreach(DataEntryEventData *data, rApp->eventData)
        if(data->marker->selected())
        {
            deleteMarker(data->marker);
            DatabaseUtility::remove(data->storageId);
            rApp->tables.value(data->eventType->id)->deleteRow(data);
            rApp->eventData.removeOne(data);
            rApp->tree->load();

        }

    selectionStateChanged();

}


void TimelineWindow::openEvent()
{

    foreach(DataEntryEventData *data, rApp->eventData)
        if(data->marker->selected())
            data->eventType->dialog->initialize(data, DataEntryDialog::Mode::Update);


}

/*
void TimelineWindow::jumpToRelated()
{

    //TODO rework this - need to select target?  make rfid digester put in absolute, not offset
    foreach(EmDataSensorEvent *data, rApp->emData->sensorEvents)
        if(data->marker->selected() && data->relatedEventOffsets.size() > 0  && data->relatedEventOffsets.values().at(0) != 0)
        {
            if(qobject_cast<EmDataSensorDurationEvent *>(data))
                cursor = qobject_cast<EmDataSensorDurationEvent *>(data)->beginSeconds + data->relatedEventOffsets.values().at(0);
            //else if(qobject_cast<EmDataSensorPointEvent *>(data))
            //    cursor = qobject_cast<EmDataSensorPointEvent *>(data)->seconds + data->relatedEventOffsets.values().at(0);
            if(cursor > secondsRange.upper)
                cursor = secondsRange.upper;
            else if(cursor < secondsRange.lower)
                cursor = secondsRange.lower;

            if(!cursorIsVisible() || cursorIsLocked())
                centerCursor();
            break;
        }



}
*/

QCPItemRect* TimelineWindow::addDurationMarkerAt(double startTime, double endTime)
{

    QCustomPlot *customPlot = ui->customPlot;
    QCPItemRect *duration = new QCPItemRect(customPlot);
    //customPlot->addItem(duration);
    //TODO - this screws up selection
    //region->setLayer("sectionBackground");
    duration->topLeft->setTypeX(QCPItemPosition::ptPlotCoords);
    duration->topLeft->setTypeY(QCPItemPosition::ptPlotCoords);
    duration->topLeft->setAxes(customPlot->xAxis, customPlot->yAxis);
    duration->topLeft->setAxisRect(customPlot->axisRect());
    duration->bottomRight->setTypeX(QCPItemPosition::ptPlotCoords);
    duration->bottomRight->setTypeY(QCPItemPosition::ptPlotCoords);
    duration->bottomRight->setAxes(customPlot->xAxis, customPlot->yAxis);
    duration->bottomRight->setAxisRect(customPlot->axisRect());
    duration->setClipToAxisRect(true); // is by default true already, but this will change in QCP 2.0.0

    duration->topLeft->setCoords(startTime,valuesRange.upper);
    duration->bottomRight->setCoords(endTime,valuesRange.lower);

    duration->setLayer("duration");

    connect(duration, &QCPAbstractItem::selectionChanged, this, &TimelineWindow::selectionStateChanged );

    return duration;
}




void TimelineWindow::createDurationMarkerKeyboard()
{

    QCustomPlot* customPlot = ui->customPlot;

    if(mode == DrawingMode::None) //start adding a new duration
    {
        mode = DrawingMode::CreatingDurationKeyboard;
        durationOrigin = cursor;

        if(durationOrigin > secondsRange.upper)
            durationOrigin = secondsRange.upper;
        else if(durationOrigin < secondsRange.lower)
            durationOrigin = secondsRange.lower;

        activeDuration = new QCPItemRect(customPlot);
        activeDuration->topLeft->setTypeX(QCPItemPosition::ptPlotCoords);
        activeDuration->topLeft->setTypeY(QCPItemPosition::ptPlotCoords);
        activeDuration->topLeft->setAxes(customPlot->xAxis, customPlot->yAxis);
        activeDuration->topLeft->setAxisRect(customPlot->axisRect());
        activeDuration->bottomRight->setTypeX(QCPItemPosition::ptPlotCoords);
        activeDuration->bottomRight->setTypeY(QCPItemPosition::ptPlotCoords);
        activeDuration->bottomRight->setAxes(customPlot->xAxis, customPlot->yAxis);
        activeDuration->bottomRight->setAxisRect(customPlot->axisRect());
        activeDuration->setClipToAxisRect(true); // is by default true already, but this will change in QCP 2.0.0

        activeDuration->topLeft->setCoords(durationOrigin,valuesRange.upper);
        activeDuration->bottomRight->setCoords(durationOrigin,valuesRange.lower);

        //invisible for now
        activeDuration->setSelectable(false);
        activeDuration->setBrush(Qt::NoBrush);
        activeDuration->setPen(QPen(Qt::red));
        activeDuration->setSelectedBrush(Qt::NoBrush);
        activeDuration->setSelectedPen(Qt::NoPen);

    }
    else if(mode == DrawingMode::CreatingDurationKeyboard) //finished adding a new duration
    {
        mode = DrawingMode::None;

        //to small to keep, could also expand size
        if(activeDuration->bottomRight->coords().x() - activeDuration->topLeft->coords().x() < Constants::MINIMUM_DURATION)
        {
            deleteMarker(activeDuration);
            return;
        }

        connect(activeDuration, &QCPAbstractItem::selectionChanged, this, &TimelineWindow::selectionStateChanged );

        if(!showContextMenu(QPoint(activeDuration->bottomRight->pixelPosition().x(),customPlot->height()/2), DataEntryEventType::MarkerType::Duration, activeDuration))
           deleteMarker(activeDuration);
        else
            activeDuration->setLayer("duration");

        activeDuration = NULL;
    }

}






void TimelineWindow::addDurationMarker(QMouseEvent* mouseEvent)
{
    if(mouseEvent->button() != Qt::MouseButton::RightButton)
        return;

    QCustomPlot* customPlot = ui->customPlot;

    if(mode == DrawingMode::EditingPoint)
    {
        adjustPointEvent();  //if we are in point edit mode, call adjust to cancel editing
    }
    else if(mode == DrawingMode::EditingDuration)  //if we are in edit mode, call adjust duration to cancel editing
    {
        adjustDurationMarker(DurationEditMode::Both);
    }
    else if(mode == DrawingMode::None) //start adding a new duration
    {
        mode = DrawingMode::CreatingDuration;
        durationOrigin = ui->customPlot->xAxis->pixelToCoord(mouseEvent->pos().x());

        if(durationOrigin > secondsRange.upper)
            durationOrigin = secondsRange.upper;
        else if(durationOrigin < secondsRange.lower)
            durationOrigin = secondsRange.lower;

        activeDuration = new QCPItemRect(customPlot);
        //customPlot->addItem(activeDuration);
        //TODO - this screws up selection
        //region->setLayer("sectionBackground");
        activeDuration->topLeft->setTypeX(QCPItemPosition::ptPlotCoords);
        activeDuration->topLeft->setTypeY(QCPItemPosition::ptPlotCoords);
        activeDuration->topLeft->setAxes(customPlot->xAxis, customPlot->yAxis);
        activeDuration->topLeft->setAxisRect(customPlot->axisRect());
        activeDuration->bottomRight->setTypeX(QCPItemPosition::ptPlotCoords);
        activeDuration->bottomRight->setTypeY(QCPItemPosition::ptPlotCoords);
        activeDuration->bottomRight->setAxes(customPlot->xAxis, customPlot->yAxis);
        activeDuration->bottomRight->setAxisRect(customPlot->axisRect());
        activeDuration->setClipToAxisRect(true); // is by default true already, but this will change in QCP 2.0.0

        activeDuration->topLeft->setCoords(durationOrigin,valuesRange.upper);
        activeDuration->bottomRight->setCoords(durationOrigin,valuesRange.lower);

        //invisible for now
        activeDuration->setSelectable(false);
        activeDuration->setBrush(Qt::NoBrush);
        activeDuration->setPen(QPen(Qt::red));
        activeDuration->setSelectedBrush(Qt::NoBrush);
        activeDuration->setSelectedPen(Qt::NoPen);

        activeDuration->setLayer("duration");

    }
    else if(mode == DrawingMode::CreatingDuration) //finished adding a new duration
    {
        mode = DrawingMode::None;

        //to small to keep, could also expand size
        if(activeDuration->bottomRight->coords().x() - activeDuration->topLeft->coords().x() < Constants::MINIMUM_DURATION)
        {
            deleteMarker(activeDuration);
            return;
        }

        connect(activeDuration, &QCPAbstractItem::selectionChanged, this, &TimelineWindow::selectionStateChanged );

        //could also postion QPoint(ui->customPlot->width()/2,ui->customPlot->height()/2)
        if(!showContextMenu(mouseEvent->pos(), DataEntryEventType::MarkerType::Duration, activeDuration))
           deleteMarker(activeDuration);

        activeDuration = NULL;
    }

}



void TimelineWindow::mouseMove(QMouseEvent *mouseEvent)
{
        if(mode == DrawingMode::None) //get out as fast as possible
            return;

        //x coordinate for mouse
        double mouseCoordX = ui->customPlot->xAxis->pixelToCoord(mouseEvent->pos().x());

        if(mouseCoordX > secondsRange.upper)
            mouseCoordX = secondsRange.upper;
        else if(mouseCoordX < secondsRange.lower)
            mouseCoordX = secondsRange.lower;

        if(mode == DrawingMode::CreatingDuration || mode == DrawingMode::EditingDuration)
        {

           if(mouseCoordX < durationOrigin)
           {
               activeDuration->topLeft->setCoords(mouseCoordX, activeDuration->topLeft->coords().y());
               activeDuration->bottomRight->setCoords(durationOrigin, activeDuration->bottomRight->coords().y());

           }
           else if (mouseCoordX > durationOrigin)
           {
               activeDuration->topLeft->setCoords(durationOrigin, activeDuration->topLeft->coords().y());
               activeDuration->bottomRight->setCoords(mouseCoordX, activeDuration->bottomRight->coords().y());
           }

           showMessage("event covers period " + QDateTime::fromMSecsSinceEpoch(activeDuration->topLeft->coords().x()*1000).toUTC().toString("MM/dd/yyyy hh:mm:ss Z")
                       + " " + QDateTime::fromMSecsSinceEpoch(activeDuration->bottomRight->coords().x()*1000).toUTC().toString("MM/dd/yyyy hh:mm:ss Z"));
        }
        else if(mode == DrawingMode::EditingPoint)
        {
            activePoint->start->setCoords(mouseCoordX, activePoint->start->coords().y());
            activePoint->end->setCoords(mouseCoordX, activePoint->end->coords().y());

            showMessage("event move to " + QDateTime::fromMSecsSinceEpoch(mouseCoordX*1000).toUTC().toString("MM/dd/yyyy hh:mm:ss Z"));
        }


}

void TimelineWindow::updateActiveDurationKeyboard()
{

    if(mode == DrawingMode::CreatingDurationKeyboard)
    {
        double currentCursorX = cursor;

        if(currentCursorX > secondsRange.upper)
            currentCursorX = secondsRange.upper;
        else if(currentCursorX < secondsRange.lower)
            currentCursorX = secondsRange.lower;

       if(currentCursorX < durationOrigin)
       {
           activeDuration->topLeft->setCoords(currentCursorX, activeDuration->topLeft->coords().y());
           activeDuration->bottomRight->setCoords(durationOrigin, activeDuration->bottomRight->coords().y());

       }
       else if (currentCursorX > durationOrigin)
       {
           activeDuration->topLeft->setCoords(durationOrigin, activeDuration->topLeft->coords().y());
           activeDuration->bottomRight->setCoords(currentCursorX, activeDuration->bottomRight->coords().y());
       }

       showMessage("event covers period " + QDateTime::fromMSecsSinceEpoch(activeDuration->topLeft->coords().x()*1000).toUTC().toString("MM/dd/yyyy hh:mm:ss Z")
                   + " " + QDateTime::fromMSecsSinceEpoch(activeDuration->bottomRight->coords().x()*1000).toUTC().toString("MM/dd/yyyy hh:mm:ss Z"));
    }



}

bool TimelineWindow::showContextMenu(QPoint pos, DataEntryEventType::MarkerType markerType, QCPAbstractItem *item)
{

    QPoint globalPos = ui->customPlot->mapToGlobal(pos);
    // for QAbstractScrollArea and derived classes you would use:
    // QPoint globalPos = myWidget->viewport()->mapToGlobal(pos);

    QMenu eventMenu;
    foreach(DataEntryEventType* type, rApp->eventTypes)
        if(type->markerType == markerType)
        {   QAction *action = new QAction(0);
            action->setText(type->name);
            action->setData(type->id);
            eventMenu.addAction(action);
        }

    if(eventMenu.actions().length() > 0)
    {
        QAction* selectedItem = eventMenu.exec(globalPos);
        if (selectedItem)
        {
            DataEntryEventType *type = rApp->eventTypes.value(selectedItem->data().toInt());
            DataEntryEventData *data = new DataEntryEventData();
            data->eventType = type;
            data->marker = item;
            type->dialog->initialize(data, DataEntryDialog::Mode::Create);
            return true;
        }
    }

    return false;
}

void TimelineWindow::addPointMarker()
{
    addPointMarkerWithEvent(-1, false, QList<DataEntryFieldValue>()); //show pick list of events, don't auto confirm
}

void TimelineWindow::addPointMarkerWithEvent(int eventTypeId, bool noConfirm, QList<DataEntryFieldValue> hotkeyPresets)
{
    //can't create a point event when drawing a duration
    if(mode != DrawingMode::None)
        return;

    //capture the current position
    double mark = cursor;
    QCPItemLine* newPointMarker = new QCPItemLine(ui->customPlot);
    //ui->customPlot->addItem(newPointMarker);
    //invisible for now
    newPointMarker->setPen(Qt::NoPen);
    newPointMarker->setSelectedPen(Qt::NoPen);
    newPointMarker->setSelectable(false);
    newPointMarker->start->setCoords(mark, ui->customPlot->yAxis->range().lower);
    newPointMarker->end->setCoords(mark, ui->customPlot->yAxis->range().upper);

    newPointMarker->setLayer("point");

    connect(newPointMarker, &QCPAbstractItem::selectionChanged, this, &TimelineWindow::selectionStateChanged );

    if(eventTypeId == -1) //not opened with hotkey
    {
        if(!showContextMenu(QPoint(ui->customPlot->width()/2,ui->customPlot->height()/2), DataEntryEventType::MarkerType::Point, newPointMarker))
            deleteMarker(newPointMarker);
    }
    else //create based on hot key with optional presets
    {
        DataEntryEventType *type = rApp->eventTypes.value(eventTypeId);
        DataEntryEventData *data = new DataEntryEventData();
        data->eventType = type;
        data->marker = newPointMarker;
        type->dialog->initialize(data, DataEntryDialog::Mode::Create, noConfirm, hotkeyPresets);
    }


}


void TimelineWindow::adjustPointEvent()
{
    if(mode == DrawingMode::None)
    {
        bool active = false;
        foreach(DataEntryEventData *data, rApp->eventData)
            if(data->marker->selected())
                if(data->eventType->markerType == DataEntryEventType::MarkerType::Point)
                {
                    activePoint = qobject_cast<QCPItemLine*>(data->marker);
                    active = true;
                }

        if(!active) //either nothing selected, or selected marker is wrong type
            return;

        mode = DrawingMode::EditingPoint;
    }
    else if(mode == DrawingMode::EditingPoint)
    {
        mode = DrawingMode::None;

        foreach(DataEntryEventData *data, rApp->eventData)
            if(data->marker == activePoint)
            {
                DatabaseUtility::update(data->storageId, data->toInternalJson());
                rApp->tables.value(data->eventType->id)->updateRow(data);
                rApp->tree->load();
            }
    }
}


void TimelineWindow::adjustDurationMarkerLeft()
{
    adjustDurationMarker(DurationEditMode::Left);
}


void TimelineWindow::adjustDurationMarkerRight()
{
    adjustDurationMarker(DurationEditMode::Right);
}


void TimelineWindow::adjustDurationMarker(DurationEditMode side)
{

    if(mode == DrawingMode::None)
    {
        bool active = false;
        foreach(DataEntryEventData *data, rApp->eventData)
            if(data->marker->selected())
                if(data->eventType->markerType == DataEntryEventType::MarkerType::Duration)
                {
                    activeDuration = qobject_cast<QCPItemRect*>(data->marker);
                    active = true;
                }

        if(!active) //either nothing selected, or selected marker is wrong type
            return;

        if(side == DurationEditMode::Left)
            durationOrigin = activeDuration->bottomRight->coords().x();
        else
            durationOrigin = activeDuration->topLeft->coords().x();

        mode = DrawingMode::EditingDuration;

    }
    else if(mode == DrawingMode::EditingDuration)
    {
        mode = DrawingMode::None;

        foreach(DataEntryEventData *data, rApp->eventData)
            if(data->marker == activeDuration)
            {
                DatabaseUtility::update(data->storageId, data->toInternalJson());
                rApp->tables.value(data->eventType->id)->updateRow(data);
                rApp->tree->load();
            }

        //activeDuration->setSelected(false);

    }
}



QCPItemLine* TimelineWindow::addPointMarkerAt(double mark)
{
    QCPItemLine* newPointMarker = new QCPItemLine(ui->customPlot);
    newPointMarker->start->setCoords(mark, ui->customPlot->yAxis->range().lower);
    newPointMarker->end->setCoords(mark, ui->customPlot->yAxis->range().upper);
    connect(newPointMarker, &QCPAbstractItem::selectionChanged, this, &TimelineWindow::selectionStateChanged );

    newPointMarker->setLayer("point");

    return newPointMarker;

}


void TimelineWindow::itemSelected(QCPAbstractItem* item)
{
    foreach(DataEntryEventData *data, rApp->eventData)
        if(data->marker == item)
        {
            showMessage(data->getSummary());
            if(rApp->dataTableTabs->isVisible())
                 rApp->tables.value(data->eventType->id)->selectRow(data);

            if(rApp->tree->isVisible())
                 rApp->tree->selectItem(data);
        }

    foreach(EmDataSensorEvent *sensor, rApp->emData->sensorEvents)
        if(sensor->marker == item)
        {
            qDebug() << sensor->getSummary();
            showMessage(sensor->getSummary());
        }




}


void TimelineWindow::setCursorPosition()
{

    if(ui->customPlot->hasItem(cursorLine))
        ui->customPlot->removeItem(cursorLine);

    cursorLine = new QCPItemLine(ui->customPlot);
    //cursorLine->setHead(QCPLineEnding::esDiamond);
    cursorLine->setAntialiased(true);
    QPen pen(Qt::black, 1, Qt::SolidLine);
    if(QDateTime::currentDateTime().currentMSecsSinceEpoch()/500 % 2 == 0)
       pen = QPen(Qt::black, 1, Qt::DotLine);
    cursorLine->setPen(pen);
    cursorLine->setSelectable(false);

    cursorLine->start->setCoords(cursor,ui->customPlot->yAxis->range().lower);
    cursorLine->end->setCoords(cursor,ui->customPlot->yAxis->range().upper);
    cursorLine->setClipToAxisRect(true);
    if(ui->customPlot->layer("cursor") != 0)
        cursorLine->setLayer("cursor");

    //TODO config file for date formats
    ui->lblCursorDateTime->setText(QDateTime::fromMSecsSinceEpoch(cursor*1000).toUTC().toString("MM/dd/yyyy hh:mm:ss Z"));
}



void TimelineWindow::syncVideo()
{
    if(rApp == NULL)
        return;

    QList<int> activeChannels;

    foreach(EmDataVideoDuration* v, rApp->emData->videosByName)
    {
        //qDebug() << cursor;
        if( cursor >= v->startSeconds && cursor <= v->endSeconds)
        {
            activeChannels.append(v->videoChannel);
            //qDebug() << "setting start seconds to: " << v->startSeconds;
            rApp->players[v->videoChannel]->setState(v->fileName, v->startSeconds, cursor, speedRatio);

            if(playing)
                rApp->players[v->videoChannel]->play();
            else
                rApp->players[v->videoChannel]->pause();

        }
    }


         foreach(int x, rApp->players.keys())
            if(!activeChannels.contains(x))
                rApp->players[x]->setIdle();


}

void TimelineWindow::syncImages()
{
    if(rApp == NULL)
        return;

    QList<int> activeChannels;
    foreach(EmDataStillImageDuration* v, rApp->emData->stillImagesByName)
    {
        if( cursor >= v->startSeconds && cursor <= v->endSeconds)
        {
            activeChannels.append(v->channel);
            //qDebug() << "setting start seconds to: " << v->startSeconds;
            rApp->stillImageViewers[v->channel]->setImage(rApp->currentEmDataDirectory + "/" + v->fileName);
        }
    }

    foreach(int x, rApp->stillImageViewers.keys())
        if(!activeChannels.contains(x))
            rApp->stillImageViewers[x]->clear();


}



void TimelineWindow::positionChange(QMouseEvent* mouseEvent)
{
    if(mouseEvent->button() == Qt::MouseButton::LeftButton
       && !ui->customPlot->legend->outerRect().contains(mouseEvent->pos()))
    {

        if(rApp->timelineSnapSeconds > 0)
            cursor = std::round(ui->customPlot->xAxis->pixelToCoord(mouseEvent->pos().x())/rApp->timelineSnapSeconds)*rApp->timelineSnapSeconds;
        else
            cursor = ui->customPlot->xAxis->pixelToCoord(mouseEvent->pos().x());

         foreach(int x, rApp->players.keys())
            rApp->players[x]->positionChange();

    }
}


void TimelineWindow::setRange(QCPRange newRange)
{
    //prevent zooming to less than a second
    if(std::floor(newRange.lower) == std::floor(newRange.upper))
    {
        ui->customPlot->xAxis->setRangeLower(std::floor(newRange.lower));

        if(std::floor(newRange.lower) + 1 > secondsRange.upper)
            ui->customPlot->xAxis->setRangeUpper(secondsRange.upper);
        else
            ui->customPlot->xAxis->setRangeUpper(std::floor(newRange.lower) + 1);
    }


    //prevent walking off of the end of the dataset
    if(newRange.lower < secondsRange.lower)
        ui->customPlot->xAxis->setRangeLower(secondsRange.lower);

    if(newRange.upper > secondsRange.upper)
        ui->customPlot->xAxis->setRangeUpper(secondsRange.upper);
}




void TimelineWindow::update()
{

    //calc time since last update and convert the interval into fractional seconds
    if(playing)
    {
       if(cursor < secondsRange.upper)
            cursor += (speedRatio*lastUpdate.msecsTo(QDateTime::currentDateTime())/1000.0);
       else
           togglePause();

       ui->btnPlay->setIcon(pauseIcon);
    }
    else
        ui->btnPlay->setIcon(playIcon);

    setCursorPosition();

    //scroll window if we are playing, scroll lock is checked, and we aren't at the end yet
    if(playing && ui->chkLockCursor->isChecked() && ui->customPlot->xAxis->range().upper < secondsRange.upper)
        ui->customPlot->xAxis->moveRange(speedRatio*lastUpdate.msecsTo(QDateTime::currentDateTime())/1000.0);

    lastUpdate = QDateTime::currentDateTime();

    //QElapsedTimer t;
    //t.start();

    //confirm that video is in sync
    //if(rApp->emData->videosByStart.contains(qFloor(cursor)))

    syncVideo();
    syncImages();
    syncMap();
    updateActiveDurationKeyboard();



    ui->customPlot->replot();

    //qDebug() << " replot completed in " << t.elapsed() << " milliseconds";
    //qDebug() << (playing ? "play":"pause");
}




void TimelineWindow::showMessage(QString m)
{
    ui->statusbar->showMessage(m);
}



void TimelineWindow::appendMessage(QString m)
{
    ui->statusbar->showMessage(ui->statusbar->currentMessage() + m);
}


void TimelineWindow::syncMap()
{
    if(rApp == NULL)
        return;

    QGeoCoordinate* g = getClosestCoordinateAtTime(cursor);

    if(g == NULL)
        return;

    QMetaObject::invokeMethod(rApp->map, "setCursor",
           Q_ARG(QVariant, QVariant::fromValue(g->latitude())),
           Q_ARG(QVariant, QVariant::fromValue(g->longitude())));
}


QGeoCoordinate* TimelineWindow::getClosestCoordinateAtTime(double t)
{
    QMap<double, QGeoCoordinate*>::const_iterator i = rApp->emData->mapSegmentsByStart.lowerBound(t);

    //we've reached the end of the map segements or are still at the beginning
    if(i == rApp->emData->mapSegmentsByStart.constEnd() || i == rApp->emData->mapSegmentsByStart.constBegin())
        return NULL;

    double k1 = i.key();
    double k2 = 0;
    if (k1 > t) //get the value before
    {
        i--;
        k2 = i.key();
    }

    double k = std::abs(k1-t) > std::abs(k2-t) ? k2 : k1;

    return rApp->emData->mapSegmentsByStart.value(k);
}


void TimelineWindow::deleteAllImportedData()
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "Delete All Imported Event Data",
                                                                "Are you sure you would like to delete all imported event data?",
                                                                QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::No);
    if (resBtn == QMessageBox::Yes)
    {
        int n = 1;
        QList<int> ids;
        foreach(DataEntryEventData *data, rApp->eventData)
            if(data->createdByAutomation)
            {
                deleteMarker(data->marker);
                ids.append(data->storageId);
                rApp->eventData.removeOne(data);
                showMessage(QString("deleted %1 events").arg(n++));
            }

         DatabaseUtility::batchRemove(ids);

         //reload data tables
         if(rApp->dataTableTabs->isVisible())
         {
             foreach(EventDataTable *w, rApp->tables)
                 w->load();
         }

    }
    else
        return;

}



void TimelineWindow::showHideTables()
{


    if(!rApp->dataTableTabs->isVisible())
    {
        rApp->dataTableTabs->setVisible(true);
        foreach(EventDataTable *w, rApp->tables)
            w->load();
    }
    else
        rApp->dataTableTabs->setVisible(false);

}

void TimelineWindow::showHideTree()
{


    if(!rApp->tree->isVisible())
    {
        rApp->tree->setVisible(true);
        rApp->tree->load();
    }
    else
        rApp->tree->setVisible(false);

}

void TimelineWindow::closeVideoChannel(int x)
{
    /*
    QAction *channelToggle = new QAction();
    channelToggle->setCheckable(true);
    channelToggle->setChecked(true);
    channelToggle->setText(rApp->emData->videoChannelNames[x]);
    connect(channelToggle, &QAction::toggled, rApp->players[x], &VideoWindow::setInUse);
    //connect(channelToggle, &QAction::toggled(false), rApp->players[x], &VideoWindow::hide());
    ui->menuVideo->addAction(channelToggle);
    */
    foreach(QAction *a, ui->menuVideo->actions())
        if(a->text() == rApp->emData->videoChannelNames[x])
            a->setChecked(false);

}

void TimelineWindow::openEmFilesAndValidate()
{
    //see if we have a template
    if(rApp->currentTemplate.isEmpty())
    {
        QMessageBox::StandardButton resBtn = QMessageBox::question( this, "No Template",
                                                                    "No template selected - would you like to set one now?",
                                                                    QMessageBox::No | QMessageBox::Yes,
                                                                    QMessageBox::Yes);
        if (resBtn == QMessageBox::Yes)
        {
            setTemplate();
            //if we failed to set the template, can't proceed
            if(rApp->currentTemplate.isEmpty())
                return;
        }
        else
            return;
    }

    //find em data directory
    QString curDir = rApp->settings->value("last_em_data_directory", QDir::currentPath()).toString();
    qDebug() << "suggesting em dir" << curDir;
    QString selection = QFileDialog::getExistingDirectory(this, "Select EM data directory", curDir);
    qDebug() << "user selected" << selection;
    if(selection.isEmpty())
        return;

    rApp->currentEmDataDirectory = selection;
    rApp->settings->setValue("last_em_data_directory", rApp->currentEmDataDirectory);
    //qDebug() << "saved " << rApp->currentEmDataDirectory;



    //we are going to try and load some data, so clean up
    setWindowTitle("Timeline");
    ui->menuData->setEnabled(false);
    ui->menuVideo->setEnabled(false);
    toggleShortcuts(false);
    cursor = 0;

    //delete and replace the plot
    delete ui->customPlot;
    ui->customPlot = new QCustomPlot(ui->centralwidget);
    ui->gridLayout->addWidget(ui->customPlot,0,0,1,7);

    //cleanup the map
    QMetaObject::invokeMethod(rApp->map, "clearMap");

    //delete the dialogs and links
    foreach(int x, rApp->dataDialogs.keys())
        delete rApp->dataDialogs[x];
    rApp->dataTableTabs->clear();
    rApp->dataDialogs.clear();
    rApp->eventTypes.clear();
    rApp->eventData.clear();

    //remove the video players
    foreach(int x, rApp->players.keys())
        delete rApp->players[x];
    rApp->players.clear();

    //clear the channels
    foreach(QAction *sub, ui->menuVideo->actions())
        ui->menuVideo->removeAction(sub);

    //remove the still image viewers
    foreach(int x, rApp->stillImageViewers.keys())
        delete rApp->stillImageViewers[x];
    rApp->stillImageViewers.clear();

    QStringList errorMessages;

    //does the template define a database file name? if not, ask the user
    QSettings tmplt(rApp->currentTemplate, QSettings::IniFormat);
    tmplt.beginGroup("review_template");
    QString dbFileName = tmplt.value("database_file/name").toString();
    tmplt.endGroup();
    if(!dbFileName.isEmpty())
        rApp->currentDatabaseFile = QString("%1/%2").arg(rApp->currentEmDataDirectory).arg(dbFileName);
    else
    {
        DatabaseSelectOrCreateFileDialog dbDialog;
        dbDialog.setParent(this);
        dbDialog.setModal(true);
        dbDialog.setAcceptMode(QFileDialog::AcceptSave);
        dbDialog.setConfirmOverwrite(false);
        dbDialog.setDirectory(rApp->currentEmDataDirectory);
        dbDialog.setFileMode(QFileDialog::AnyFile);
        dbDialog.setNameFilter("SQLite3 Files (*.sqlite3)");
        //QString x = dbDialog.getSaveFileFileName(this, "Select a Database File or Create a New One", rApp->currentEmDataDirectory, "SQLite3 Files (*.sqlite3)");
        if(dbDialog.exec() && dbDialog.selectedFiles().length() > 0 && !dbDialog.selectedFiles().at(0).isEmpty())
        {
            qDebug() << "database file set to" << dbDialog.selectedFiles().at(0);
            rApp->currentDatabaseFile = dbDialog.selectedFiles().at(0);
        }
        else //can't continue without a database file
            return;
    }


    //clear all data entry shortcuts (hotkeys)
    foreach(QShortcut* osc, rApp->dataEntryShortcuts)
    {
        osc->setEnabled(false);
        qDebug() << "disabled" << osc->key();
    }

    rApp->dataEntryShortcuts.clear();


    QApplication::setOverrideCursor(Qt::WaitCursor);
    //load the data
    QElapsedTimer t;
    t.start();
    bool loaded = EmDataLoad::load(&errorMessages);
    QApplication::restoreOverrideCursor();

    if(!loaded)
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        QString message = QString("Data load failed:\n");
        foreach(QString e, errorMessages)
            message.append(QString("%1\n").arg(e));
        msgBox.setText(message);
        msgBox.exec();
        return;
    }

    //check to see if the database was created by an older version or template
    QString databaseSoftwareVersion = DatabaseUtility::retrieveMetadata("SOFTWARE_VERSION");
    QString databaseTemplateName = DatabaseUtility::retrieveMetadata("TEMPLATE_NAME");
    QString databaseTemplateVersion = DatabaseUtility::retrieveMetadata("TEMPLATE_VERSION");

    //only perform this check if name and version have been set in the template and version has been loaded from the res.qrc file
    if(rApp->version.length() > 0 && rApp->templateName.length() > 0 && rApp->templateVersion.length() > 0)
    {
        //is there a mismatch
        if(databaseSoftwareVersion != rApp->version || databaseTemplateName != rApp->templateName || databaseTemplateVersion != rApp->templateVersion )
        {
            QString versionWarning;
            //if we have database metadata be specific, otherwise be vauge
            if(databaseSoftwareVersion.length() > 0 && databaseTemplateName.length() > 0 && databaseTemplateVersion.length() > 0)
                versionWarning = QString("The current software version is %1 and the selected template is %2 version %3. "
                               "The existing event database in the selected EM data directory was created using software "
                               "version %4 and template %5 version %6. Are you sure you would like to proceed?")
                               .arg(rApp->version).arg(rApp->templateName).arg(rApp->templateVersion)
                               .arg(databaseSoftwareVersion).arg(databaseTemplateName).arg(databaseTemplateVersion);
            else
                versionWarning = QString("The current software version is %1 and the selected template is %2 version %3. "
                               "The existing event database in the selected EM data directory was created using an unknown software and template version. "
                               "Are you sure you would like to proceed?")
                               .arg(rApp->version).arg(rApp->templateName).arg(rApp->templateVersion);

            QMessageBox::StandardButton resBtn = QMessageBox::question( this, "Database Version Warning",
                                                                        versionWarning,
                                                                        QMessageBox::No | QMessageBox::Yes,
                                                                        QMessageBox::No);
            //dump the dataload and cancel
            if (resBtn == QMessageBox::No)
            {
                qDebug() << "canceling due to database version warning";
                rApp->emData = new EmData();
                return;
            }
        }
    }


    //check for missing video files
     if(!rApp->videoFromUrl)
     {
        QStringList missingFiles;
        foreach(QString fn, rApp->emData->videosByName.keys())
        {
            QFileInfo fi(rApp->currentEmDataDirectory + "/" + fn);
            if(!fi.exists())
                missingFiles.append(rApp->currentEmDataDirectory + "/" + fn);
        }

        if(missingFiles.length() > 0)
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            //TODO constants
            QString message = QString("Some video files could not be located");
            qDebug() << "missing video files:" << missingFiles.join(", ");
            msgBox.setText(message);
            msgBox.setDetailedText(missingFiles.join("\n"));
            msgBox.exec();
        }
     }

    valuesRange = QCPRange(rApp->emData->minValue, rApp->emData->maxValue);

    if(rApp->emData->series.count() > 0)
        appendMessage(QString("%1 line csv parsed in %2 milliseconds")
                  .arg(rApp->emData->series[0]->seconds.size()).arg(t.elapsed()));

    t.restart();
    //setup the map

    QMetaObject::invokeMethod(rApp->map, "setTrack",
            Q_ARG(QVariant, QVariant::fromValue(rApp->emData->mapSegmentVariants)));
    appendMessage(QString(" ... map initialized in %1 milliseconds").arg(t.elapsed()));

    t.restart();

    //sort the series by zOrder so that they are drawn in ascending z order
    qSort(rApp->emData->series.begin(), rApp->emData->series.end(), EmDataSeries::compareSeries);


    //determine min and max, get names and colors from emdata object
    //note that we make the max 1 second short on each end, as this improves plot performance
    double min = -1, max = -1;

    foreach(EmDataSeries* s, rApp->emData->series)
    {

        if(s->type == EmDataSeries::LineType::Line)
            addSeriesLine(s->name,s->seconds,s->values,QPen(s->color,s->thickness));

        if(s->type == EmDataSeries::LineType::StepLine)
            addSeriesStepLine(s->name,s->seconds,s->values,QPen(s->color,s->thickness));

        if(s->type == EmDataSeries::LineType::Shade)
            addSeriesShaded(s->name,s->seconds,s->values,QBrush(s->color));

        //determine min and max seconds
        if(min == -1 || min > s->seconds.first()+1)
            min = s->seconds.first()+1;
        if(max == -1 || max < s->seconds.last()-1)
            max = s->seconds.last()-1;

    }

    foreach(EmDataSensorEvent *e, rApp->emData->sensorEvents)
    {

        if(qobject_cast<EmDataSensorDurationEvent *>(e))
        {
            EmDataSensorDurationEvent *de = qobject_cast<EmDataSensorDurationEvent *>(e);
            de->marker = addSensorDurationEvent(de->beginSeconds, de->endSeconds, de->value);
            de->setMarkerStyle();

            //determine min and max seconds
            if(min == -1 || min > de->beginSeconds + 1)
                min = de->beginSeconds+1;
            if(max == -1 || max < de->endSeconds - 1)
                max = de->endSeconds - 1;
        }

    }

    foreach(EmDataEnumEvent *e, rApp->emData->enumEvents)
    {
        e->marker = addEnumEvent(e->seconds, e->position, e->color, e->symbol, e->size);

        //determine min and max seconds
        if(min == -1 || min > e->seconds + 1)
            min = e->seconds + 1;
        if(max == -1 || max < e->seconds - 1)
            max = e->seconds - 1;
    }

    secondsRange = QCPRange(min, max);

    setupPlot();

    //add saved events
    foreach(DataEntryEventData *eventData, rApp->eventData)
    {
        if(eventData->eventType->markerType == DataEntryEventType::MarkerType::Duration)
            eventData->marker = addDurationMarkerAt(eventData->getStorageStartSeconds(), eventData->getStorageEndSeconds());
        else if(eventData->eventType->markerType == DataEntryEventType::MarkerType::Point)
            eventData->marker = addPointMarkerAt(eventData->getStorageSeconds());

        eventData->setMarkerStyle();
    }


    appendMessage(QString(" ... sensor plots initialized in %1 milliseconds").arg(t.elapsed()));


    //setup players
    for(int x = 0; x < rApp->emData->videoChannels; x++)
    {
        rApp->players.insert(x, new VideoWindow(x));
        rApp->players[x]->showNormal();
        if(rApp->settings->contains(QString("geometry_video_%1").arg(x)))
            rApp->players[x]->restoreGeometry(rApp->settings->value(QString("geometry_video_%1").arg(x)).toByteArray());
        else
            rApp->players[x]->move(x*Constants::VIDEO_WINDOW_OFFSETS, x*Constants::VIDEO_WINDOW_OFFSETS);

        QAction *channelToggle = new QAction();
        channelToggle->setCheckable(true);
        channelToggle->setChecked(true);
        channelToggle->setText(rApp->emData->videoChannelNames[x]);
        connect(channelToggle, &QAction::toggled, rApp->players[x], &VideoWindow::setInUse);
        //connect(channelToggle, &QAction::toggled(false), rApp->players[x], &VideoWindow::hide());
        ui->menuVideo->addAction(channelToggle);


    }

    //setup still image viewers
    for(int x = 0; x < rApp->emData->stillImageChannels; x++)
    {
        rApp->stillImageViewers.insert(x, new StillImageWindow());
        rApp->stillImageViewers[x]->showNormal();

        if(rApp->settings->contains(QString("geometry_still_%1").arg(x)))
            rApp->stillImageViewers[x]->restoreGeometry(rApp->settings->value(QString("geometry_still_%1").arg(x)).toByteArray());
        else
            rApp->stillImageViewers[x]->move(x*Constants::VIDEO_WINDOW_OFFSETS, x*Constants::VIDEO_WINDOW_OFFSETS);
    }

    //restore geometry and load data tables
    foreach(int x, rApp->tables.keys())
    {
        //rApp->tables.value(x)->restoreGeometry(rApp->settings->value(QString("geometry_data_table_tab_%1").arg(x)).toByteArray());

        //if the tables are visible, load them
        if(rApp->dataTableTabs->isVisible())
            rApp->tables.value(x)->load();
    }

    setWindowTitle(QString("Timeline: %1").arg(rApp->currentEmDataDirectory));


    //set the slider min max
    ui->sldrPlaybackSpeed->setMaximum(rApp->playbackMaxFactor*rApp->speedSteps);
    ui->sldrPlaybackSpeed->setMinimum(rApp->playbackMinFactor*rApp->speedSteps);


    //adjust order of timeline and video windows
    raise();
    setWindowState(Qt::WindowActive);
    toggleShortcuts(true);
    ui->menuData->setEnabled(true);
    ui->menuVideo->setEnabled(true);

    beginPlayback();
}

/*
void TimelineWindow::openEmFilesSilent()
{

    //delete and replace the plot
    delete ui->customPlot;
    ui->customPlot = new QCustomPlot(ui->centralwidget);
    ui->gridLayout->addWidget(ui->customPlot,0,0,1,8);

    //cleanup the map
    QMetaObject::invokeMethod(rApp->map, "clearMap");

    //delete the dialogs and links
    foreach(int x, rApp->dataDialogs.keys())
        delete rApp->dataDialogs[x];
    rApp->dataTableTabs->clear();
    rApp->dataDialogs.clear();
    rApp->eventTypes.clear();
    rApp->eventData.clear();

    //remove the video players
    foreach(int x, rApp->players.keys())
        delete rApp->players[x];
    rApp->players.clear();

    //remove the still image viewers
    foreach(int x, rApp->stillImageViewers.keys())
        delete rApp->stillImageViewers[x];
    rApp->stillImageViewers.clear();


    //load the data
    bool loaded = EmDataLoad::load();

    if(!loaded)
    {
        qCritical() << "failed to load" << rApp->currentEmDataDirectory;
        return;
    }


    valuesRange = QCPRange(rApp->emData->minValue, rApp->emData->maxValue);


    //setup the map
    QMetaObject::invokeMethod(rApp->map, "setTrack",
            Q_ARG(QVariant, QVariant::fromValue(rApp->emData->mapSegmentVariants)));

    //sort the series by zOrder so that they are drawn in ascending z order
    qSort(rApp->emData->series.begin(), rApp->emData->series.end(), EmDataSeries::compareSeries);


    //determine min and max, get names and colors from emdata object
    //note that we make the max 1 second short on each end, as this improves plot performance
    double min = -1, max = -1;

    foreach(EmDataSeries* s, rApp->emData->series)
    {

        if(s->type == EmDataSeries::LineType::Line)
            addSeriesLine(s->name,s->seconds,s->values,QPen(s->color));

        if(s->type == EmDataSeries::LineType::StepLine)
            addSeriesStepLine(s->name,s->seconds,s->values,QPen(s->color));

        if(s->type == EmDataSeries::LineType::Shade)
            addSeriesShaded(s->name,s->seconds,s->values,QBrush(s->color));

        //determine min and max seconds
        if(min == -1 || min > s->seconds.first()+1)
            min = s->seconds.first()+1;

        if(max == -1 || max < s->seconds.last()-1)
            max = s->seconds.last()-1;

    }

    foreach(EmDataSensorEvent *e, rApp->emData->sensorEvents)
    {

        if(qobject_cast<EmDataSensorDurationEvent *>(e))
        {
            EmDataSensorDurationEvent *de = qobject_cast<EmDataSensorDurationEvent *>(e);
            de->marker = addSensorDurationEvent(de->beginSeconds, de->endSeconds, de->value);
            de->setMarkerStyle();

            //determine min and max seconds
            if(min == -1 || min > de->beginSeconds + 1)
                min = de->beginSeconds+1;

            if(max == -1 || max < de->endSeconds - 1)
                max = de->endSeconds - 1;
        }

    }

    secondsRange = QCPRange(min, max);

    setupPlot();

    //add saved events
    foreach(DataEntryEventData *eventData, rApp->eventData)
    {
        if(eventData->eventType->markerType == DataEntryEventType::MarkerType::Duration)
            eventData->marker = addDurationMarkerAt(eventData->beginSeconds, eventData->endSeconds);
        else if(eventData->eventType->markerType == DataEntryEventType::MarkerType::Point)
            eventData->marker = addPointMarkerAt(eventData->seconds);

        eventData->setMarkerStyle();
    }


    showMessage(QString("loaded %1").arg(rApp->currentEmDataDirectory));

    setWindowTitle(QString("Timeline: %1").arg(rApp->currentEmDataDirectory));

    //set the slider min max
    ui->sldrPlaybackSpeed->setMaximum(rApp->playbackMaxFactor);
    ui->sldrPlaybackSpeed->setMinimum(rApp->playbackMinFactor);


    //adjust order of timeline and video windows
    raise();
    setWindowState(Qt::WindowActive);

}
*/

void TimelineWindow::setPostionFromMap(double lat, double lon)
{
    //qDebug() << "lat: " << lat << " lon: " << lon;
    if(rApp->emData->spatialIndex.size() == 0)
        return;

    std::vector<std::pair<Constants::Coordinates,long>> result;
    rApp->emData->spatialIndex.query(boost::geometry::index::nearest(Constants::Coordinates(lat, lon),1),std::back_inserter(result));

    double c = 0;
    if(result.size() > 0)
    {
        if(result.at(0).second < secondsRange.lower)
            c = secondsRange.lower;
        else if (result.at(0).second > secondsRange.upper)
            c = secondsRange.upper;
        else
            c = result.at(0).second;


        if(rApp->timelineSnapSeconds > 0)
            cursor = std::round(c/rApp->timelineSnapSeconds)*rApp->timelineSnapSeconds;
        else
            cursor = c;


        //if(!cursorIsVisible() || cursorIsLocked())
        //    centerCursor();
    }
}


void TimelineWindow::deleteMarker(QCPAbstractItem *item)
{
    ui->customPlot->removeItem(item);
}



void TimelineWindow::centerCursor()
{
      QCPRange currentRange = ui->customPlot->xAxis->range();
      double halfSpan = (currentRange.upper - currentRange.lower)/2;

      //make sure we have enough graph to center the cursor
      if(cursor - halfSpan < secondsRange.lower)
         halfSpan = cursor - secondsRange.lower;

      if(cursor + halfSpan > secondsRange.upper)
         halfSpan = secondsRange.upper - cursor;

      if(halfSpan < 0)
          halfSpan = 0;

     ui->customPlot->xAxis->setRange(QCPRange(cursor-halfSpan, cursor+halfSpan));

}


void TimelineWindow::lockCursor()
{
    //TODO maybe locking the cursor implies keeping it centered??
    //ui->btnCenterCursor->setEnabled(ui->chkLockCursor->checkState() != Qt::Checked);
}




void TimelineWindow::speedNormal(){
    ui->sldrPlaybackSpeed->setValue(0);
    setPlaybackSpeed();
}
void TimelineWindow::speedDouble(){
    ui->sldrPlaybackSpeed->setValue(ui->sldrPlaybackSpeed->value()+ui->sldrPlaybackSpeed->singleStep());
    setPlaybackSpeed();
}
void TimelineWindow::speedHalf(){
    ui->sldrPlaybackSpeed->setValue(ui->sldrPlaybackSpeed->value()-ui->sldrPlaybackSpeed->singleStep());
    setPlaybackSpeed();
}

void TimelineWindow::jumpForwardHalf(){
   cursor += .5;
}
void TimelineWindow::jumpBackHalf(){
   cursor += .5;
}
void TimelineWindow::jumpForward1(){
   cursor += 1;
}
void TimelineWindow::jumpBack1(){
   cursor -= 1;
}
void TimelineWindow::jumpForward3(){
   cursor += 3;
}
void TimelineWindow::jumpBack3(){
   cursor -= 3;
}
void TimelineWindow::jumpForward5(){
   cursor += 5;
}
void TimelineWindow::jumpBack5(){
   cursor -= 5;
}





