#ifndef DATABASESELECTORCREATEFILEDIALOG_H
#define DATABASESELECTORCREATEFILEDIALOG_H
#include <QFileDialog>

class DatabaseSelectOrCreateFileDialog : public QFileDialog
{
    Q_OBJECT

public:
    DatabaseSelectOrCreateFileDialog();

public slots:
    void setButtonText();
};

#endif // DATABASESELECTORCREATEFILEDIALOG_H
