#include "stillimageview.h"
#include <QWheelEvent>

StillImageView::StillImageView(QWidget *parent) : QGraphicsView(parent)
{
    //this will force a hand cursor
    setDragMode(QGraphicsView::ScrollHandDrag);
}


void StillImageView::wheelEvent(QWheelEvent* event) {
    //zoom rate
    double factor = 1.05;
    double scaleTo = 1;
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    if(event->delta() > 0)
        scaleTo = factor;
    else if(event->delta() < 0)
        scaleTo = 1/factor;
    scale(scaleTo,scaleTo);

}


