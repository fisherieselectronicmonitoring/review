#ifndef MAPCONFIG_H
#define MAPCONFIG_H

#include <QObject>

class MapConfig : public QObject
{
    Q_OBJECT
public:
    explicit MapConfig(QObject *parent = 0);
    Q_INVOKABLE QString getHost();
    Q_INVOKABLE QString getImageFormat();
    QString host;
    QString imageFormat;
private:


signals:

public slots:
};

#endif // MAPCONFIG_H
