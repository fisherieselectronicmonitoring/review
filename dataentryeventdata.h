#ifndef DATAENTRYEVENTDATA_H
#define DATAENTRYEVENTDATA_H

#include "dataentryeventtype.h"
#include "qcustomplot.h"

#include <QObject>
#include <QJsonDocument>
#include <QVariantMap>
#include <QGeoCoordinate>

class DataEntryEventData : public QObject
{
    Q_OBJECT
public:
    explicit DataEntryEventData(QObject *parent = 0);
    DataEntryEventType *eventType; //store id of type
    bool createdByAutomation; //if this event was created by a reviewer or by analytics
    /*
    QList<int> fieldsRequired; //these must always have values (although template changes could let them slip)
    QList<int> fieldsRequiredOnExport; //which fields must have values prior to export
    QList<int> fieldsWarnOnExport; //warn on export if fields don't have values
    */

    QMap<int, QVariant> data;
    QCPAbstractItem *marker;
    QString getSummary();
    QString getTreeViewSummary();
    QString getHeader();
    QList<QString> getHeaderList();
    //QList<QString> toStringList();
    QList<QVariant> toVariantList();
    QString toCsv();
    QJsonDocument toExportJson();
    QJsonDocument toInternalJson();
    bool fromJson(QJsonDocument, int storageId = -1);
    int storageId; //key to serialized form on disk, -1 for objects not yet serialized

    //get the position of the marker
    double getMarkerStartSeconds(); //the left side of duration events, or the position of point event in seconds
    double getMarkerEndSeconds(); //the right side of duration events, or the position of point event in seconds


    //used to rehydrate from json
    double getStorageStartSeconds();
    double getStorageEndSeconds();
    double getStorageSeconds();

    //sort events by type and then by chronological order of markers
    static bool compare(DataEntryEventData *d1, DataEntryEventData *d2);

    void setMarkerStyle();
    

    DataEntryEventData* getParent();

private:
    QString getTimeDescription();
    double getClosestCoordinateReportTimeAtTime(double);
    QMap<double, QGeoCoordinate*>::const_iterator getCoordinateIter(double);
    double getTraveledDistanceKm();
    double getStraightLineDistanceKm();
    QString getStartDateTimeFormatted();
    QString getEndDateTimeFormatted();
    qint64 getDurationSeconds();
    QString getDuration8601();

    double getStartLat();
    double getStartLon();
    double getEndLat();
    double getEndLon();

    QString getStartLatDdm();
    QString getStartLonDdm();
    QString getEndLatDdm();
    QString getEndLonDdm();

    QVariantMap toVariantMap(bool internal);  //will generate either name:value pairs or key:value pairs depending on args

    //used only to marshall/unmarshall from the database
    double seconds;
    double startSeconds;
    double endSeconds;


signals:

public slots:
};

#endif // DATAENTRYEVENTDATA_H
