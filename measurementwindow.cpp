#include "measurementwindow.h"
#include "measurementview.h"
#include "measurementutility.h"
#include "ui_measurementwindow.h"
#include "reviewapplication.h"

#include <QDesktopWidget>
#include <QDebug>



MeasurementWindow::MeasurementWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MeasurementWindow)
{
    ui->setupUi(this);
    scene = new MeasurementScene();
    view = new MeasurementView();
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    view->setRenderHints(QPainter::Antialiasing);
    ui->gridLayout->addWidget(view,0,0,1,1);
    view->setScene(scene);
    //this will only work if we don't allow dragging
    view->setCursor(QCursor(Qt::CrossCursor));
    setWindowTitle("Measurement");

    connect(ui->btnApplyCalibration, &QPushButton::clicked, this, &MeasurementWindow::applyCameraCalibration);
    connect(ui->btnRectify, &QPushButton::clicked, this, &MeasurementWindow::rectify);
    connect(ui->sldrBorderFactor, &QSlider::valueChanged, this,&MeasurementWindow::update);
    connect(ui->btnCreateEvent, &QPushButton::clicked, this, &MeasurementWindow::addPointMarker);
    connect(ui->btnClose, &QPushButton::clicked, this, &MeasurementWindow::close);

    //load the last used dimensions
    ui->txtWidth->setText(rApp->settings->value("measurement_width").toString());
    ui->txtHeight->setText(rApp->settings->value("measurement_height").toString());
    ui->sldrBorderFactor->setValue(rApp->settings->value("measurement_border_factor").toInt());

    setTabOrder(ui->txtWidth,ui->txtHeight);
    setTabOrder(ui->txtHeight,ui->sldrBorderFactor);
    setTabOrder(ui->sldrBorderFactor,ui->btnRectify);

    ui->txtHeight->setValidator(new QDoubleValidator(this));
    ui->txtWidth->setValidator(new QDoubleValidator(this));

    restoreGeometry(rApp->settings->value(QString("geometry_measurement")).toByteArray());
    //disiable the rectification button until points are added and length width entered
    //ui->btnRectify->setEnabled(false);
    //disable the save button until a measurement line has been created
    //ui->btnSave->setEnabled(false);
}

void MeasurementWindow::initialize(QString path)
{
    scene->loadCameraCalibration();
    ui->statusbar->showMessage(QString("loaded %1 camera calibrations").arg(scene->getCalibrationCount()) );
    ui->btnApplyCalibration->setEnabled(scene->getCalibrationCount());
    ui->btnApplyCalibration->setVisible(true);
    ui->btnRectify->setVisible(true);
    ui->btnCreateEvent->setVisible(true);
    ui->lblBorder->setVisible(true);
    ui->lblBorderFactor->setVisible(true);
    ui->sldrBorderFactor->setVisible(true);
    ui->lblHeight->setVisible(true);
    ui->lblWidth->setVisible(true);
    ui->txtHeight->setVisible(true);
    ui->txtWidth->setVisible(true);

    scene->setupForRectification(path);
    show();
    view->fitInView(scene->baseImageRect(), Qt::KeepAspectRatio);
}

void MeasurementWindow::initializeCameraCalibration(QString path)
{

    ui->btnApplyCalibration->setVisible(false);
    ui->btnRectify->setVisible(false);
    ui->btnCreateEvent->setVisible(false);
    ui->lblBorder->setVisible(false);
    ui->lblBorderFactor->setVisible(false);
    ui->sldrBorderFactor->setVisible(false);
    ui->lblHeight->setVisible(false);
    ui->lblWidth->setVisible(false);
    ui->txtHeight->setVisible(false);
    ui->txtWidth->setVisible(false);

    scene->setupForRectification(path);

    show();
    view->fitInView(scene->baseImageRect(), Qt::KeepAspectRatio);

    scene->loadCameraCalibration();
    ui->statusbar->showMessage(QString("loaded %1 camera calibrations").arg(scene->getCalibrationCount()) );
}


void MeasurementWindow::initialize(QString path, double seconds)
{
   eventTime = seconds;
   initialize(path);
}





void MeasurementWindow::update()
{
    ui->lblBorderFactor->setText(QString("%1%").arg(ui->sldrBorderFactor->value()));
}


void MeasurementWindow::rectify()
{
    //TODO validate settings
    scene->setRectificationParameters(ui->txtWidth->text().toDouble(),ui->txtHeight->text().toDouble(), ui->sldrBorderFactor->value());

    //persist the settings in the config file
    rApp->settings->setValue("measurement_width",ui->txtWidth->text());
    rApp->settings->setValue("measurement_height",ui->txtHeight->text());
    rApp->settings->setValue("measurement_border_factor", ui->sldrBorderFactor->value());

    if(scene->rectify())
    {
        scene->setupForMeasurement();
        view->fitInView(scene->baseImageRect(), Qt::KeepAspectRatio);
    }
}

void MeasurementWindow::calibrateCamera()
{
    if(scene->calibrateCamera())
        ui->statusbar->showMessage(QString("adding calibration - total of %1 camera calibrations").arg(scene->getCalibrationCount()));
    else
        ui->statusbar->showMessage(QString("unable to find chessboard"));

}

void MeasurementWindow::applyCameraCalibration()
{
    ui->btnApplyCalibration->setEnabled(false);
    ui->statusbar->showMessage(QString("applying calibration"));
    scene->applyCameraCalibration();
    ui->statusbar->showMessage(QString("calibration applied"));


}

void MeasurementWindow::addPointMarker()
{
    DataEntryEventData *data = new DataEntryEventData();
    data->eventType = rApp->measurementEvent;
    data->marker = rApp->timeline->addPointMarkerAt(eventTime);
    rApp->measurementEvent->dialog->initialize(data, DataEntryDialog::Mode::Create);
    rApp->measurementEvent->dialog->setControlValue(rApp->measurementLengthControlId,QString::number(scene->getLastMeasurementCm(),'f',rApp->measurementDecimals));
}


MeasurementWindow::~MeasurementWindow()
{
    delete ui;
}
