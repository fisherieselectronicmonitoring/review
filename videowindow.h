#ifndef VIDEOWINDOW_H
#define VIDEOWINDOW_H

#include <QMainWindow>
#include <QMap>

namespace Ui {
    class VideoWindow;
}

class VlcInstance;
class VlcMedia;
class VlcMediaPlayer;

class VideoWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit VideoWindow(int, QWidget *parent = 0);
    ~VideoWindow();

    void setState(QString, double, double, float);
    void setIdle();
    //void setMrl(QString mrl);
    void pause();
    void play();
    void hideVideo();
    void showVideo();
    void positionChange();
    double getTime();
    bool hasVout();

public slots:

    void setInUse(bool);

private slots:
    void saveImage();
    void measure();
    void calibrateCamera();
    void complete();
    void update();
    void toggleLockPlayback(bool);
    void togglePause();
    //void filter();

private:

    QIcon playIcon;
    QIcon pauseIcon;

    float leftCrop, rightCrop, topCrop, bottomCrop;
    float limitZero(float);
    float limitZeroOne(float);
    void resetCrop();


    Ui::VideoWindow *ui;

    VlcMediaPlayer *player;
    VlcInstance *instance;
    VlcMedia *media;

    QMap<QString, VlcMedia *> mediaByName;

    //TODO combine these into a state enum
    bool playing;
    bool idle;
    bool broken;

    bool filtered;

    int channel;
    double startSeconds;
    QString currentFile;
    qint64 masterOffsetMillis;
    float masterSpeedRatio;
    QVector<qint64> lagLeadBuffer;
    bool positionChanged;

    QString currentTimeString();
    QString currentFilenameTimeString();

protected:
    virtual void wheelEvent(QWheelEvent *event);
    //virtual void keyPressEvent(QKeyEvent *event);
    virtual void closeEvent (QCloseEvent *event);


};

#endif // VIDEOWINDOW_H
