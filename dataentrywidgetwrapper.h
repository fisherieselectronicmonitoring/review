#ifndef DATAENTRYWIDGETWRAPPER_H
#define DATAENTRYWIDGETWRAPPER_H

#include <QObject>
#include <QWidget>

#include "dataentryeventdata.h"

class DataEntryWidgetWrapper : public QObject
{
    Q_OBJECT
public:
    explicit DataEntryWidgetWrapper(QObject *parent = 0);
    enum class WidgetType
      {
         MultiSelect,
         Select,
         Text,
         DateTime,
         TextBox
      };

    WidgetType widgetType;

    //this is the unique id of this control on the form
    int id;
    QString name;
    QString description;
    bool required;
    bool sticky;
    int autoIncrement;
    float order;
    QWidget *widget;
    //used by select and multiselect controls
    QStringList defaultIds;
    //used by text controls
    QString defaultValue;

    void setDefaults();
    void setAutoIncrement();
    void setControlValues(QVariant);
    void getControlValues(DataEntryEventData *);
    bool validate();
    void fixup();

    static bool compare(DataEntryWidgetWrapper *, DataEntryWidgetWrapper *);






signals:

public slots:
};

#endif // DATAENTRYWIDGETWRAPPER_H
