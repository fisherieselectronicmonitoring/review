#ifndef MEASUREMENTVIEW_H
#define MEASUREMENTVIEW_H

#include <QObject>
#include <QGraphicsView>

class MeasurementView : public QGraphicsView
{
    Q_OBJECT
public:
  explicit MeasurementView(QWidget *parent = 0);
  double currentScale;

protected:
    virtual void wheelEvent(QWheelEvent* event);
    void enterEvent(QEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);



};

#endif // MEASUREMENTVIEW_H



