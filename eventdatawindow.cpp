#include "eventdatawindow.h"
#include "ui_eventdatawindow.h"
#include "reviewapplication.h"

EventDataWindow::EventDataWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EventDataWindow)
{
    ui->setupUi(this);
    setWindowTitle("Event Data");
    ui->tabEventData->clear();

    restoreGeometry(rApp->settings->value("geometry_data_tables").toByteArray());
}

EventDataWindow::~EventDataWindow()
{
    delete ui;
}

void EventDataWindow::addTab(QString label, QWidget *w)
{
    ui->tabEventData->addTab(w, label);
}


void EventDataWindow::clear()
{
    ui->tabEventData->clear();
}
