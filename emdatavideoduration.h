#ifndef EMDATAVIDEODURATION_H
#define EMDATAVIDEODURATION_H
#include <QString>
#include <QObject>


class EmDataVideoDuration : public QObject
{
    Q_OBJECT
public:
    explicit EmDataVideoDuration(QObject *parent = 0);
    ~EmDataVideoDuration();
    double startSeconds;
    double endSeconds;
    int videoChannel;
    QString fileName;

signals:

public slots:
};

#endif// EMDATAVIDEODURATION_H
