#ifndef EMDATASENSORPOINTEVENT_H
#define EMDATASENSORPOINTEVENT_H

#include "emdatasensorevent.h"

class EmDataSensorPointEvent : public EmDataSensorEvent
{
        Q_OBJECT
public:
    EmDataSensorPointEvent();
};

#endif // EMDATASENSORPOINTEVENT_H
