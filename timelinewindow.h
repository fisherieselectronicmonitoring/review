#ifndef TIMELINEWINDOW_H
#define TIMELINEWINDOW_H

#include "videowindow.h"
#include "qcustomplot.h"
#include "emdatavideoduration.h"
#include "dataentryeventtype.h"
#include "dataentryeventdata.h"
#include "databaseselectorcreatefiledialog.h"
#include "emdataenumevent.h"
#include "dataentryfieldvalue.h"
#include "nlohmann/json.hpp"
#include <QMainWindow>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QDebug>
#include <QString>
#include <QStandardItemModel>
#include <QList>
#include <QDir>
#include <QGeoCoordinate>
#include <QDateTime>
#include <QRubberBand>


namespace Ui {
class TimelineWindow;
}

class TimelineWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TimelineWindow(QWidget *parent = 0);
    ~TimelineWindow();

    //TODO these should come from config
    const int UPDATE_INTERVAL_MS = 50;
    const int WIDTH = 1400;
    const int HEIGHT = 400;

    void deleteMarker(QCPAbstractItem *);
    void jumpToDateTime(QDateTime);
    QCPItemLine * addPointMarkerAt(double);
    QCPItemRect * addDurationMarkerAt(double, double);

    void closeVideoChannel(int);

private:
    Ui::TimelineWindow *ui;

    void beginPlayback();
    void closeEvent (QCloseEvent *event);

    //current master playback postion in seconds since the epoch
    double cursor;
    bool cursorIsVisible();
    bool cursorIsLocked();
    void setCursorPosition();
    QCPItemLine *cursorLine;

    QIcon playIcon;
    QIcon pauseIcon;

    //duration drawing state
    enum class DrawingMode {
        None,
        CreatingDuration,
        CreatingDurationKeyboard,
        EditingDuration,
        EditingPoint
    };
    enum class DurationEditMode {
        Both, //currently NA
        Left,
        Right
    };
    DrawingMode mode;
    double durationOrigin;
    QCPItemRect* activeDuration;
    QCPItemLine* activePoint;

    //control playback speed
    bool playing;
    QTimer *timer;
    float speedRatio;
    QDateTime lastUpdate;

    //max and min in seconds since the epoch for current plot
    QCPRange secondsRange;
    //overide to display a shorter range
    QCPRange visibleRange;

    //max and min values set from template
    QCPRange valuesRange;



    void addSeriesLine(QString name, QVector<double> x, QVector<double> y, QPen p);
    void addSeriesStepLine(QString name, QVector<double> x, QVector<double> y, QPen p);
    void addSeriesShaded(QString name, QVector<double> x, QVector<double> y, QBrush p);
    QCPItemRect* addSensorDurationEvent(double, double, double);
    QCPGraph* addEnumEvent(double, float, QColor, EmDataEnumEvent::Symbol, float);
    void setupPlot();
    void showMessage(QString);
    void appendMessage(QString);
    void selectItem(QCPAbstractItem *);
    void selectNone();
    void exportStillImages();
    void adjustDurationMarker(DurationEditMode);



    QList<QShortcut *> shortcuts;
    void toggleShortcuts(bool);

    QGeoCoordinate* getClosestCoordinateAtTime(double);

    nlohmann::json buildJsonTree(DataEntryEventData *);

    static bool chronologicalCompare(DataEntryEventData *, DataEntryEventData *);

public slots:
    void setPostionFromMap(double, double);
    void nextFrame();
    void previousFrame();
    void nextDataEntryEvent();
    void previousDataEntryEvent();
    void selectDataEntryEvent(int);
    void nextSensorEvent();
    void previousSensorEvent();
    void addPointMarkerWithEvent(int, bool, QList<DataEntryFieldValue>);

private slots:
    void updateActiveDurationKeyboard();
    void createDurationMarkerKeyboard();
    void isItemSelected();
    void templateInfo();
    void showHideTables();
    void showHideTree();
    void selectionStateChanged();
    void deleteAllImportedData();
    void deleteEvent();
    void openEvent();
    void jumpTo();
    void zoomIn();
    void zoomOut();
    void itemSelected(QCPAbstractItem*);
    void graphSelected(QCPAbstractPlottable*);
    void cancelSelectOrDraw();
    void lockCursor();
    void centerCursor();
    void addPointMarker();
    void addDurationMarker(QMouseEvent*);
    void syncVideo();
    void syncImages();
    void syncMap();
    void setPlaybackSpeed();
    void setTemplate();
    void positionChange(QMouseEvent*);
    void setRange(QCPRange);
    void openEmFilesAndValidate();
    void importData();
    void exportData();
    void exportDataset();
    void openOnlineDoc();
    void about();
    void update();
    void togglePause();
    void legendDoubleClicked(QCPLegend *, QCPAbstractLegendItem *, QMouseEvent *);
    bool showContextMenu(QPoint, DataEntryEventType::MarkerType, QCPAbstractItem *);
    void mouseMove(QMouseEvent*);
    void speedDouble();
    void speedHalf();
    void speedNormal();
    void jumpForwardHalf();
    void jumpForward1();
    void jumpForward3();
    void jumpForward5();
    void jumpBackHalf();
    void jumpBack1();
    void jumpBack3();
    void jumpBack5();
    void adjustDurationMarkerLeft();
    void adjustDurationMarkerRight();
    void adjustPointEvent();
    void showHideUneventfulTimeline();


};

#endif // TIMELINEWINDOW_H
