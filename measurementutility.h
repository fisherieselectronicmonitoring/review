#ifndef MEASUREMENTUTILITY_H
#define MEASUREMENTUTILITY_H

#include <QObject>
#include <QImage>
#include <QPixmap>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"


class MeasurementUtility : public QObject
{
    Q_OBJECT
public:
    explicit MeasurementUtility(QObject *parent = 0);
    static double interpolate(QList<QVector3D>, QPointF);

    static QImage  cvMatToQImage( const cv::Mat & );
    static QPixmap cvMatToQPixmap( const cv::Mat & );
    static cv::Mat qImageToCvMat( const QImage & );
    static cv::Mat qPixmapToCvMat( const QPixmap & );


signals:

public slots:
};

#endif // MEASUREMENTUTILITY_H
