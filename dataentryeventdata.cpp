#include "dataentryeventdata.h"
#include "reviewapplication.h"
#include "formatutility.h"

DataEntryEventData::DataEntryEventData(QObject *parent) : QObject(parent)
{
    //until we save to disk the first time
    storageId = -1;
    //presume creation by user
    createdByAutomation = false;
}

double DataEntryEventData::getStorageSeconds()
{
    return seconds;
}

double DataEntryEventData::getStorageStartSeconds()
{
    return startSeconds;
}

double DataEntryEventData::getStorageEndSeconds()
{
    return endSeconds;
}

double DataEntryEventData::getMarkerStartSeconds()
{
    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
       return qobject_cast<QCPItemRect*>(marker)->topLeft->coords().x();
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
       return qobject_cast<QCPItemLine*>(marker)->start->coords().x();
    else
        return 0;
}

double DataEntryEventData::getMarkerEndSeconds()
{
    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
       return qobject_cast<QCPItemRect*>(marker)->bottomRight->coords().x();
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
       return qobject_cast<QCPItemLine*>(marker)->start->coords().x();
    else
        return 0;
}


double DataEntryEventData::getStartLat()
{
    QMap<double, QGeoCoordinate*>::const_iterator start = getCoordinateIter(getMarkerStartSeconds());
    return start.value()->latitude();
}

double DataEntryEventData::getStartLon()
{
    QMap<double, QGeoCoordinate*>::const_iterator start = getCoordinateIter(getMarkerStartSeconds());
    return start.value()->longitude();
}

double DataEntryEventData::getEndLat()
{
    QMap<double, QGeoCoordinate*>::const_iterator start = getCoordinateIter(getMarkerEndSeconds());
    return start.value()->latitude();
}

double DataEntryEventData::getEndLon()
{
    QMap<double, QGeoCoordinate*>::const_iterator start = getCoordinateIter(getMarkerEndSeconds());
    return start.value()->longitude();
}

QString DataEntryEventData::getStartLatDdm()
{
    return FormatUtility::getDegreeDecimalMinutes(getStartLat(),2);
}

QString DataEntryEventData::getStartLonDdm()
{
   return FormatUtility::getDegreeDecimalMinutes(getStartLon(),2);
}

QString DataEntryEventData::getEndLatDdm()
{
    return FormatUtility::getDegreeDecimalMinutes(getEndLat(),2);
}

QString DataEntryEventData::getEndLonDdm()
{
    return FormatUtility::getDegreeDecimalMinutes(getEndLon(),2);
}


QString DataEntryEventData::getStartDateTimeFormatted()
{
    QString frmt(Constants::DATE_TIME_FORMAT);

    if(!rApp->exportDateTimeFormat.isEmpty())
        frmt = rApp->exportDateTimeFormat;

    return QDateTime::fromMSecsSinceEpoch(getMarkerStartSeconds()*1000).toUTC().toString(frmt);
}

QString DataEntryEventData::getEndDateTimeFormatted()
{
    QString frmt(Constants::DATE_TIME_FORMAT);

    if(!rApp->exportDateTimeFormat.isEmpty())
        frmt = rApp->exportDateTimeFormat;

    return QDateTime::fromMSecsSinceEpoch(getMarkerEndSeconds()*1000).toUTC().toString(frmt);
}

qint64 DataEntryEventData::getDurationSeconds()
{
    double seconds = getMarkerEndSeconds() - getMarkerStartSeconds();
    return round(seconds);
}


QString DataEntryEventData::getDuration8601()
{


        //calculate the duration
        qint64 x = getDurationSeconds();
        int seconds = x % 60;
        x /= 60;
        int minutes = x % 60;
        x /= 60;
        int hours = x % 24;
        x /= 24;
        int days = x;

        //set to duration with padded fields
        return QString("P%1DT%2H%3M%4S").arg(days).arg(hours).arg(minutes).arg(seconds);


}


QString DataEntryEventData::getTimeDescription()
{

    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {
        //calculate the duration
        int x = (qobject_cast<QCPItemRect*>(marker)->bottomRight->coords().x() - qobject_cast<QCPItemRect*>(marker)->topLeft->coords().x());
        int seconds = x % 60;
        x /= 60;
        int minutes = x % 60;
        x /= 60;
        int hours = x % 24;
        x /= 24;
        int days = x;

        //set to duration with padded fields
        return QString("%1:%2:%3:%4").arg(days).arg(hours,2,10,QChar('0')).arg(minutes,2,10,QChar('0')).arg(seconds,2,10,QChar('0'));
    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
        return QDateTime::fromMSecsSinceEpoch(getMarkerStartSeconds()).toUTC().toString(Constants::DATE_TIME_FORMAT);

    return "";

}



QString DataEntryEventData::getSummary()
{

    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {

        return QString("%1 ... Duration: %2 ... Traveled Distance: %3 km ... Straight Line Distance: %4 km")
            .arg(eventType->name)
            .arg(getTimeDescription())
            .arg(QString::number(getTraveledDistanceKm(), 'f', 2))
            .arg(QString::number(getStraightLineDistanceKm(), 'f', 2));
        //could add json display
        //.arg(QString::fromUtf8(toJson(false).toJson()));

    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
    {


        QMap<double, QGeoCoordinate*>::const_iterator point = rApp->emData->mapSegmentsByStart.lowerBound(qobject_cast<QCPItemLine*>(marker)->start->coords().x());

        return QString("%1 ... Time: %2 ... Position: %3")
                .arg(eventType->name)
                .arg(getTimeDescription())
                .arg(point == rApp->emData->mapSegmentsByStart.end() ? "" : FormatUtility::getDegreeDecimalMinutes(point.value()));
    }


    return "";

}




QString DataEntryEventData::getTreeViewSummary()
{

    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {

        QDateTime s = QDateTime::fromMSecsSinceEpoch(qobject_cast<QCPItemRect*>(marker)->topLeft->coords().x()*1000).toUTC();


        QDateTime e = QDateTime::fromMSecsSinceEpoch(qobject_cast<QCPItemRect*>(marker)->bottomRight->coords().x()*1000).toUTC();


        return QString("%1: %2 - %3")
            .arg(eventType->name)
            .arg(s.toString(Constants::DATE_TIME_FORMAT))
            .arg(e.toString(Constants::DATE_TIME_FORMAT));

    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
    {

        QDateTime p = QDateTime::fromMSecsSinceEpoch(qobject_cast<QCPItemLine*>(marker)->start->coords().x()*1000).toUTC();

        return QString("%1: %2")
            .arg(eventType->name)
            .arg(p.toString(Constants::DATE_TIME_FORMAT));

    }


    return "";

}


double DataEntryEventData::getTraveledDistanceKm()
{
    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {

        QMap<double, QGeoCoordinate*>::const_iterator start = getCoordinateIter(getMarkerStartSeconds());
        QMap<double, QGeoCoordinate*>::const_iterator end = getCoordinateIter(getMarkerEndSeconds());

        if(start == rApp->emData->mapSegmentsByStart.end())
            return 0;

        //qDebug() << end.value()->toString();

        double traveledDistanceM = 0;
        while (start != end) {
            QMap<double, QGeoCoordinate*>::const_iterator last = start;
            start++;
            if(!rApp->emData->mapSegmentsByStart.contains(start.key()))
                break;
            double dist = start.value()->distanceTo(QGeoCoordinate(last.value()->latitude(),last.value()->longitude()));
            traveledDistanceM += dist;
            //qDebug() << dist << traveledDistanceM;
        }
        /*
        QGeoCoordinate *last = start.value();
        for (start; start <= end; start++)
        {
            double dist = start.value()->distanceTo(QGeoCoordinate(last->latitude(),last->longitude()));
            traveledDistanceM += dist;
            last = start.value();
            qDebug() << dist << traveledDistanceM;
        }
        */

        return traveledDistanceM/1000;

    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
        return 0;

    return 0;

}

double DataEntryEventData::getStraightLineDistanceKm()
{
    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {
        QMap<double, QGeoCoordinate*>::const_iterator start = getCoordinateIter(getMarkerStartSeconds());
        QMap<double, QGeoCoordinate*>::const_iterator end =  getCoordinateIter(getMarkerEndSeconds());

        if(start == rApp->emData->mapSegmentsByStart.end())
            return 0;

        return start.value()->distanceTo(QGeoCoordinate(end.value()->latitude(),end.value()->longitude()))/1000;

    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
        return 0;


    return 0;

}



QMap<double, QGeoCoordinate*>::const_iterator DataEntryEventData::getCoordinateIter(double seconds)
{
    return rApp->emData->mapSegmentsByStart.lowerBound(getClosestCoordinateReportTimeAtTime(seconds));
}






double DataEntryEventData::getClosestCoordinateReportTimeAtTime(double t)
{
    QMap<double, QGeoCoordinate*>::const_iterator i = rApp->emData->mapSegmentsByStart.lowerBound(t);

    //we've reached the end of the map segements or are still at the beginning
    if(i == rApp->emData->mapSegmentsByStart.constEnd() || i == rApp->emData->mapSegmentsByStart.constBegin())
        return 0;

    double k1 = i.key();
    double k2 = 0;
    if (k1 > t) //get the value before
    {
        i--;
        k2 = i.key();
    }

    double k = std::abs(k1-t) > std::abs(k2-t) ? k2 : k1;

    return k;
}



QString DataEntryEventData::toCsv()
{
    QString stub;
    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {
        double startTime = qobject_cast<QCPItemRect*>(marker)->topLeft->coords().x();
        double endTime = qobject_cast<QCPItemRect*>(marker)->bottomRight->coords().x();

        QString startStr = rApp->exportDateTimeFormat.isEmpty() ? QString::number(startTime,'f',2) :
                                                                  QDateTime::fromMSecsSinceEpoch(startTime*1000).toUTC().toString(rApp->exportDateTimeFormat);

        QString endStr = rApp->exportDateTimeFormat.isEmpty() ? QString::number(endTime,'f',2) :
                                                                  QDateTime::fromMSecsSinceEpoch(endTime*1000).toUTC().toString(rApp->exportDateTimeFormat);

        QMap<double, QGeoCoordinate*>::const_iterator start = rApp->emData->mapSegmentsByStart.lowerBound(startTime);
        QMap<double, QGeoCoordinate*>::const_iterator end = rApp->emData->mapSegmentsByStart.lowerBound(endTime);


        stub.append(QString("\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\",\"%8\"")
                .arg(startStr)
                .arg(endStr)
                .arg(start == rApp->emData->mapSegmentsByStart.end() ? NULL : start.value()->latitude())
                .arg(start == rApp->emData->mapSegmentsByStart.end() ? NULL : start.value()->longitude())
                .arg(end == rApp->emData->mapSegmentsByStart.end() ? NULL : end.value()->latitude())
                .arg(end == rApp->emData->mapSegmentsByStart.end() ? NULL : end.value()->longitude())
                .arg(QString::number(getStraightLineDistanceKm(),'f',3))
                .arg(QString::number(getTraveledDistanceKm(),'f',3)));



    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
    {
        double time = qobject_cast<QCPItemLine*>(marker)->start->coords().x();

        QString timeStr = rApp->exportDateTimeFormat.isEmpty() ? QString::number(time,'f',2) :
                                                                  QDateTime::fromMSecsSinceEpoch(time*1000).toUTC().toString(rApp->exportDateTimeFormat);

        QMap<double, QGeoCoordinate*>::const_iterator start = rApp->emData->mapSegmentsByStart.lowerBound(time);
        //TODO constants
        stub.append(QString("\"%1\",\"%2\",\"%3\"")
                .arg(timeStr)
                .arg(start == rApp->emData->mapSegmentsByStart.end() ? NULL : start.value()->latitude())
                .arg(start == rApp->emData->mapSegmentsByStart.end() ? NULL : start.value()->longitude()));
    }

    foreach(QVariant val, data.values())
    {
        stub.append(",");

        if(val.type() == QVariant::Type::List || val.type() == QVariant::Type::StringList)
        {
            QStringList l = val.toStringList();
            stub.append("[");
            if(l.length() > 0)
                stub.append("'");
            stub.append(l.join("'|'"));
            if(l.length() > 0)
                stub.append("'");
            stub.append("]");
        }
        else if(val.type() == QVariant::Type::String)
        {
           //TODO escape function for all odd chars?
           stub.append("\"");
           stub.append(val.toString().replace("\"","\"\""));
           stub.append("\"");
        }
        else
        {
            stub.append("\"");
            stub.append(val.toString());
            stub.append("\"");
        }
    }

    return stub;
}



QString DataEntryEventData::getHeader()
{
    return getHeaderList().join(",");
}


bool DataEntryEventData::compare(DataEntryEventData *d1, DataEntryEventData *d2)
{   if (d1->eventType->id == d2->eventType->id)
        return d1->getMarkerStartSeconds() < d2->getMarkerStartSeconds();
     else
        return d1->eventType->id < d2->eventType->id;
}




QList<QString> DataEntryEventData::getHeaderList()
{
    QList<QString> stub;
    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
       stub << "Begin Time" << "End Time" << "Begin Latitude" << "Begin Longitude" << "End Latitude" << "End Longitude" << "Straight Line Kilometers" << "Traveled Kilometers";
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
       stub << "Time" << "Latitude" << "Longitude";

    foreach(int key, data.keys())
        stub.append(eventType->fieldNamesById.value(key));

    return stub;
}


QList<QVariant> DataEntryEventData::toVariantList()
{
    QList<QVariant> stub;
    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {
        double startTime = qobject_cast<QCPItemRect*>(marker)->topLeft->coords().x();
        double endTime = qobject_cast<QCPItemRect*>(marker)->bottomRight->coords().x();

        QMap<double, QGeoCoordinate*>::const_iterator start = rApp->emData->mapSegmentsByStart.lowerBound(startTime);
        QMap<double, QGeoCoordinate*>::const_iterator end = rApp->emData->mapSegmentsByStart.lowerBound(endTime);

        stub.append(QDateTime::fromMSecsSinceEpoch(startTime*1000).toUTC());
        stub.append(QDateTime::fromMSecsSinceEpoch(endTime*1000).toUTC());
        stub.append(start == rApp->emData->mapSegmentsByStart.end() ? NULL : FormatUtility::getDegreeDecimalMinutes(start.value()->latitude(),Constants::Coordinate::Latitude,3));
        stub.append(start == rApp->emData->mapSegmentsByStart.end() ? NULL : FormatUtility::getDegreeDecimalMinutes(start.value()->longitude(),Constants::Coordinate::Longitude,3));
        stub.append(end == rApp->emData->mapSegmentsByStart.end() ? NULL : FormatUtility::getDegreeDecimalMinutes(end.value()->latitude(),Constants::Coordinate::Latitude,3));
        stub.append(end == rApp->emData->mapSegmentsByStart.end() ? NULL : FormatUtility::getDegreeDecimalMinutes(end.value()->longitude(),Constants::Coordinate::Longitude,3));

        stub.append(QString::number(getStraightLineDistanceKm(),'f',3));
        stub.append(QString::number(getTraveledDistanceKm(),'f',3));
    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
    {
        double time = qobject_cast<QCPItemLine*>(marker)->start->coords().x();
        QMap<double, QGeoCoordinate*>::const_iterator start = rApp->emData->mapSegmentsByStart.lowerBound(time);

         stub.append(QDateTime::fromMSecsSinceEpoch(time*1000).toUTC());
         stub.append(start == rApp->emData->mapSegmentsByStart.end() ? NULL : FormatUtility::getDegreeDecimalMinutes(start.value()->latitude(),Constants::Coordinate::Latitude,3));
         stub.append(start == rApp->emData->mapSegmentsByStart.end() ? NULL : FormatUtility::getDegreeDecimalMinutes(start.value()->longitude(),Constants::Coordinate::Longitude,3));
    }

    foreach(int key, data.keys())
    {

       if(eventType->multiEnumeratedValueFields.contains(key))
       {
           //convert from ids to values
           QStringList stringVals;
           foreach(QVariant v, data.value(key).toList())
               stringVals.append(eventType->enumeratedValuesById.value(key).value(v.toString()));

           stub.append(stringVals.join(", "));
       }
       else if(eventType->singleEnumeratedValueFields.contains(key))
       {
           //convert from ids to values
           QString s = eventType->enumeratedValuesById.value(key).value(data.value(key).toString());
           stub.append(s);
       }
       else if(eventType->dateTimes.contains(key))
       {
            QDateTime t = QDateTime::fromMSecsSinceEpoch(data.value(key).toULongLong()*1000);
            t.setTimeSpec(Qt::UTC);
            stub.append(t);
       }
       else
           stub.append(data.value(key));

   }

    return stub;
}


QJsonDocument DataEntryEventData::toInternalJson()
{
    return QJsonDocument::fromVariant(toVariantMap(true));
}


QJsonDocument DataEntryEventData::toExportJson()
{
    QVariantMap m = toVariantMap(false);


    //duration only
    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {
        m.insert("startDateTimeFormatted", getStartDateTimeFormatted());
        m.insert("startLatitude", getStartLat());
        m.insert("startLongitude", getStartLon());
        m.insert("startLatitudeDDM", getStartLatDdm());
        m.insert("startLongitudeDDM", getStartLonDdm());
        m.insert("endLatitude", getEndLat());
        m.insert("endLongitude", getEndLon());
        m.insert("endDateTimeFormatted", getEndDateTimeFormatted());
        m.insert("traveledDistanceKm", getTraveledDistanceKm());
        m.insert("straightLineDistanceKm", getStraightLineDistanceKm());
        m.insert("durationSeconds",getDurationSeconds());
        m.insert("duration8601",getDuration8601());
    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
    {
        m.insert("dateTimeFormatted", getStartDateTimeFormatted());
        m.insert("latitude", getStartLat());
        m.insert("longitude", getStartLon());
        m.insert("latitudeDDM", getStartLatDdm());
        m.insert("longitudeDDM", getStartLonDdm());
    }

    return QJsonDocument::fromVariant(m);
}




QVariantMap DataEntryEventData::toVariantMap(bool internal)
{
    QVariantMap map;

    if(internal)
        map.insert("typeId", eventType->id);
    else
        map.insert("typeName", eventType->name);

    //qDebug() << "saving createdByAtuomation" << createdByAutomation;
    map.insert("createdByAutomation", createdByAutomation);

    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {
        //TODO constants
        map.insert("startSeconds", getMarkerStartSeconds());
        map.insert("endSeconds", getMarkerEndSeconds());
    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
        map.insert("seconds", getMarkerStartSeconds());

    QVariantMap eventData;
    foreach(int controlId, data.keys())
    {
       if(internal) //use ids
            eventData.insert(QString::number(controlId), data.value(controlId));
       else //use human readable string values or key value pairs for enumerated types
       {
           QString fieldName = eventType->fieldNamesById.value(controlId);
           QVariant val;
           //TODO replace with better tracking of field types
           if(eventType->multiEnumeratedValueFields.contains(controlId))
           {
               //key value pairs
               QVariantMap kvMap;
               kvMap.insert("keys",QVariant::fromValue(data.value(controlId)));
               QVariantList l;
               foreach(QString k, data.value(controlId).toStringList())
                   l.append(eventType->enumeratedValuesById.value(controlId).value(k));
               kvMap.insert("values",l);
               val = kvMap;
           }
           else if(eventType->singleEnumeratedValueFields.contains(controlId))
           {
               //key value pairs
               QVariantMap kvMap;
               kvMap.insert("key",QVariant::fromValue(data.value(controlId)));
               kvMap.insert("value",QVariant::fromValue(
                                eventType->enumeratedValuesById.value(controlId).value(data.value(controlId).toString())));
               val = kvMap;
           }
           else
               val = data.value(controlId);

           if(val.userType() == QMetaType::QString)
               val = val.toString().trimmed();

           QString sanitizedFieldName = fieldName;
           //TODO move to util class
           sanitizedFieldName.remove(QRegularExpression("[^a-zA-Z\\d\\s]"));
           sanitizedFieldName.replace(QRegularExpression("[\\s]"),"_");
           qDebug() << fieldName << sanitizedFieldName;

           eventData.insert(sanitizedFieldName, val);
       }
    }

    map.insert("eventData", eventData);

    return map;
}


//reconstitute from JSON, return false on failure
bool DataEntryEventData::fromJson(QJsonDocument json, int id)
{
    storageId = id;
    //qDebug() << json.toJson();

    QVariantMap obj = json.toVariant().toMap();
    int typeId = obj.value("typeId").toInt();

    //don't load event types we no longer have
    if(!rApp->eventTypes.keys().contains(typeId))
        return false;

    eventType = rApp->eventTypes.value(typeId);
    createdByAutomation = rApp->eventTypes.value(obj.value("createdByAutomation").toBool());

    if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {
  startSeconds = obj.value("startSeconds").toDouble();
        endSeconds = obj.value("endSeconds").toDouble();
    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Point)
        seconds = obj.value("seconds").toDouble();

    QVariantMap eventData = obj.value("eventData").toMap();
    //TODO move template stucture out of dialog
    foreach(DataEntryWidgetWrapper *wrapper, eventType->dialog->controls)
        if(eventData.contains(QString::number(wrapper->id)))
            data.insert(wrapper->id, eventData.value(QString::number(wrapper->id)));
        else
            data.insert(wrapper->id, QVariant());

    return true;

}

//TODO refactor
DataEntryEventData* DataEntryEventData::getParent()
{

    QList<DataEntryEventData *> possibles;

    //if this event specifies a parent type, filter the list to those types
    foreach(DataEntryEventData *possible, rApp->eventData)
        if(eventType->parentEventId == -1 || possible->eventType->id == eventType->parentEventId)
            possibles.append(possible);

    DataEntryEventData *parent = NULL;
    double duration = -1;
    foreach(DataEntryEventData *possible, possibles)
        if(possible->eventType->markerType == DataEntryEventType::MarkerType::Duration) //must be a duration
            if(possible->getMarkerStartSeconds() <= this->getMarkerStartSeconds() && possible->getMarkerEndSeconds() >= this->getMarkerEndSeconds() &&
                    possible->storageId != this->storageId) //must completely contain and not match itself
                if(duration == -1 || possible->getMarkerEndSeconds() - possible->getMarkerStartSeconds() < duration) //find the shortest duration
                {
                    duration = possible->getMarkerEndSeconds() - possible->getMarkerStartSeconds();
                    parent = possible;
                }

    return parent;
}



void DataEntryEventData::setMarkerStyle()
{
    //double top = rApp->emData->maxValue;
    //double bot = rApp->emData->minValue;

    if(eventType->markerType == DataEntryEventType::MarkerType::Point)
    {

       QCPItemLine *line = qobject_cast<QCPItemLine *>(marker);
       //lines always start at the bottom and are created at full height (so we want end coords for both)
       line->start->setCoords(line->start->coords().x(), eventType->markerBottom < 0 ? abs(rApp->emData->minValue) * eventType->markerBottom : abs(rApp->emData->maxValue) * eventType->markerBottom);
       line->end->setCoords(line->end->coords().x(),  eventType->markerTop < 0 ? abs(rApp->emData->minValue) * eventType->markerTop : abs(rApp->emData->maxValue) * eventType->markerTop);
       //qDebug() << "set line size " << line->start->coords().y() << line->end->coords().y();
       line->setPen(QPen(eventType->color, eventType->width, eventType->LineStyle));
       //TODO global config for selected color
       line->setSelectedPen(QPen(Qt::blue, eventType->width + 1, Qt::SolidLine));
       line->setSelectable(true);
    }
    else if(eventType->markerType == DataEntryEventType::MarkerType::Duration)
    {
       QCPItemRect *duration = qobject_cast<QCPItemRect *>(marker);

       //same as lines, use top left and multiply
       //qDebug() << "duration size before styling " << duration->topLeft->coords() << duration->bottomRight->coords();
       duration->bottomRight->setCoords( duration->bottomRight->coords().x(), eventType->markerBottom < 0 ? abs(rApp->emData->minValue) * eventType->markerBottom : abs(rApp->emData->maxValue) * eventType->markerBottom);
       duration->topLeft->setCoords(duration->topLeft->coords().x(), eventType->markerTop < 0 ? abs(rApp->emData->minValue) * eventType->markerTop : abs(rApp->emData->maxValue) * eventType->markerTop);
       //qDebug() << "duration size after styling " << duration->topLeft->coords() << duration->bottomRight->coords();

       if(eventType->durationMarkerStyle == DataEntryEventType::DurationMarkerStyle::Shade)
       {
           duration->setPen(Qt::NoPen);
           duration->setBrush(QBrush(eventType->color));
           duration->setSelectedBrush(QBrush(eventType->color));
       }
       else if(eventType->durationMarkerStyle == DataEntryEventType::DurationMarkerStyle::Line)
       {
           duration->setPen(QPen(eventType->color, eventType->width, eventType->LineStyle));
           duration->setBrush(Qt::NoBrush);
           duration->setSelectedBrush(Qt::NoBrush);
       }

       //TODO global config
       duration->setSelectedPen(QPen(Qt::blue, eventType->width + 1, Qt::SolidLine));
       duration->setSelectable(true);
    }

}

