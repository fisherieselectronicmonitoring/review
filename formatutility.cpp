#include "formatutility.h"

#include <QtMath>
#include <QString>

FormatUtility::FormatUtility(QObject *parent) : QObject(parent)
{

}


QString FormatUtility::getDegreeDecimalMinutes(double decimalDeg, Constants::Coordinate coordType, int decimals)
{
    return QString("%1 %2 %3")
               .arg(qFloor(qAbs(decimalDeg)))
               .arg(QString::number(fmod(qAbs(decimalDeg), 1) * 60, Constants::FLOATING_POINT_FORMAT, decimals))
               .arg(coordType == Constants::Coordinate::Latitude ? (decimalDeg > 0 ? "N" : "S") : (decimalDeg > 0 ? "E" : "W"));

}

QString FormatUtility::getDegreeDecimalMinutes(double decimalDeg, int decimals)
{
    return QString("%1%2%3")
               .arg(decimalDeg > 0 ? "" : "-")
               .arg(qFloor(qAbs(decimalDeg)))
               .arg(QString::number(fmod(qAbs(decimalDeg), 1) * 60, Constants::FLOATING_POINT_FORMAT, decimals)
                    .rightJustified(2, '0'));


}

QString FormatUtility::getDegreeDecimalMinutes(QGeoCoordinate *coord, int decimals)
{
    return QString("%1 %2")
               .arg(getDegreeDecimalMinutes(coord->latitude(), Constants::Coordinate::Latitude, decimals))
               .arg(getDegreeDecimalMinutes(coord->longitude(), Constants::Coordinate::Longitude, decimals));

}
