#ifndef MEASUREMENTWINDOW_H
#define MEASUREMENTWINDOW_H

#include <QMainWindow>

#include "measurementscene.h"
#include "measurementview.h"

namespace Ui {
class MeasurementWindow;
}

class MeasurementWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MeasurementWindow(QWidget *parent = 0);
    ~MeasurementWindow();
    void rectify();

public slots:
      void initialize(QString);
      void initializeCameraCalibration(QString);
      void initialize(QString, double);
      void calibrateCamera();


private:
    Ui::MeasurementWindow *ui;
    MeasurementScene *scene;
    MeasurementView* view;
    double eventTime;



private slots:
   void update();
   void addPointMarker();
   void applyCameraCalibration();

};

#endif // MEASUREMENTWINDOW_H
