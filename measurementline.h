#ifndef MEASUREMENTLINE_H
#define MEASUREMENTLINE_H

#include <QObject>
#include <QGraphicsEllipseItem>


class MeasurementLine : public QObject
{
    Q_OBJECT
public:
    explicit MeasurementLine(QObject *parent = 0);
    QList<QGraphicsEllipseItem*> points;
    QList<QGraphicsLineItem*> lines;
    QGraphicsTextItem* label;

    void update();
    double lineLengthPx();
    double lineLengthCm;
    void removeFromScene(QGraphicsScene*);
    void setVisibility(bool);




};

#endif // MEASUREMENTLINE_H
