#ifndef EMDATA_H
#define EMDATA_H

#include "emdataseries.h"
#include "emdatasensorevent.h"
#include "emdataenumevent.h"
#include "emdatavideoduration.h"
#include "emdatastillimageduration.h"
#include "constants.h"

#ifndef Q_MOC_RUN  // See: https://bugreports.qt-project.org/browse/QTBUG-22829
    #include <boost/geometry/index/rtree.hpp>
#endif

#include <QObject>
#include <QString>
#include <QGeoCoordinate>
#include <QMap>
#include <QMultiMap>
#include <QHash>
#include <QVariant>
#include <VLCQtCore/Media.h>



class EmData : public QObject
{

    Q_OBJECT
public:
    explicit EmData(QObject *parent = 0);

    //data series for plotting
    QList<EmDataSeries*> series;

    //sensor events
    QList<EmDataSensorEvent *> sensorEvents;

    //events with enumerated types
    QList<EmDataEnumEvent *> enumEvents;

    //index the video by name and by start time
    QHash<QString, EmDataVideoDuration *> videosByName;
    QMultiMap<int, EmDataVideoDuration *> videosByChannel;
    QMultiMap<double, EmDataVideoDuration *> videosByStart;


    //index the still images by name and by start time
    QHash<QString, EmDataStillImageDuration *> stillImagesByName;
    QMultiMap<double, EmDataStillImageDuration *> stillImagesByStart;

    //map coordinates by time
    QMap<double, QGeoCoordinate *> mapSegmentsByStart;
    //map colors by time
    QMap<double, QString> mapSegmentColorByStart;

    //qvariant list of qvariantmaps with coordinate lat/lons and segment colors
    QVariantList mapSegmentVariants;

    //boost spatial index of coordinates
    boost::geometry::index::rtree<std::pair<Constants::Coordinates, long>, boost::geometry::index::quadratic<16>>  spatialIndex;

    //minimum and maximum y values
    double minValue;
    double maxValue;

    int videoChannels;
    QMap<int, QString> videoChannelNames;
    int stillImageChannels;







signals:

public slots:
};

#endif // EMDATA_H
