#ifndef MEASUREMENTSCENE_H
#define MEASUREMENTSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLineItem>
#include <QAction>
#include <QGraphicsView>
#include <QKeyEvent>
#include <QTimer>


#include <QDebug>
#include <QTimer>
#include <QInputDialog>
#include <QVector3D>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>

#include "measurementline.h"

class MeasurementScene : public QGraphicsScene
{
public:

    MeasurementScene(QObject* parent = 0);
    void setupForRectification(QString);
    void setRectificationParameters(double, double, int);
    bool rectify();
    bool calibrateCamera();
    void loadCameraCalibration();
    void applyCameraCalibration();
    void setupForMeasurement();
    void autoCalibrate();
    QRectF baseImageRect();
    int getCalibrationCount();

    double getLastMeasurementCm();

    //TODO delete later
    int count = 0;

protected:
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*);
    void keyPressEvent(QKeyEvent *event);

private:
    //holds the image we are working on
    QString currentImagePath;

    enum DrawingMode {NoMode, DrawingCalibrationLine, DrawingMeasurementLine, AddingRectificationPoints};
    enum MeasuringMode {Rectification, Measurement};
    DrawingMode currentDrawingMode;
    MeasuringMode currentMeasuringMode;


    //measurement area sizes, must be set prior to calling rectify()
    double heightCm;
    double widthCm;
    double borderFactor;


    QGraphicsPixmapItem* baseImage;
    QList<MeasurementLine*> calibrationLines;
    QList<MeasurementLine*> measurementLines;
    QList<QGraphicsEllipseItem*> rectificationPoints;


    void addCalibrationLine(QPointF, QPointF, double);
    void addCalibrationPoint(QPointF);
    void addMeasurementPoint(QPointF);
    void addRectificationPoint(QPointF);
    QGraphicsEllipseItem* createPoint(QPointF, QColor);
    QGraphicsLineItem* createLine(QColor);
    QGraphicsTextItem* createLabel(QColor);
    QTimer* measurementLineDrawingModeTimer;

    void deleteMeasurementLines();
    void deleteCalibrationLines();

    void setRectificationPointVisiblity(bool);
    void setCalibrationLineVisiblity(bool);

    void setImage();

    double computeReprojectionErrors(const std::vector< std::vector< cv::Point3f > >& objectPoints,
    const std::vector< std::vector< cv::Point2f > >& imagePoints,
    const std::vector< cv::Mat >& rvecs, const std::vector< cv::Mat >& tvecs,
    const cv::Mat& cameraMatrix , const cv::Mat& distCoeffs);

    void writeVectorOfVector(cv::FileStorage &fs, std::string name, std::vector<std::vector<cv::Point3f>> &data);
    void readVectorOfVector(cv::FileStorage &fns, std::string name, std::vector<std::vector<cv::Point3f>> &data);
    void writeVectorOfVector(cv::FileStorage &fs, std::string name, std::vector<std::vector<cv::Point2f>> &data);
    void readVectorOfVector(cv::FileStorage &fns, std::string name, std::vector<std::vector<cv::Point2f>> &data);

    template <typename T> cv::Mat_<T> vec2cvMat_2D(std::vector< std::vector<T> > &inVec);
    template <typename T> std::vector< std::vector<T> > cvMat2vec_2D(cv::Mat_<T> &inMat);

    //these two get serialzied and are read on start up
    std::vector< std::vector< cv::Point3f > > object_points;
    std::vector< std::vector< cv::Point2f > > image_points;
    std::vector< cv::Point2f > corners;
    std::vector< cv::Point2f > imagePoints2;
    std::vector< float > perViewErrors;
    cv::Mat cameraMatrix;
    cv::Mat distCoeffs;
    bool cameraCalibrationNeeded;

private slots:
    void sceneChange();
    void unsetMeasurementLineMode();

};

#endif // MEASUREMENTSCENE_H
