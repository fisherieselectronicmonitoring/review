#include "emdatasensorevent.h"

EmDataSensorEvent::EmDataSensorEvent(QObject *parent) : QObject(parent)
{

}


void EmDataSensorEvent::setMarkerStyle()
{

}


QString EmDataSensorEvent::getSummary()
{
    QString summary = QString("%1 (").arg(name);
    foreach(QString s, displayData.keys())
    {
        summary.append(QString("%1: %2 ").arg(s).arg(displayData.value(s).toString()));
    }
    summary.truncate(summary.length()-1);
    summary.append(")");

    return summary;
}
