#include "measurementscene.h"
#include "measurementutility.h"
#include "reviewapplication.h"

// include input and output archivers
//#include <boost/archive/text_oarchive.hpp>
//#include <boost/archive/text_iarchive.hpp>

// include this header to serialize vectors
//#include <boost/serialization/vector.hpp>

#include <iostream>
#include <fstream>



MeasurementScene::MeasurementScene(QObject* parent): QGraphicsScene(parent)
{
    //TODO replace with buttons?
    currentDrawingMode = NoMode;

    //this will draw lines, labels and measure distances
    connect(this, &MeasurementScene::changed, this, &MeasurementScene::sceneChange);

    measurementLineDrawingModeTimer = new QTimer();
    //TODO constants
    measurementLineDrawingModeTimer->setInterval(2500);
    measurementLineDrawingModeTimer->setSingleShot(true);

    setBackgroundBrush(QBrush(Qt::black, Qt::SolidPattern));

    //this unsets the measurement line drawing mode after a timeout period
    connect(measurementLineDrawingModeTimer, &QTimer::timeout, this, &MeasurementScene::unsetMeasurementLineMode);

    //default pixmap to nothing
    baseImage = 0;

    cameraCalibrationNeeded = true;
}

void MeasurementScene::loadCameraCalibration()
{
    //reload saved calibration
    cv::FileStorage fsr;
    fsr.open(rApp->currentEmDataDirectory.toStdString() + "/calibration.xml", cv::FileStorage::READ);
    qDebug() << "node name" << QString::fromStdString(fsr.getFirstTopLevelNode().name());
    cv::Mat_<cv::Point3f> o;
    fsr["object_points"] >> o;
    qDebug() << "op" << o.size;
    object_points = cvMat2vec_2D(o);
    cv::Mat_<cv::Point2f> i;
    fsr["image_points"] >> i;
    qDebug() << "ip" << i.size;
    image_points = cvMat2vec_2D(i);
    fsr.release();


}

void MeasurementScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    //TODO replace with button controled modes
    if(event->button() == Qt::MouseButton::LeftButton)
        addRectificationPoint(event->scenePos());
    else if(event->button() == Qt::MouseButton::MiddleButton)
        addCalibrationPoint(event->scenePos());
    else if(event->button() == Qt::MouseButton::RightButton)
        addMeasurementPoint(event->scenePos());

}


void MeasurementScene::addCalibrationPoint(QPointF p)
{
    //new calibration line
    if(currentMeasuringMode != Measurement)
        return;

    if(currentDrawingMode == DrawingMode::DrawingMeasurementLine || currentDrawingMode == DrawingMode::AddingRectificationPoints)
        return;

    else if(currentDrawingMode == DrawingMode::NoMode)
    {
        currentDrawingMode = DrawingMode::DrawingCalibrationLine;
        MeasurementLine* cLine = new MeasurementLine();
        cLine->points.append(createPoint(p,Qt::red));
        cLine->label = createLabel(Qt::white);
        calibrationLines.append(cLine);
    }
    else if(currentDrawingMode == DrawingMode::DrawingCalibrationLine)
    {
        //TODO replace with line edit and validator
        double cm = QInputDialog::getDouble(rApp->measurement, "Calibration",
                                               "Enter Length in cm:",0,0.00001,10000,rApp->measurementDecimals);
        if (cm > 0)
            calibrationLines.last()->lineLengthCm = cm;

        currentDrawingMode = DrawingMode::NoMode;
        calibrationLines.last()->lines.append(createLine(Qt::white));
        calibrationLines.last()->points.append(createPoint(p,Qt::blue));

    }
}


void MeasurementScene::addRectificationPoint(QPointF p)
{
    //new rectification point

    //new calibration line
    if(currentMeasuringMode != Rectification)
        return;

    if(currentDrawingMode == DrawingMode::DrawingMeasurementLine || currentDrawingMode == DrawingMode::DrawingCalibrationLine)
        return;
    else if(currentDrawingMode == DrawingMode::NoMode)
    {
        foreach(QGraphicsEllipseItem* i, rectificationPoints)
            removeItem(i);
        rectificationPoints.clear();
        currentDrawingMode = DrawingMode::AddingRectificationPoints;
        rectificationPoints.append(createPoint(p,Qt::white));

    }
    else if(currentDrawingMode == DrawingMode::AddingRectificationPoints)
    {
        rectificationPoints.append(createPoint(p,Qt::white));
        if(rectificationPoints.length() > 3)
            currentDrawingMode = DrawingMode::NoMode;
    }
}


void MeasurementScene::addMeasurementPoint(QPointF p)
{
    //new measurement line

    //new calibration line
    if(currentMeasuringMode != Measurement)
        return;

    if(currentDrawingMode == DrawingMode::DrawingCalibrationLine || currentDrawingMode== DrawingMode::AddingRectificationPoints)
        return;
    else if(currentDrawingMode == DrawingMode::NoMode)
    {
        currentDrawingMode = DrawingMode::DrawingMeasurementLine;
        MeasurementLine* line = new MeasurementLine();
        line->points.append(createPoint(p,Qt::green));
        line->label = createLabel(Qt::white);
        measurementLines.append(line);

        measurementLineDrawingModeTimer->stop();

    }
    else if(currentDrawingMode == DrawingMode::DrawingMeasurementLine)
    {
        //set color of points
        measurementLines.last()->lines.append(createLine(Qt::white));
        measurementLines.last()->points.append(createPoint(p,Qt::yellow));

        //start the timer that will terminate measurement line mode
        measurementLineDrawingModeTimer->start();
        update();


    }
}


void MeasurementScene::unsetMeasurementLineMode()
{
    currentDrawingMode = NoMode;
}

void MeasurementScene::setupForRectification(QString path)
{
    currentMeasuringMode = Rectification;

    deleteMeasurementLines();
    deleteCalibrationLines();
    //setCalibrationLineVisiblity(false);
    setRectificationPointVisiblity(true);


    //setup the image
    currentImagePath = path;
    setImage();
}

void MeasurementScene::setupForMeasurement()
{
    currentMeasuringMode = Measurement;

    autoCalibrate();

    setCalibrationLineVisiblity(true);
    setRectificationPointVisiblity(false);
    setImage();
}

void MeasurementScene::autoCalibrate()
{
    deleteCalibrationLines();

    addCalibrationLine(QPointF(baseImage->boundingRect().topLeft().x() + borderFactor * baseImage->boundingRect().width(),
                               baseImage->boundingRect().topLeft().y() + borderFactor * baseImage->boundingRect().height()),
                       QPointF(baseImage->boundingRect().topRight().x() + borderFactor * baseImage->boundingRect().width(),
                                baseImage->boundingRect().topRight().y() + borderFactor * baseImage->boundingRect().height()),
                       widthCm);

}

void MeasurementScene::addCalibrationLine(QPointF p1, QPointF p2, double cm)
{
    currentDrawingMode = DrawingMode::DrawingCalibrationLine;
    MeasurementLine* cLine = new MeasurementLine();
    cLine->points.append(createPoint(p1,Qt::red));
    cLine->label = createLabel(Qt::white);
    calibrationLines.append(cLine);
    calibrationLines.last()->lineLengthCm = cm;
    calibrationLines.last()->lines.append(createLine(Qt::white));
    calibrationLines.last()->points.append(createPoint(p2,Qt::blue));
    currentDrawingMode = DrawingMode::NoMode;
}

void MeasurementScene::setImage()
{
    //qDebug() << "in setImage()";
    //qDebug() << "pixmap " << baseImage;
    if(baseImage){
        //qDebug() << "removing pixmap " << baseImage;
        removeItem(baseImage);
    }

    baseImage = addPixmap(QPixmap(currentImagePath));
     //qDebug() << "added pixmap " << baseImage;
    //send to back
    baseImage->setZValue(-1);

   setSceneRect(baseImage->boundingRect());
}


QRectF MeasurementScene::baseImageRect()
{
    return baseImage->boundingRect();
}


QGraphicsEllipseItem* MeasurementScene::createPoint(QPointF p, QColor c)
{
    //TODO constants
    QGraphicsEllipseItem* i = new QGraphicsEllipseItem(-8,-8,16,16);
    i->setPos(p);
    i->setPen(QPen(c,2,Qt::SolidLine));
    i->setFlag(QGraphicsItem::ItemIsSelectable,true);
    i->setFlag(QGraphicsItem::ItemIsMovable,true);
    i->setFlag(QGraphicsItem::ItemIgnoresTransformations,true);
    addItem(i);
    return i;
}



QGraphicsLineItem* MeasurementScene::createLine(QColor c)
{
    QGraphicsLineItem* l = new QGraphicsLineItem();
    l->setPen(QPen(c,1,Qt::SolidLine));
    //l->setFlag(QGraphicsItem::ItemIgnoresTransformations,true);
    addItem(l);
    return l;
}

QGraphicsTextItem* MeasurementScene::createLabel(QColor c)
{
    QGraphicsTextItem* t = new QGraphicsTextItem();
    t->setDefaultTextColor(c);
    //TODO better fontsize calc
    t->setFont(QFont("Helvetica",16));
    t->setFlag(QGraphicsItem::ItemIgnoresTransformations,true);
    addItem(t);
    return t;
}


//event handler each time a point is added or moves
void MeasurementScene::sceneChange()
{
    //qDebug() << "measurementScene::sceneChange: " << count++;


    QList<QVector3D> samples;
    foreach(MeasurementLine* cLine, calibrationLines)
    {
        //TODO get viewport scale
        cLine->update();

        //if there is no length in CM available, skip
        if(cLine->lineLengthCm == 0)
            continue;

        foreach(QGraphicsLineItem* line, cLine->lines)
        {

            //get midpoint for each calibration line
            QLineF l = line->line();
            float x = (l.p1().x() + l.p2().x())/2;
            float y = (l.p1().y() + l.p2().y())/2;

            //get pixel to cm correspondence
            float z = l.length()/cLine->lineLengthCm;

            samples.append(QVector3D(x,y,z));
        }



    }

    foreach(MeasurementLine* mLine, measurementLines)
    {
        //TODO get viewport scale
        mLine->update();

        //if we have no samples
        if(samples.size() < 1)
            continue;

        double cm = 0;
        foreach(QGraphicsLineItem* line, mLine->lines)
        {
            //get midpoint for each segment
            QLineF l = line->line();
            double x = (l.p1().x() + l.p2().x())/2;
            double y = (l.p1().y() + l.p2().y())/2;

            //use the midpoint of the line to determine the resolution in this area of the image
            QPointF midpoint(x,y);
            double resolution = MeasurementUtility::interpolate(samples, midpoint);

            //get length for each segment
            cm += l.length()/resolution;
            qDebug() << "cm " <<cm << "res " << resolution;
        }

        mLine->lineLengthCm = cm;


    }


}

double MeasurementScene::getLastMeasurementCm()
{
    if(measurementLines.size() > 0)
        return measurementLines.last()->lineLengthCm;
    else
        return 0;
}

void MeasurementScene::setRectificationParameters(double width, double height, int border)
{
    widthCm = width;
    heightCm = height;
    //turn slider value into a percentage
    borderFactor = border/100.0;
}


bool MeasurementScene::calibrateCamera()
{
    using namespace std; {
        using namespace cv; {

            //get from config
            int board_width = rApp->measurementChessboardHorizontalCornerCount;
            int board_height = rApp->measurementChessboardVerticalCornerCount;
            float square_size = rApp->measurementChessboardSquareSizeCm;
            Size board_size = Size(board_width, board_height);
            Mat img = MeasurementUtility::qPixmapToCvMat(baseImage->pixmap());

            Mat gray;
            cv::cvtColor(img, gray, CV_BGR2GRAY);
            Mat result;
            cv::cvtColor(gray, result, CV_GRAY2BGR);

            bool found = false;
            found = cv::findChessboardCorners(img, board_size, corners,
                                              CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
            if (found)
            {
                cornerSubPix(gray, corners, cv::Size(5, 5), cv::Size(-1, -1),
                             TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
                drawChessboardCorners(result, board_size, corners, found);

                vector< Point3f > obj;
                for (int i = 0; i < board_height; i++)
                    for (int j = 0; j < board_width; j++)
                        obj.push_back(Point3f((float)j * square_size, (float)i * square_size, 0));

                qDebug() << "found corners!";
                image_points.push_back(corners);
                object_points.push_back(obj);


                //serialize vectors
                FileStorage fs(rApp->currentEmDataDirectory.toStdString() + "/calibration.xml", FileStorage::WRITE);

                cv::write(fs,"image_points",vec2cvMat_2D(image_points));
                cv::write(fs,"object_points",vec2cvMat_2D(object_points));
                //writeVectorOfVector(fs,"image_points",image_points);
                //writeVectorOfVector(fs,"object_points",object_points);
                fs.release();

                cameraCalibrationNeeded = true;

                baseImage->setPixmap(MeasurementUtility::cvMatToQPixmap(result));
                applyCameraCalibration();


            }
            else
                qDebug() << "no corners found";


            return found;
        }
    }
}



template <typename T> cv::Mat_<T> MeasurementScene::vec2cvMat_2D(std::vector< std::vector<T> > &inVec){
    int rows = static_cast<int>(inVec.size());
    int cols = static_cast<int>(inVec[0].size());

    cv::Mat_<T> resmat(rows, cols);
    for (int i = 0; i < rows; i++)
        resmat.row(i) = cv::Mat(inVec[i]).t();
    return resmat;
}

template <typename T> std::vector< std::vector<T> > MeasurementScene::cvMat2vec_2D(cv::Mat_<T> &inMat){
    std::vector< std::vector<T> > vec(inMat.rows);
    for (int i = 0; i < inMat.rows; i++)
        for (int h = 0; h < inMat.cols; h++)
            vec[i].push_back(inMat.at<T>(i,h));

    return vec;
}



// save on file
void MeasurementScene::writeVectorOfVector(cv::FileStorage &fs, std::string name, std::vector<std::vector<cv::Point2f>> &data)
{
    fs << name;
    fs << "{";
    for (int i = 0; i < data.size(); i++)
    {

        fs << name + "_" + std::to_string(i);
        std::vector<cv::Point2f> tmp = data[i];
        fs << tmp;
    }
    fs << "}";
}


int MeasurementScene::getCalibrationCount()
{
    return object_points.size();
}

/*
// read from file
void MeasurementScene::readVectorOfVector(cv::FileStorage &fns, std::string name, std::vector<std::vector<cv::Point2f>> &data)
{
    data.clear();
    cv::FileNode fn = fns[name];
    if (fn.empty()){
        return;
    }

    cv::FileNodeIterator current = fn.begin(), it_end = fn.end();
    for (; current != it_end; ++current)
    {
        std::vector<cv::Point2f> tmp;
        cv::FileNode item = *current;
        item >> tmp;
        data.push_back(tmp);
    }
}


// save on file
void MeasurementScene::writeVectorOfVector(cv::FileStorage &fs, std::string name, std::vector<std::vector<cv::Point3f>> &data)
{
    fs << name;
    fs << "{";
    for (int i = 0; i < data.size(); i++)
    {
        fs << name + "_" + std::to_string(i);
        std::vector<cv::Point3f> tmp = data[i];
        fs << tmp;
    }
    fs << "}";
}

// read from file
void MeasurementScene::readVectorOfVector(cv::FileStorage &fns, std::string name, std::vector<std::vector<cv::Point3f>> &data)
{
    data.clear();
    cv::FileNode fn = fns[name];
    if (fn.empty()){
        return;
    }

    cv::FileNodeIterator current = fn.begin(), it_end = fn.end();
    for (; current != it_end; ++current)
    {
        std::vector<cv::Point3f> tmp;
        cv::FileNode item = *current;
        item >> tmp;
        data.push_back(tmp);
    }
}

*/


void MeasurementScene::applyCameraCalibration()
{
    using namespace std; {
        using namespace cv; {

            Mat img = MeasurementUtility::qPixmapToCvMat(baseImage->pixmap());
            Mat tmp;
            Mat color;
            cv::cvtColor(img, tmp, CV_BGR2HLS);
            cv::cvtColor(tmp, color, CV_HLS2BGR);

            if(cameraCalibrationNeeded)
            {
                  cameraMatrix = Mat();
                  distCoeffs = Mat();
                  vector< Mat > rvecs, tvecs;
                  int flag = 0;
                  flag |= CV_CALIB_FIX_K4;
                  flag |= CV_CALIB_FIX_K5;

                  cv::calibrateCamera(object_points, image_points, color.size(), cameraMatrix, distCoeffs, rvecs, tvecs, flag);
                  cameraCalibrationNeeded = false;
                  qDebug() << "repojection error:" << computeReprojectionErrors(object_points, image_points, rvecs, tvecs, cameraMatrix, distCoeffs) << endl;
            }
            Mat undistorted;
            qDebug() << "calling undistort";
            cv::undistort(color, undistorted, cameraMatrix, distCoeffs);
            qDebug() << "undistort complete";
            baseImage->setPixmap(MeasurementUtility::cvMatToQPixmap(undistorted));


        }
    }
}


double MeasurementScene::computeReprojectionErrors(const std::vector< std::vector< cv::Point3f > >& objectPoints,
                                                           const std::vector< std::vector< cv::Point2f > >& imagePoints,
                                                           const std::vector< cv::Mat >& rvecs, const std::vector< cv::Mat >& tvecs,
                                                           const cv::Mat& cameraMatrix , const cv::Mat& distCoeffs) {
    using namespace std; {
        using namespace cv; {
              imagePoints2.clear();
              int i, totalPoints = 0;
              double totalErr = 0, err;
              perViewErrors.clear();
              perViewErrors.resize(objectPoints.size());

              for (i = 0; i < (int)objectPoints.size(); ++i) {
                projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,
                              distCoeffs, imagePoints2);
                err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);
                int n = (int)objectPoints[i].size();
                perViewErrors[i] = (float) std::sqrt(err*err/n);
                totalErr += err*err;
                totalPoints += n;
              }
              return std::sqrt(totalErr/totalPoints);
        }
    }
}




bool MeasurementScene::rectify()
{

    //check if we are ok to proceed
    if(rectificationPoints.length() < 4 || currentMeasuringMode != Rectification || heightCm <= 0 || widthCm <= 0)
        return false;

    //TODO could move much of this functionality to MeasurementUtility
    //convert the QPixmap to cv::mat
    cv::Mat input = MeasurementUtility::qPixmapToCvMat(baseImage->pixmap());

    //corners of the input image, must be clockwise starting with top left
    std::vector<cv::Point2f> inputCorners;
    foreach(QGraphicsEllipseItem* p, rectificationPoints)
        inputCorners.push_back(cv::Point2f(p->scenePos().x(),p->scenePos().y()));

    //define the destination image dimensions
    double aspectRatio = heightCm/widthCm;
    //qDebug() << factor;


    int hborder = borderFactor * input.cols;
    int vborder = borderFactor * input.cols * aspectRatio;
    int outputWidth = input.cols + (2 * hborder);
    int outputHeight =  input.cols * aspectRatio + (2 * vborder);
    cv::Mat rectified = cv::Mat::zeros(outputHeight, outputWidth, input.type());

    //corners of the destination image
    std::vector<cv::Point2f> rectifiedCorners;
    rectifiedCorners.push_back(cv::Point2f(hborder, vborder));
    rectifiedCorners.push_back(cv::Point2f(rectified.cols - hborder, vborder));
    rectifiedCorners.push_back(cv::Point2f(rectified.cols - hborder, rectified.rows - vborder));
    rectifiedCorners.push_back(cv::Point2f(hborder, rectified.rows - vborder));


    //get transformation matrix
    cv::Mat transmtx = cv::getPerspectiveTransform(inputCorners, rectifiedCorners);

    //apply perspective transformation
    cv::warpPerspective(input, rectified, transmtx, rectified.size());
    //cv::imshow("fix", rectified);

    //write out the rectfied image for later reference
    //TODO constants
    QString path = currentImagePath + "_rectified.png";
    QFile file(path);
    file.open(QIODevice::WriteOnly);
    MeasurementUtility::cvMatToQPixmap(rectified).save(&file, "PNG");
    currentImagePath = path;

    return true;

}



void MeasurementScene::keyPressEvent(QKeyEvent *event){
    if(event->key() == Qt::Key_Delete || event->key() == Qt::Key_Backspace)
        foreach(QGraphicsItem* item, selectedItems()){
            QGraphicsEllipseItem* point = dynamic_cast<QGraphicsEllipseItem*>(item);
            if(!point)
                continue;

            foreach(MeasurementLine* l, calibrationLines)
                if(l->points.contains(point))
                {
                    l->removeFromScene(this);
                    calibrationLines.removeOne(l);
                }

            foreach(MeasurementLine* l, measurementLines)
                if(l->points.contains(point))
                {
                    l->removeFromScene(this);
                    measurementLines.removeOne(l);
                }

            foreach(QGraphicsEllipseItem* p, rectificationPoints)
                if(point == p)
                {
                    rectificationPoints.removeOne(p);
                    removeItem(p);
                    delete p;
                }

        }
    else
        QGraphicsScene::keyPressEvent(event);
}


void MeasurementScene::deleteMeasurementLines()
{
    foreach(MeasurementLine* l, measurementLines)
    {
        l->removeFromScene(this);
        measurementLines.removeOne(l);
    }
}

void MeasurementScene::deleteCalibrationLines()
{
    foreach(MeasurementLine* l, calibrationLines)
    {
        l->removeFromScene(this);
        calibrationLines.removeOne(l);
    }
}

void MeasurementScene::setRectificationPointVisiblity(bool visible)
{
    foreach(QGraphicsEllipseItem* p, rectificationPoints)
        p->setVisible(visible);
}

void MeasurementScene::setCalibrationLineVisiblity(bool visible)
{
    foreach(MeasurementLine* l, calibrationLines)
          l->setVisibility(visible);


}

