rmdir /s /q deploy
mkdir deploy
mkdir deploy\logs
mkdir deploy\templates
xcopy /sy ..\templates\* deploy\templates\
xcopy /sy c:\Tools\Deploy\vc\* deploy\
xcopy /sy c:\Tools\Deploy\openSSL\* deploy\
xcopy /sy ..\build\release\* deploy\
xcopy /y ..\Review.ini deploy\
xcopy /y ..\README* deploy\
xcopy /y ..\LICENSE* deploy\
C:\Qt\Qt5.7.0\5.7\msvc2015_64\bin\windeployqt.exe -qmldir ..\ deploy\Review.exe
"C:\Program Files\7-Zip\7z.exe" a review.7z deploy 