#ifndef REVIEWAPPLICATION_H
#define REVIEWAPPLICATION_H
#if defined(rApp)
#undef rApp
#endif
#define rApp (qobject_cast<ReviewApplication *>(QCoreApplication::instance()))

#include "videowindow.h"
#include "emdata.h"
#include "timelinewindow.h"
#include "measurementwindow.h"
#include "dataentrydialog.h"
#include "dataentryeventtype.h"
#include "eventdatatable.h"
#include "eventdatawindow.h"
#include "constants.h"
#include "stillimagewindow.h"
#include "treeviewwindow.h"
#include <QApplication>
#include <QSettings>
#include <QSplashScreen>
#include <QQuickView>



class ReviewApplication : public QApplication
{
    Q_OBJECT
public:
    explicit ReviewApplication(int &, char **);

    //overall app settings
    QSettings *settings;

    //state management
    QString currentTemplate;
    QString currentEmDataDirectory;
    QString currentDatabaseFile;

    //references to controls
    QObject *map;
    TimelineWindow *timeline;
    MeasurementWindow *measurement;
    TreeViewWindow *tree;
    QMap<int, StillImageWindow *> stillImageViewers;
    QMap<int, VideoWindow *> players;
    QMap<int, DataEntryDialog *> dataDialogs;
    QMap<int, EventDataTable *> tables;
    EventDataWindow *dataTableTabs;

    //descriptors for data entry types and markers
    QMap<int, DataEntryEventType *> eventTypes;

    //export type, set by template
    Constants::ExportFormat exportFormat;
    bool exportRowNums;
    QString exportTemplateFile;
    QString exportFileExtension;
    QString exportDateTimeFormat;


    //measurement
    bool measurementModeEnabled;
    DataEntryEventType *measurementEvent;
    int measurementLengthControlId;
    int measurementDecimals;
    int measurementChessboardHorizontalCornerCount;
    int measurementChessboardVerticalCornerCount;
    float measurementChessboardSquareSizeCm;

    //playback speed limits (2^factor)
    int playbackMaxFactor;
    int playbackMinFactor;

    //how many intermediate steps
    float speedSteps;

    //allow a certain amount of variance in the sync (scaled by by playback speed)
    int videoSyncPlayToleranceMillis;
    int videoSyncPauseToleranceMillis;

    //smaller values will adjust speed in smaller steps (always scaled by playback speed)
    double videoSyncStepFactor;

    //snap the random access clicks into the timeline to this number of seconds
    int timelineSnapSeconds;

    //should we expect a url reference with tokens in the toc files
    bool videoFromUrl;

    //fps used for frame step (might be different for different video files, so use an approximation)
    int defaultFps;

    //additional arguments passed to VLC at construction
    QStringList vlcAdditionalArgs;

    //should we use hw acceleration
    QString vlcHwAcceleration;

    int videoWindowRefreshMillis;

    //reveiewer generated events
    QList<DataEntryEventData *> eventData;

    //reference to raw em data
    EmData *emData;

    //splash/about
    QSplashScreen *splash;

    //application wide shortcuts
    QList<QShortcut *> applicationShortcuts;

    //data enrty form shortcuts
    QList<QShortcut *> dataEntryShortcuts;

    //version and build strings
    QString build;
    QString version;
    QString templateName;
    QString templateVersion;

signals:

public slots:
};

#endif // REVIEWAPPLICATION_H
