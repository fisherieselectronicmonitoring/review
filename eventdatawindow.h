#ifndef EVENTDATAWINDOW_H
#define EVENTDATAWINDOW_H

#include <QMainWindow>


namespace Ui {
class EventDataWindow;
}

class EventDataWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit EventDataWindow(QWidget *parent = 0);
    ~EventDataWindow();
     void clear();
     void addTab(QString, QWidget*);

private:
    Ui::EventDataWindow *ui;
};

#endif // EVENTDATAWINDOW_H
