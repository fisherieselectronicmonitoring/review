#include "emdatasensordurationevent.h"

EmDataSensorDurationEvent::EmDataSensorDurationEvent()
{

}

void EmDataSensorDurationEvent::setMarkerStyle()
{
   QCPItemRect *duration = qobject_cast<QCPItemRect *>(marker);
   if(durationMarkerStyle == EmDataSensorDurationEvent::DurationMarkerStyle::Shade)
   {
       duration->setPen(Qt::NoPen);
       duration->setBrush(QBrush(color));
       duration->setSelectedBrush(QBrush(color));
   }
   else if(durationMarkerStyle == EmDataSensorDurationEvent::DurationMarkerStyle::Line)
   {
       duration->setPen(QPen(color, 1, Qt::SolidLine));
       duration->setBrush(Qt::NoBrush);
       duration->setSelectedBrush(Qt::NoBrush);
   }

   //TODO global config
   duration->setSelectedPen(QPen(Qt::blue, 1, Qt::SolidLine));
   duration->setSelectable(true);

}
