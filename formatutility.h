#ifndef FORMATUTILITY_H
#define FORMATUTILITY_H

#include <QObject>
#include <QGeoCoordinate>

# include "constants.h"

class FormatUtility : public QObject
{
    Q_OBJECT
public:
    explicit FormatUtility(QObject *parent = 0);


    static QString getDegreeDecimalMinutes(double, Constants::Coordinate, int = Constants::DECIMAL_MINUTES_PRECISION);
    static QString getDegreeDecimalMinutes(double, int = Constants::DECIMAL_MINUTES_PRECISION);
    static QString getDegreeDecimalMinutes(QGeoCoordinate *, int = Constants::DECIMAL_MINUTES_PRECISION);

signals:

public slots:
};

#endif // FORMATUTILITY_H
