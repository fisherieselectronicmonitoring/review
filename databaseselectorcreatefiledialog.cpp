#include "databaseselectorcreatefiledialog.h"
#include <QDebug>

DatabaseSelectOrCreateFileDialog::DatabaseSelectOrCreateFileDialog()
{
    connect(this, SIGNAL(currentChanged(const QString &)), this, SLOT(setButtonText()));

    //if there is only a single database file, default to that one
    QFileInfoList files = directory().entryInfoList();
    int dbCount = 0;
    QString selected;
    foreach(QFileInfo f, files)
    {
        if(f.fileName().endsWith(".sqlite3"))
        {
            dbCount++;
            selected = f.fileName();
        }
    }
    if(dbCount >= 1) {
        setWindowTitle("Select a database file or create a new one");
        setLabelText(QFileDialog::Accept, "Open");
    }
    else {
         setWindowTitle("Create a database file");
         setLabelText(QFileDialog::Accept, "Save");
    }

    if(dbCount == 1)
         selectFile(selected);



}

void DatabaseSelectOrCreateFileDialog::setButtonText()
{
    if(selectedFiles().length() > 0 && !selectedFiles().at(0).isEmpty())
    {
        QFileInfo f(selectedFiles().at(0));
        if(f.exists())
        {
            //setAcceptMode(QFileDialog::AcceptOpen);
            setLabelText(DialogLabel::Accept, "Open");
            qDebug() << "button text should be switched to SELECT";
        }
        else
        {
            //setAcceptMode(QFileDialog::AcceptSave);
            setLabelText(DialogLabel::Accept, "Save");
            qDebug() << "button text should be switched to SAVE";
        }
    }
}
