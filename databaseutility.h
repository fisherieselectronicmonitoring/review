#ifndef DATABASEUTILITY_H
#define DATABASEUTILITY_H

#include "dataentryeventdata.h"

#include <QObject>
#include <QSqlDatabase>
#include <QJsonDocument>
#include <QReadWriteLock>

class DatabaseUtility : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseUtility(QObject *parent = 0);
    static int store(QJsonDocument);
    static void update(int, QJsonDocument);
    static QJsonDocument retrieve(int);
    static void remove(int);
    static void removeAll();
    static void initializeDatabase();
    static QMap<int, QJsonDocument> retrieveAll();
    //static QMap<int, QJsonDocument> retrieveAllFromFile(QString);
    static void storeMetadata(QString, QString, bool);
    static QString retrieveMetadata(QString);
    static QList<int> batchStore(QList<QJsonDocument>);
    static void batchRemove(QList<int>);

private:
    static void openDatabase();
    static void closeDatabase();
    static QSqlDatabase db;
    static QReadWriteLock lock;
    
signals:

public slots:
};

#endif // DATABASEUTILITY_H
