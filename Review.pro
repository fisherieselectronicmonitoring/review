#VLC 2.x
VLC_SDK = /Users/eric/workspace/libraries/VLC/sdk
VLC_QT = /Users/eric/workspace/libraries/VLC-Qt_1.1.0_win64_msvc2015
VLC_QT_BIN = C:\\Users\\eric\\workspace\\libraries\\VLC-Qt_1.1.0_win64_msvc2015\\bin
OPENCV = /Users/eric/workspace/libraries/opencv300_win64_msvc2013/install/
OPENCV_BIN = C:\\Users\\eric\\workspace\\libraries\\opencv300_win64_msvc2013\\install\\x64\\vc12\\bin
BOOST = /Users/eric/workspace/libraries/boost_1_60_0
FFMPEG_BIN = C:\\Users\\eric\\workspace\\libraries\\ffmpeg-3.2.2-win64-static\\bin
STACKWALKER = /Users/eric/workspace/libraries/StackWalker

DEFINES += QT_MESSAGELOGCONTEXT
DEFINES += QCUSTOMPLOT_USE_OPENGL

LIBS += -lOpenGL32

TARGET       = Review
TEMPLATE     = app
CONFIG 	    += c++11

QT          += core gui widgets quick positioning sql printsupport opengl script

SOURCES     += main.cpp \
    emdata.cpp \
    reviewapplication.cpp \
    measurementutility.cpp \
    measurementscene.cpp \
    measurementview.cpp \
    dataentrydialog.cpp \
    dataentrywidgetwrapper.cpp \
    emdataload.cpp \
    measurementline.cpp \
    emdataseries.cpp \
    measurementwindow.cpp \
    videowindow.cpp \
    timelinewindow.cpp \
    dataentryeventtype.cpp \
    dataentryeventdata.cpp \
    formatutility.cpp \
    databaseutility.cpp \
    emdatasensorevent.cpp \
    emdatasensordurationevent.cpp \
    emdatasensorpointevent.cpp \
    mapconfig.cpp \
    eventdatawindow.cpp \
    eventdatatable.cpp \
    emdatavideoduration.cpp \
    emdatastillimageduration.cpp \
    stillimagewindow.cpp \
    stillimageview.cpp \
    archivedialog.cpp \
    treeviewwindow.cpp \
    databaseselectorcreatefiledialog.cpp \
    emdataenumevent.cpp

HEADERS     += \
    emdata.h \
    reviewapplication.h \
    measurementutility.h \
    measurementscene.h \
    measurementview.h \
    dataentrydialog.h \
    dataentrywidgetwrapper.h \
    emdataload.h \
    measurementline.h \
    emdataseries.h \
    measurementwindow.h \
    videowindow.h \
    timelinewindow.h \
    dataentryeventtype.h \
    dataentryeventdata.h \
    formatutility.h \
    constants.h \
    databaseutility.h \
    emdatasensorevent.h \
    emdatasensordurationevent.h \
    emdatasensorpointevent.h \
    mapconfig.h \
    eventdatawindow.h \
    eventdatatable.h \
    emdatavideoduration.h \
    emdatastillimageduration.h \
    stillimagewindow.h \
    stillimageview.h \
    archivedialog.h \
    treeviewwindow.h \
    databaseselectorcreatefiledialog.h \
    inja/bytecode.hpp \
    inja/config.hpp \
    inja/environment.hpp \
    inja/function_storage.hpp \
    inja/inja.hpp \
    inja/json.hpp \
    inja/lexer.hpp \
    inja/parser.hpp \
    inja/polyfill.hpp \
    inja/renderer.hpp \
    inja/string_view.hpp \
    inja/template.hpp \
    inja/token.hpp \
    inja/utils.hpp \
    inja/nlohmann/json.hpp \
    nlohmann/json.hpp \
    emdataenumevent.h \
    dataentryfieldvalue.h


FORMS       += \
    dataentrydialog.ui \
    measurementwindow.ui \
    videowindow.ui \
    timelinewindow.ui \
    eventdatawindow.ui \
    eventdatatable.ui \
    stillimagewindow.ui \
    archivedialog.ui \
    treeviewwindow.ui
    
include(alglib.pri)
include(qcustomplot.pri)

#libvlc
INCLUDEPATH += $$VLC_SDK/include
LIBS        += -L$$VLC_SDK/lib -llibvlc

#stackwalker
INCLUDEPATH += $$STACKWALKER
LIBS        += $$STACKWALKER/x64/Release/StackWalker.lib

#boost
INCLUDEPATH += $$BOOST

#vlc qt
INCLUDEPATH += $$VLC_QT/include

#opencv
INCLUDEPATH += $$OPENCV/include

#opencv debug
CONFIG(debug, debug|release):LIBS += -L$$OPENCV/x64/vc12/lib -lopencv_core300d -lopencv_highgui300d -lopencv_imgproc300d -lopencv_calib3d300d
CONFIG(debug, debug|release):QMAKE_POST_LINK += "xcopy /sy $$OPENCV_BIN\*300d.dll .\\debug\\ & "

#opencv release
CONFIG(release, debug|release):LIBS += -L$$OPENCV/x64/vc12/lib -lopencv_core300 -lopencv_highgui300 -lopencv_imgproc300 -lopencv_calib3d300
CONFIG(release, debug|release):QMAKE_POST_LINK += "xcopy /sy $$OPENCV_BIN\*300.dll .\\release\\ & "

#vlc qt debug
CONFIG(debug, debug|release):LIBS += -L$$VLC_QT/lib -lVLCQtCored -lVLCQtWidgetsd
CONFIG(debug, debug|release):QMAKE_POST_LINK += "xcopy /sy $$VLC_QT_BIN\* .\\debug\\ & "

#vlc qt release
CONFIG(release, debug|release):LIBS += -L$$VLC_QT/lib -lVLCQtCore -lVLCQtWidgets
CONFIG(release, debug|release):QMAKE_POST_LINK += "xcopy /sy $$VLC_QT_BIN\* .\\release\\ & "

#ffmpeg
CONFIG(debug, debug|release):QMAKE_POST_LINK += "xcopy /sy $$FFMPEG_BIN\* .\\debug\\ & "
CONFIG(release, debug|release):QMAKE_POST_LINK += "xcopy /sy $$FFMPEG_BIN\* .\\release\\ & "


DISTFILES += \
    Review.ini \
    mappolyline.qml \
    mapwindow.qml \
    version.txt \
    LICENSE \
    templates/example-annotated.ini \
    templates/field-trial-pollock-trawl.ini \
    templates/example-windows.ini \
    README.md \
    README.ReleaseNotes \
    README.Commands \
    templates/HerringEchosounder_V4.ini

RESOURCES += \
    res.qrc

QTPLUGIN += QGeoServiceProviderFactoryFlosm

CHANGESET=$$system(hg parents --template "{short(node)}")
DEFINES += BUILD_CHANGESET=\\\"$$CHANGESET\\\"







