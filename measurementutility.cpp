#include "measurementutility.h"
#include "interpolation.h"
#include "stdafx.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <QPointF>
#include <QVector3D>
#include <qDebug>


#include <QImage>
#include <QPixmap>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"


using namespace alglib;

MeasurementUtility::MeasurementUtility(QObject *parent) : QObject(parent)
{

}



double MeasurementUtility::interpolate(QList<QVector3D> samples, QPointF point)
{

    rbfmodel model;
    rbfcreate(2, 1, model);


    /*real_2d_array xy;
    foreach(QVector3D p, samples)
        xy(p.x(), p.y()) = p.z();
    */


    QString s = "[";
    foreach(QVector3D p, samples)
        s+= QString("[%1,%2,%3],").arg(p.x()).arg(p.y()).arg(p.z());
    s.replace(s.length()-1,1,"]");
    //qDebug() << s;
    //real_2d_array xy = "[[100.1,95,1.2],[0,0,4],[110,0,3],[0,120,3]]";

    real_2d_array xy = s.toStdString().c_str();

    rbfsetpoints(model, xy);

    rbfreport rep;
    //rbfsetalgoqnn(model);
    rbfsetalgomultilayer(model,10,20);
    rbfbuildmodel(model, rep);
    //printf("%d\n", int(rep.terminationtype)); // EXPECTED: 1
    return rbfcalc2(model, point.x(),point.y());
}


//TODO - these could be moved to an image utility class
QImage MeasurementUtility::cvMatToQImage( const cv::Mat &inMat )
{
  switch ( inMat.type() )
  {
     // 8-bit, 4 channel
     case CV_8UC4:
     {
        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );

        return image;
     }

     // 8-bit, 3 channel
     case CV_8UC3:
     {
        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );

        return image.rgbSwapped();
     }

     // 8-bit, 1 channel
     case CV_8UC1:
     {
        static QVector<QRgb>  sColorTable;

        // only create our color table once
        if ( sColorTable.isEmpty() )
        {
           for ( int i = 0; i < 256; ++i )
              sColorTable.push_back( qRgb( i, i, i ) );
        }

        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );

        image.setColorTable( sColorTable );

        return image;
     }

     default:
        qWarning() << "cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
        break;
  }

  return QImage();
}

QPixmap MeasurementUtility::cvMatToQPixmap( const cv::Mat &inMat )
{
  return QPixmap::fromImage( cvMatToQImage( inMat ) );
}


cv::Mat MeasurementUtility::qImageToCvMat( const QImage &inImage )
{
    switch ( inImage.format() )
    {
    // 8-bit, 4 channel
    case QImage::Format_RGB32:
    {
        cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC4, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

        return mat.clone();
    }

        // 8-bit, 3 channel
    case QImage::Format_RGB888:
    {

        QImage   swapped = inImage.rgbSwapped();

        return cv::Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
    }

        // 8-bit, 1 channel
    case QImage::Format_Indexed8:
    {
        cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC1, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

        return mat.clone();
    }

    default:
        qWarning() << "QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
        break;
    }

    return cv::Mat();
}

cv::Mat MeasurementUtility::qPixmapToCvMat( const QPixmap &inPixmap)
{
  return qImageToCvMat( inPixmap.toImage());
}

