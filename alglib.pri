INCLUDEPATH += alglib

HEADERS += \
    $$PWD/alglib/alglibinternal.h \
    $$PWD/alglib/alglibmisc.h \
    $$PWD/alglib/ap.h \
    $$PWD/alglib/dataanalysis.h \
    $$PWD/alglib/diffequations.h \
    $$PWD/alglib/fasttransforms.h \
    $$PWD/alglib/integration.h \
    $$PWD/alglib/interpolation.h \
    $$PWD/alglib/linalg.h \
    $$PWD/alglib/optimization.h \
    $$PWD/alglib/solvers.h \
    $$PWD/alglib/specialfunctions.h \
    $$PWD/alglib/statistics.h \
    $$PWD/alglib/stdafx.h

SOURCES += \
    $$PWD/alglib/alglibinternal.cpp \
    $$PWD/alglib/alglibmisc.cpp \
    $$PWD/alglib/ap.cpp \
    $$PWD/alglib/dataanalysis.cpp \
    $$PWD/alglib/diffequations.cpp \
    $$PWD/alglib/fasttransforms.cpp \
    $$PWD/alglib/integration.cpp \
    $$PWD/alglib/interpolation.cpp \
    $$PWD/alglib/linalg.cpp \
    $$PWD/alglib/optimization.cpp \
    $$PWD/alglib/solvers.cpp \
    $$PWD/alglib/specialfunctions.cpp \
    $$PWD/alglib/statistics.cpp


