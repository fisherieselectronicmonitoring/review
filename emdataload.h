#ifndef EMDATALOAD_H
#define EMDATALOAD_H

#include <QObject>
#include <QDir>
#include <QFile>
#include <QSettings>

#include "emdata.h"

class EmDataLoad : public QObject
{
    Q_OBJECT
public:
    explicit EmDataLoad(QObject *parent = 0);
    static bool load(QStringList*);
    //static bool loadTemplateEventsOnly();

private:
    static void loadPrimaryDataFile(int, QSettings *, QList<QList<QString>>);
    static void loadVideoTocFile(QString, int, int, QSettings *, QList<QList<QString>>);
    static void loadStillImageTocFile(int, int, QSettings *, QList<QList<QString>>);
    static void loadSmallBoatStillImageTocFile(int, int, QSettings *, QList<QList<QString>>);
    static void loadPsmfcVideoTocFile(int, QSettings *, QList<QList<QString>>);
    static void loadPsmfcEventLogFile(int, QSettings *, QList<QList<QString>>);
    static void loadSensorDurationEventFile(int, QSettings *, QList<QList<QString>>);
    static void loadSecondarySensorLogFile(int, QSettings *, QList<QList<QString>>);
    static void loadSecondaryMultiSensorLogFile(int, QSettings *, QList<QList<QString>>);
    static void loadSystemEventLogFile(int, QSettings *, QList<QList<QString>>);
    static void loadEventLogFile(int, QSettings *, QList<QList<QString>>);
    static void loadDataEntryTemplates(int, QSettings *, QString);
    static void loadGeneralSettings(QSettings *);
    static bool initializeDatabase(QSettings *);



signals:

public slots:
};

#endif // DATALOAD_H
