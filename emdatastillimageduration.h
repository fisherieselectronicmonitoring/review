#ifndef EMDATASTILLIMAGEDURATION_H
#define EMDATASTILLIMAGEDURATION_H

#include <QObject>

class EmDataStillImageDuration : public QObject
{
    Q_OBJECT
public:
    explicit EmDataStillImageDuration(QObject *parent = 0);
    ~EmDataStillImageDuration();
    double startSeconds;
    double endSeconds;
    int channel;
    QString fileName;
signals:

public slots:
};

#endif // EMDATASTILLIMAGEDURATION_H
