#ifndef STILLIMAGEWINDOW_H
#define STILLIMAGEWINDOW_H

#include "stillimageview.h"
#include <QMainWindow>

namespace Ui {
class StillImageWindow;
}

class StillImageWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit StillImageWindow(QWidget *parent = 0);
    ~StillImageWindow();

public slots:
    void setImage(QString path);
    void clear();

private:
    Ui::StillImageWindow *ui;
    QGraphicsScene *scene;
    StillImageView* view;
    QString currentPath;

protected:
    virtual void closeEvent(QCloseEvent*);
};

#endif // STILLIMAGEWINDOW_H
