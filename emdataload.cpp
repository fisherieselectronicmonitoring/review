#include "emdataload.h"
#include "reviewapplication.h"
#include "dataentrydialog.h"
#include "dataentryeventtype.h"
#include "emdatasensordurationevent.h"
#include "databaseutility.h"
#include "eventdatatable.h"
#include "emdatastillimageduration.h"

#include <QElapsedTimer>
#include <QVector>
#include <QTextStream>
#include <QRegularExpression>
#include <QSettings>
#include <QTimeZone>
#include <QScriptEngine>

EmDataLoad::EmDataLoad(QObject *parent) : QObject(parent)
{
}

bool EmDataLoad::load(QStringList *errorMessages)
{
    //read the template
    QSettings *tmplt = new QSettings(rApp->currentTemplate, QSettings::IniFormat);
    tmplt->beginGroup("review_template");

    //init global reference for current emdata
    rApp->emData = new EmData();

    //for each file in template
    bool dataFilesFound = false;
    for(int fileNumber = 1;;fileNumber++)
    {
        //get the filename, if there isn't one, break
        QString fileNameFilter = tmplt->value(QString("file/%1/name").arg(fileNumber)).toString();
        if(fileNameFilter.isEmpty())
            break;

        //is this a globbed file name?
        QDir emDir(rApp->currentEmDataDirectory);
        emDir.setNameFilters(QList<QString>() << fileNameFilter);
        emDir.setSorting(QDir::Name);
        QList<QString> fileList = emDir.entryList();

        for(int c = 0; c < fileList.length(); c++)
        {
            QString f = fileList.at(c);
            qDebug() << "processing data file" << f;

            //parse the CSV file into a qlist of qstring qlists
            QList<QList<QString>> csv;
            QFile file(QString("%1/%2").arg(rApp->currentEmDataDirectory).arg(f));
            if (file.open(QIODevice::ReadOnly)) {
                dataFilesFound = true;
                QString tempStr;
                QChar tempChar;
                QList<QString> tempList;
                QTextStream textStream(&file);

                while (!textStream.atEnd()) {
                    textStream >> tempChar;
                    if(tempChar == '\r') {
                        //do nothing
                    }
                    else if(tempChar.toLatin1() == 0 ) { //strip binary data
                        //do nothing
                    }
                    else if (tempChar == ',') {
                       tempList.append(tempStr);
                       tempStr.clear();
                    }
                    else if(tempChar == '\n') {
                        tempList.append(tempStr);
                        csv.append(tempList);
                        tempList.clear();
                        tempStr.clear();
                    }
                    else {
                        tempStr.append(tempChar);
                    }
                }

                qDebug() << "csv load complete";
            }
            else
            {
                qDebug() << "em data file not found: " + f;
                continue;
            }

            QString fileType = tmplt->value(QString("file/%1/type").arg(fileNumber)).toString();

            if(fileType == "PRIMARY")
                loadPrimaryDataFile(fileNumber, tmplt, csv);
            //else if(fileType == "SENSOR_EVENT_DURATION")
            //    loadSensorDurationEventFile(fileNumber, tmplt, csv);
            else if(fileType == "EVENT_LOG")
                loadEventLogFile(fileNumber, tmplt, csv);
            //else if(fileType == "SYSTEM_EVENT_LOG")
            //    loadSystemEventLogFile(fileNumber, tmplt, csv);
            else if(fileType == "SECONDARY_SENSOR")
                loadSecondarySensorLogFile(fileNumber, tmplt, csv);
            else if(fileType == "SECONDARY_MULTI_SENSOR")
                loadSecondaryMultiSensorLogFile(fileNumber, tmplt, csv);
            else if(fileType == "VIDEO_TOC")
                loadVideoTocFile(QFileInfo(file).baseName(), fileNumber, c, tmplt, csv);
            else if(fileType == "STILL_IMAGE_TOC")
                loadStillImageTocFile(fileNumber, c, tmplt, csv);
            else if(fileType == "SMALL_BOAT_STILL_IMAGE_TOC")
                loadSmallBoatStillImageTocFile(fileNumber, c, tmplt, csv);
            else if(fileType == "PSMFC_VIDEO_TOC")
                loadPsmfcVideoTocFile(fileNumber, tmplt, csv);
            else if(fileType == "PSMFC_EVENT_LOG")
                loadPsmfcEventLogFile(fileNumber, tmplt, csv);

        }
    }
    //done with file loads

    //give up if no data files were found
    if(!dataFilesFound)
    {
        qDebug() << "no data files found";
        errorMessages->append("no data files found");
        return false;
    }

    //for each event in template
    for(int eventNumber = 1;;eventNumber++)
    {
        //get the event name, if there isn't one, break
        QString eventName = tmplt->value(QString("event/%1/name").arg(eventNumber)).toString();
        if(eventName.isEmpty())
            break;

        loadDataEntryTemplates(eventNumber, tmplt, eventName);
    }

    //load measurement, min max values etc
    loadGeneralSettings(tmplt);

    //don't load the database file name out of the template any more
    //QString dbFileName = tmplt->value("database_file/name").toString();
    //rApp->currentDatabaseFile = QString("%1/%2").arg(rApp->currentEmDataDirectory).arg(dbFileName);

    //get current version of software and template
    if(!initializeDatabase(tmplt))
    {
        errorMessages->append("error initializing database");
        return false;
    }

    //load is complete
    tmplt->endGroup();

    return true;
}

/*
bool EmDataLoad::loadTemplateEventsOnly()
{
    QSettings *tmplt = new QSettings(rApp->currentTemplate, QSettings::IniFormat);
    tmplt->beginGroup("review_template");

    //for each event in template
    for(int eventNumber = 1;;eventNumber++)
    {
        //get the event name, if there isn't one, break
        QString eventName = tmplt->value(QString("event/%1/name").arg(eventNumber)).toString();
        if(eventName.isEmpty())
            break;

        loadDataEntryTemplates(eventNumber, tmplt, eventName);
    }

    //load measurement, min max values etc
    loadGeneralSettings(tmplt);

    //load is complete
    tmplt->endGroup();

    return true;
}
*/





bool EmDataLoad::initializeDatabase(QSettings *tmplt)
{

    try
    {
        DatabaseUtility::initializeDatabase();

    }
    catch (const std::exception &exc)
    {
        qDebug() << QString("failure initializing database").arg(QString(exc.what()));

        return false;
    }



     QMap<int, QJsonDocument> events = DatabaseUtility::retrieveAll();
     foreach(int key, events.keys())
     {

        DataEntryEventData *event = new DataEntryEventData();

        if(event->fromJson(events.value(key), key))
            rApp->eventData.append(event);
     }

     //set current version of software and template in the database - do not overwrite if these values have already been set and events created
     DatabaseUtility::storeMetadata("SOFTWARE_VERSION", rApp->version, events.size() == 0);
     DatabaseUtility::storeMetadata("SOFTWARE_BUILD", rApp->build, events.size() == 0);
     DatabaseUtility::storeMetadata("TEMPLATE_NAME", tmplt->value("template/name").toString(), events.size() == 0);
     DatabaseUtility::storeMetadata("TEMPLATE_VERSION", tmplt->value("template/version").toString(), events.size() == 0);

     return true;
}





void EmDataLoad::loadGeneralSettings(QSettings *tmplt)
{
    //set the template name and version currently in use
    rApp->templateName = tmplt->value("template/name").toString();
    rApp->templateVersion = tmplt->value("template/version").toString();


    //setup measurement - this must happen after we load events so that we can link the measurement to it's event
    if(tmplt->value("measurement_mode_enabled").toBool())
    {
        rApp->measurementModeEnabled = true;
        rApp->measurementEvent = rApp->eventTypes.value(tmplt->value("measurement_event_id").toInt());
        rApp->measurementLengthControlId = tmplt->value("measurement_length_control_id").toInt();
        rApp->measurementDecimals = tmplt->value("measurement_decimals",1).toInt();
        rApp->measurementChessboardHorizontalCornerCount = tmplt->value("measurement_chessboard_horizontal_corner_count",9).toInt();
        rApp->measurementChessboardVerticalCornerCount =  tmplt->value("measurement_chessboard_vertical_corner_count",6).toInt();
        rApp->measurementChessboardSquareSizeCm =  tmplt->value("measurement_chessboard_square_size_cm",2.5).toFloat();
    }
    else
        rApp->measurementModeEnabled = false;

    //get default FPS for frame jumping
    rApp->defaultFps = tmplt->value("default_frame_step_fps",5).toInt();

    //get the resource type for video files
    rApp->videoFromUrl = tmplt->value("video_from_url", false).toBool();

    rApp->vlcAdditionalArgs = tmplt->value("vlc_additional_arguments").toStringList();
    rApp->vlcHwAcceleration = tmplt->value("vlc_hw_acceleration").toString();
    rApp->videoWindowRefreshMillis = tmplt->value("video_window_update_millis",500).toInt();

    rApp->videoSyncPlayToleranceMillis = tmplt->value("video_sync_play_tolerance_millis",1500).toInt();
    rApp->videoSyncPauseToleranceMillis = tmplt->value("video_sync_pause_tolerance_millis",1000).toInt();
    rApp->videoSyncStepFactor = tmplt->value("video_sync_step_factor",.00001).toDouble();

    rApp->timelineSnapSeconds = tmplt->value("timeline_snap_seconds",0).toInt();


    //default to csv export
    if(tmplt->value("export_format").toString() == "JSON")
    {
        rApp->exportTemplateFile = tmplt->value("export_template","").toString();
        rApp->exportFormat = Constants::ExportFormat::JSON;
    }
    else
        rApp->exportFormat = Constants::ExportFormat::CSV;

    rApp->exportRowNums = false;
    if(tmplt->value("export_show_rownum").toBool())
        rApp->exportRowNums = true;


    //if this is not set, UTC seconds will be used
    rApp->exportDateTimeFormat = tmplt->value("export_date_time_format","").toString();

    rApp->exportFileExtension = tmplt->value("export_file_extension","csv").toString();

    //set the min and max values that should be graphed
    rApp->emData->maxValue = tmplt->value("max_graph_y_value").toDouble();
    rApp->emData->minValue = tmplt->value("min_graph_y_value").toDouble();

    rApp->playbackMaxFactor = tmplt->value("playback_speed_max_factor",4).toInt();
    rApp->playbackMinFactor = tmplt->value("playback_speed_min_factor",-4).toInt();
    rApp->speedSteps = tmplt->value("playback_speed_steps",1).toFloat();



}









void EmDataLoad::loadVideoTocFile(QString tocFileName, int fileNumber, int channelNumber, QSettings *tmplt, QList<QList<QString>> csv)
{

    qDebug() << "loading TOC file" << tocFileName;

    foreach(QList<QString> line, csv) {
       EmDataVideoDuration* vd = new EmDataVideoDuration;
       vd->fileName = line.at(0);
       vd->startSeconds = line.at(1).toDouble()/1000;
       vd->videoChannel = channelNumber;
       vd->endSeconds = line.at(2).toDouble()/1000;
       rApp->emData->videosByName.insert(vd->fileName, vd);
       rApp->emData->videosByChannel.insert(vd->videoChannel, vd);
       rApp->emData->videosByStart.insert(vd->startSeconds, vd);
    }

    rApp->emData->videoChannelNames.insert(channelNumber, tocFileName);

    if (channelNumber + 1 > rApp->emData->videoChannels)
        rApp->emData->videoChannels = channelNumber + 1;


}

void EmDataLoad::loadStillImageTocFile(int fileNumber, int channelNumber, QSettings *tmplt, QList<QList<QString>> csv)
{

    foreach(QList<QString> line, csv) {
       //make image link objects
       EmDataStillImageDuration* sid = new EmDataStillImageDuration;
       sid->fileName = line.at(0);
       sid->startSeconds = line.at(1).toDouble()/1000;
       sid->channel = channelNumber;
       sid->endSeconds = line.at(2).toDouble()/1000;
       rApp->emData->stillImagesByName.insert(sid->fileName, sid);
       rApp->emData->stillImagesByStart.insert(sid->startSeconds, sid);
    }

    if (channelNumber + 1 > rApp->emData->stillImageChannels)
        rApp->emData->stillImageChannels = channelNumber + 1;



}


void EmDataLoad::loadSmallBoatStillImageTocFile(int fileNumber, int channelNumber, QSettings *tmplt, QList<QList<QString>> csv)
{

    QList<EmDataStillImageDuration*> sidList;
    foreach(QList<QString> line, csv) {
       //make image link objects
       EmDataStillImageDuration* sid = new EmDataStillImageDuration;
       QDateTime time = QDateTime::fromString(QString(line.at(0)).append(line.at(1)), "yyyyMMddHHmmss.z");
       time.setTimeSpec(Qt::UTC);
       sid->startSeconds = time.toMSecsSinceEpoch()/(double)1000.0;
       sid->fileName = time.toString("yyyyMMddTHHmmss").append(".jpg");
       sid->channel = channelNumber;
       //default duration to 1 second
       sid->endSeconds = sid->startSeconds + 1;
       rApp->emData->stillImagesByName.insert(sid->fileName, sid);
       rApp->emData->stillImagesByStart.insert(sid->startSeconds, sid);
       sidList.append(sid);
    }

    //set the end time to the start time of the next image -1 millisecond
    for(int x = 0; x < sidList.size() -1; x++)
        sidList.at(x)->endSeconds = sidList.at(x+1)->startSeconds - 0.001;

    if (channelNumber + 1 > rApp->emData->stillImageChannels)
        rApp->emData->stillImageChannels = channelNumber + 1;



}


void EmDataLoad::loadPsmfcVideoTocFile(int fileNumber, QSettings *tmplt, QList<QList<QString>> csv)
{

    //get the format string for the date time value
    QString dateTimeformat = tmplt->value(QString("file/%1/date_time_format_string").arg(fileNumber)).toString();

      //date & time
    int dateIdx = tmplt->value(QString("file/%1/date_field_index").arg(fileNumber),-1).toInt();
    int timeIdx = tmplt->value(QString("file/%1/time_field_index").arg(fileNumber),-1).toInt();
    //int milliIdx = tmplt->value(QString("file/%1/millisecond_field_index").arg(fileNumber),-1).toInt();

    //determine the number of video channels
    rApp->emData->videoChannels = tmplt->value(QString("file/%1/video_channel_count").arg(fileNumber),-1).toInt();

    int fileNameIdx = tmplt->value(QString("file/%1/video_filename_field_index").arg(fileNumber),-1).toInt();
    int chanIdx = tmplt->value(QString("file/%1/video_channel_field_index").arg(fileNumber),-1).toInt();
    int videoLengthHoursIdx = tmplt->value(QString("file/%1/video_length_hours_field_index").arg(fileNumber),-1).toInt();

    QList<int> channels;

    foreach(QList<QString> line, csv) {

        //skip the first line
        if(line[0] == "count")
            continue;

        //concat date time if necesarry
        QString dateTime = line[dateIdx] + line[timeIdx]; // + line[milliIdx];

        QDateTime time = QDateTime::fromString(dateTime, dateTimeformat).addYears(100);
        time.setTimeSpec(Qt::UTC);

        double startSeconds = (double)time.toTime_t();

        //make video link objects

       QString fn = line[fileNameIdx];
       fn.append(".MP4");

       EmDataVideoDuration* vd = new EmDataVideoDuration;
       vd->fileName = fn;
       vd->startSeconds = startSeconds;
       //qDebug() << "video duration start: " << vd->startSeconds << " last seconds: " << seconds.last() << " offset: " << line[videoOffsetIdx].toInt();
       vd->videoChannel = line[chanIdx].toInt() - 1;
       vd->endSeconds = startSeconds + (line[videoLengthHoursIdx].toDouble() * 60 * 60);
       rApp->emData->videosByName.insert(vd->fileName, vd);
       rApp->emData->videosByChannel.insert(vd->videoChannel, vd);
       rApp->emData->videosByStart.insert(vd->startSeconds, vd);
    }

    foreach(int channelNumber, rApp->emData->videosByChannel.keys())
    {
        QString channelName = "Channel " + QString::number(channelNumber + 1);
        rApp->emData->videoChannelNames.insert(channelNumber,channelName);

        if (channelNumber + 1 > rApp->emData->videoChannels)
            rApp->emData->videoChannels = channelNumber + 1;
    }

}



void EmDataLoad::loadDataEntryTemplates(int eventNumber, QSettings *tmplt, QString eventName)
{
    //check event status
    if(tmplt->value(QString("event/%1/status").arg(eventNumber),"ACTIVE").toString() == "INACTIVE")
        return;

    //create and set entry type
    QString eventMarkerType = tmplt->value(QString("event/%1/marker_type").arg(eventNumber)).toString();
    QString eventColor = tmplt->value(QString("event/%1/color").arg(eventNumber),"brown").toString();
    //default this to opaque
    int eventAlpha = tmplt->value(QString("event/%1/alpha").arg(eventNumber),255).toInt();
    int width = tmplt->value(QString("event/%1/width").arg(eventNumber),1).toInt();
    QString durationMarkerStyle = tmplt->value(QString("event/%1/style").arg(eventNumber),"SHADE").toString();
    QString lineStyle =  tmplt->value(QString("event/%1/line_style").arg(eventNumber),"SOLID_LINE").toString();

    bool parentRequired = tmplt->value(QString("event/%1/parent_required").arg(eventNumber),false).toBool();
    int parentEventId = tmplt->value(QString("event/%1/parent_event_id").arg(eventNumber),-1).toInt();

    //add the optional hotkey with an optional list of default field values
    QList<QStringList> hotkeyList;
    for (int i = 1; ; i++)
    {
        QStringList hotkey = tmplt->value(QString("event/%1/hotkey/%2").arg(eventNumber).arg(i),QStringList()).toStringList();
        if(hotkey.isEmpty())
            break;
        else
            hotkeyList.append(hotkey);
    }
    bool hotkeyAuto = tmplt->value(QString("event/%1/hotkey_auto_ok").arg(eventNumber),false).toBool();

    DataEntryEventType *eventType = new DataEntryEventType();

    //TODO constants
    if(eventMarkerType == "POINT")
    {
        eventType->markerType = eventType->MarkerType::Point;
        eventType->markerTop = tmplt->value(QString("event/%1/plot_top").arg(eventNumber),1).toFloat();
        eventType->markerBottom = tmplt->value(QString("event/%1/plot_bottom").arg(eventNumber),.5).toFloat();
    }
    else if(eventMarkerType == "DURATION")
    {
        eventType->markerType = eventType->MarkerType::Duration;
        eventType->markerTop = tmplt->value(QString("event/%1/plot_top").arg(eventNumber),0.9).toFloat();
        eventType->markerBottom = tmplt->value(QString("event/%1/plot_bottom").arg(eventNumber),0).toFloat();
    }

    if(durationMarkerStyle == "SHADE")
        eventType->durationMarkerStyle = eventType->DurationMarkerStyle::Shade;
    else if(durationMarkerStyle == "LINE")
        eventType->durationMarkerStyle = eventType->DurationMarkerStyle::Line;

    if(lineStyle == "SOLID_LINE")
        eventType->LineStyle = Qt::SolidLine;
    else if(lineStyle == "DASH_LINE")
        eventType->LineStyle = Qt::DashLine;
    else if(lineStyle == "DOT_LINE")
        eventType->LineStyle = Qt::DotLine;
    else if(lineStyle == "DASH_DOT_LINE")
        eventType->LineStyle = Qt::DashDotLine;
    else if(lineStyle == "DASH_DOT_DOT_LINE")
        eventType->LineStyle = Qt::DashDotDotLine;

    eventType->id = eventNumber;
    eventType->name = eventName;
    eventType->color = QColor(eventColor);
    eventType->width = width;
    eventType->color.setAlpha(eventAlpha);
    eventType->parentRequired = parentRequired;
    eventType->parentEventId = parentEventId;

    //add the types to a globally accessable list for use by other controls
    rApp->eventTypes.insert(eventType->id, eventType);

    //build the dialog
    DataEntryDialog *dialog = new DataEntryDialog();


    //load hotkeys
    if(!hotkeyList.isEmpty())
    {
        foreach(QStringList hotkey, hotkeyList)
        {
            //parse out the values in the hotkey
            QList<DataEntryFieldValue> hotkeyPresets;

            //skip the first value as that is the hotkey defintion
            for (int i = 1; i < hotkey.size(); i++)
            {
                QStringList raw = hotkey[i].split(":");
                DataEntryFieldValue fv;
                fv.key = raw[0].toInt();
                fv.value = raw[1];
                hotkeyPresets.append(fv);
            }

            QShortcut* sc = new QShortcut(QKeySequence::fromString(hotkey[0])[0], rApp->timeline);
            sc->setContext(Qt::ApplicationShortcut);
            sc->setAutoRepeat(false);

            rApp->dataEntryShortcuts.append(sc);

            connect(sc, &QShortcut::activated, [=]()
             {
                rApp->timeline->addPointMarkerWithEvent(eventType->id, hotkeyAuto, hotkeyPresets);
             });
        }
    }


    //TODO setter for entry type, set title there
    dialog->setWindowTitle(eventName);
    dialog->eventType = eventType;
    eventType->dialog = dialog;

    rApp->dataDialogs.insert(eventNumber, dialog);

    dialog->restoreGeometry(rApp->settings->value(QString("geometry_data_dialog_%1").arg(eventNumber)).toByteArray());

    //build the table view fot this type
    EventDataTable *table = new EventDataTable();
    table->eventType = eventType;
    rApp->tables.insert(eventNumber, table);

    //add to tabbed view
    rApp->dataTableTabs->addTab(eventName, table);


    //TODO move construction of widgets to dialog class?
    for(int controlNumber = 1;;controlNumber++)
    {

        if(tmplt->value(QString("event/%1/control/%2/status").arg(eventNumber).arg(controlNumber),"ACTIVE").toString() == "INACTIVE")
            continue;

        //get the name (label) for the control
        QString controlName = tmplt->value(QString("event/%1/control/%2/name").arg(eventNumber).arg(controlNumber)).toString();
        if(controlName.isEmpty())
            break;

        //load the control number to name lookup
        eventType->fieldNamesById.insert(controlNumber, controlName);


        //required?
        bool required = tmplt->value(QString("event/%1/control/%2/required").arg(eventNumber).arg(controlNumber), false).toBool();
        float order = tmplt->value(QString("event/%1/control/%2/order").arg(eventNumber).arg(controlNumber), (float)controlNumber).toFloat();
        //qDebug() << "order" << order;

        //sticky applies to all field tyes, but only numeric types honor autoincrement
        bool sticky = tmplt->value(QString("event/%1/control/%2/sticky").arg(eventNumber).arg(controlNumber), false).toBool();
        int autoIncrement = tmplt->value(QString("event/%1/control/%2/autoincrement").arg(eventNumber).arg(controlNumber), 0).toInt();

        QString description = tmplt->value(QString("event/%1/control/%2/description").arg(eventNumber).arg(controlNumber), "").toString();

        //bool exportValue = tmplt->value(QString("event/%1/control/%2/export_key").arg(eventNumber).arg(controlNumber), false).toBool();

        //depending on the type of control, different parameters will be set
        QString controlType = tmplt->value(QString("event/%1/control/%2/type").arg(eventNumber).arg(controlNumber)).toString();
        //TODO constants
        if(controlType == "SELECT")
        {
            //colon separated key value pairs
            QList<QString> valuesRaw = tmplt->value(QString("event/%1/control/%2/values").arg(eventNumber).arg(controlNumber)).toStringList();

            //colon separated key value pair
            QString defaultKey = tmplt->value(QString("event/%1/control/%2/default_value").arg(eventNumber).arg(controlNumber)).toString().split(":")[0];

            //unordered list
            QList<QPair<QString, QString>> keyValues;
            //lookup
            QMap<QString,QString> keyValueMap;
            foreach(QString v, valuesRaw)
            {
                keyValues.append(QPair<QString,QString>(v.split(":")[0], v.split(":")[1]));
                keyValueMap.insert(v.split(":")[0], v.split(":")[1]);
            }

            //track the enumerated types by id
            eventType->enumeratedValuesById.insert(controlNumber, keyValueMap);
            eventType->singleEnumeratedValueFields.append(controlNumber);

            //add the combobox to the dialog
            dialog->addSelectControl(controlNumber, controlName, description,  required, order, sticky, keyValues, defaultKey);
        }
        else if(controlType == "MULTI_SELECT")
        {
            //checkbox list
            //colon separated key value pairs
            QList<QString> valuesRaw = tmplt->value(QString("event/%1/control/%2/values").arg(eventNumber).arg(controlNumber)).toStringList();

            //colon separated key value pairs
            QList<QString> defaultValuesRaw = tmplt->value(QString("event/%1/control/%2/default_values").arg(eventNumber).arg(controlNumber)).toStringList();

            //unordered list
            QList<QPair<QString, QString>> keyValues;
            //lookup
            QMap<QString,QString> keyValueMap;
            foreach(QString v, valuesRaw)
            {
                keyValues.append(QPair<QString, QString>(v.split(":")[0], v.split(":")[1]));
                keyValueMap.insert(v.split(":")[0], v.split(":")[1]);
            }

            QList<QString> defaultKeys;
            foreach(QString v, defaultValuesRaw)
                defaultKeys.append(v.split(":")[0]);

            //track the enumerated types by id
            eventType->enumeratedValuesById.insert(controlNumber, keyValueMap);
            eventType->multiEnumeratedValueFields.append(controlNumber);

            dialog->addMultiSelectControl(controlNumber, controlName, description, required, order, sticky, keyValues, defaultKeys);
        }
        else if(controlType == "FLOAT")
        {
            double bottom = tmplt->value(QString("event/%1/control/%2/bottom").arg(eventNumber).arg(controlNumber)).toDouble();
            double top = tmplt->value(QString("event/%1/control/%2/top").arg(eventNumber).arg(controlNumber)).toDouble();
            int decimals = tmplt->value(QString("event/%1/control/%2/decimals").arg(eventNumber).arg(controlNumber)).toInt();
            QString defaultValue = tmplt->value(QString("event/%1/control/%2/default_value").arg(eventNumber).arg(controlNumber)).toString();

            dialog->addTextControl(controlNumber, controlName, description, required, order, sticky, -1, defaultValue, autoIncrement, new QDoubleValidator(bottom, top, decimals));
        }
        else if(controlType == "INTEGER")
        {
            double bottom = tmplt->value(QString("event/%1/control/%2/bottom").arg(eventNumber).arg(controlNumber)).toDouble();
            double top = tmplt->value(QString("event/%1/control/%2/top").arg(eventNumber).arg(controlNumber)).toDouble();
            QString defaultValue = tmplt->value(QString("event/%1/control/%2/default_value").arg(eventNumber).arg(controlNumber)).toString();

            dialog->addTextControl(controlNumber, controlName, description, required, order, sticky, -1, defaultValue, autoIncrement, new QIntValidator(bottom, top));
        }
        else if(controlType == "DATE_TIME")
        {
            dialog->addDateTimeControl(controlNumber, controlName, description, required, order, sticky);
            eventType->dateTimes.append(controlNumber);
        }
        else if(controlType == "TEXT")
        {
            QString defaultValue = tmplt->value(QString("event/%1/control/%2/default_value").arg(eventNumber).arg(controlNumber)).toString();
            int maxLength = tmplt->value(QString("event/%1/control/%2/max_length").arg(eventNumber).arg(controlNumber),-1).toInt();
            dialog->addTextControl(controlNumber, controlName, description, required, order, sticky, maxLength, defaultValue);
        }
        else if(controlType == "TEXTBOX")
        {
            QString defaultValue = tmplt->value(QString("event/%1/control/%2/default_value").arg(eventNumber).arg(controlNumber)).toString();
            dialog->addTextBoxControl(controlNumber, controlName, description, required, order, sticky, defaultValue);
        }
    }
    dialog->generateForm();
}








void EmDataLoad::loadPrimaryDataFile(int fileNumber, QSettings *tmplt, QList<QList<QString>> csv)
{
    //qDebug() << QColor::colorNames();

    //skip lines that have fewer values
    int minimumFieldCount =  tmplt->value(QString("file/%1/minimum_field_count").arg(fileNumber),-1).toInt();

    //winnow the file if requested
    int fileWinnowFactor = tmplt->value(QString("file/%1/winnow_factor").arg(fileNumber)).toInt();
    if(fileWinnowFactor > 1)
       for(int x = csv.size() -1; x >= 0; x--)
            if (x % fileWinnowFactor != 0)
                csv.removeAt(x);
    else //set factor to 1
       fileWinnowFactor = 1;

    //prepare to detect gaps if requested
    bool fileDetectGaps = tmplt->value(QString("file/%1/detect_gaps").arg(fileNumber)).toBool();
    int fileAllowableGapSeconds = tmplt->value(QString("file/%1/gap_seconds").arg(fileNumber)).toInt();

    //get the format string for the date time value
    QString dateTimeformat = tmplt->value(QString("file/%1/date_time_format_string").arg(fileNumber)).toString();

    //build list of fields to look for, if the field doesn't exist in the template, set the field index to -1

    //date & time (optionally specified in one or two columns
    int dateIdx = tmplt->value(QString("file/%1/date_field_index").arg(fileNumber),-1).toInt();
    int timeIdx = tmplt->value(QString("file/%1/time_field_index").arg(fileNumber),-1).toInt();
    int dateTimeIdx = tmplt->value(QString("file/%1/date_time_combined_field_index").arg(fileNumber),-1).toInt();

    //lat/lon
    int latIdx = tmplt->value(QString("file/%1/latitude_field_index").arg(fileNumber),-1).toInt();
    int lonIdx = tmplt->value(QString("file/%1/longitude_field_index").arg(fileNumber),-1).toInt();

    //map color coding - field at idx must be a number!!!
    int mapColorFieldIdx = tmplt->value(QString("file/%1/map_color_code/field_index").arg(fileNumber),-1).toInt();
    double mapColorThreshold = tmplt->value(QString("file/%1/map_color_code/threshold").arg(fileNumber),-1).toDouble();
    QString mapColor = tmplt->value(QString("file/%1/map_color_code/highlight_color").arg(fileNumber)).toString();
    QString mapTrackColor = tmplt->value(QString("file/%1/map_color_code/track_color").arg(fileNumber)).toString();
    int mapGapDistanceMeters = tmplt->value(QString("file/%1/map_color_code/gap_meters").arg(fileNumber)).toInt();

    //video filename fields
    QList<int> videoFileNameIdxs;
    for(int v = 1;;v++)
        if(tmplt->contains(QString("file/%1/video_channel/%2/filename_field_index").arg(fileNumber).arg(v)))
            videoFileNameIdxs.append(tmplt->value(QString("file/%1/video_channel/%2/filename_field_index").arg(fileNumber).arg(v)).toInt());
        else
            break;

    //determine the number of video channels either from the filename list or from template setting
    if(tmplt->contains(QString("video_channel_count")))
        rApp->emData->videoChannels = tmplt->value("video_channel_count").toInt();
    else
        rApp->emData->videoChannels = videoFileNameIdxs.length();

    //set the number of still image channels
    rApp->emData->stillImageChannels = tmplt->value("still_image_channel_count",0).toInt();

    //video offset
    int videoOffsetIdx = tmplt->value(QString("file/%1/video_offset_field_index").arg(fileNumber),-1).toInt();

    //all sensor plot types
    QList<EmDataSeries*> sensors;
    for(int z = 1;;z++)
        if(tmplt->contains(QString("file/%1/sensor_plot/%2/field_index").arg(fileNumber).arg(z)))
        {
            EmDataSeries* s = new EmDataSeries();
            s->fieldIndex = tmplt->value(QString("file/%1/sensor_plot/%2/field_index").arg(fileNumber).arg(z)).toInt();
            s->name = tmplt->value(QString("file/%1/sensor_plot/%2/name").arg(fileNumber).arg(z)).toString();
            s->color = QColor(tmplt->value(QString("file/%1/sensor_plot/%2/color").arg(fileNumber).arg(z)).toString());
            //default this to opaque
            int alpha = tmplt->value(QString("file/%1/sensor_plot/%2/alpha").arg(fileNumber).arg(z),255).toInt();
            s->color.setAlpha(alpha);
            s->zOrder = tmplt->value(QString("file/%1/sensor_plot/%2/zorder").arg(fileNumber).arg(z)).toInt();
            s->factor = tmplt->value(QString("file/%1/sensor_plot/%2/factor").arg(fileNumber).arg(z),1).toDouble();
            s->expression = tmplt->value(QString("file/%1/sensor_plot/%2/expression").arg(fileNumber).arg(z),"").toString();


            //remap strings to numbers
            QStringList rawMaps = tmplt->value(QString("file/%1/sensor_plot/%2/string_map").arg(fileNumber).arg(z)).toStringList();
            foreach(QString m, rawMaps)
            {
                QStringList mp = m.split(":");
                qDebug() << "adding string->double map" << mp;
                s->stringMap.insert(mp.at(0), mp.at(1).toDouble());
            }
            //TODO move line styles to constants
            QString lineType = tmplt->value(QString("file/%1/sensor_plot/%2/style").arg(fileNumber).arg(z)).toString();
            if(lineType == "LINE")
                s->type = EmDataSeries::LineType::Line;
            else if (lineType == "STEP_LINE")
                s->type = EmDataSeries::LineType::StepLine;
            else if (lineType == "SHADE")
                s->type = EmDataSeries::LineType::Shade;

            s->thickness = tmplt->value(QString("file/%1/sensor_plot/%2/width").arg(fileNumber).arg(z),1).toInt();

            bool existingSeries = false;
            foreach(EmDataSeries* es, rApp->emData->series)
                if(es->name == s->name)
                    existingSeries = true;

            if(!existingSeries)
                sensors.append(s);

        }
        else
            break;

    //used to eval "expression" sensor plot types
    QScriptEngine engine;

    //these two vectors are used by all file types
    QVector<double> seconds, gaps;

    int lineNum = 0;
    bool gapEnd = false;
    foreach(QList<QString> line, csv) {

        lineNum++;
        //qDebug() << lineNum;

        if(line.size() < minimumFieldCount)
        {
            qWarning() << "skipped malformed line " << lineNum << ": " << line;
            continue;
        }

        //concat date time if necesarry
        QString dateTime;
        if(dateTimeIdx >= 0)
            dateTime = line[dateTimeIdx];
        else
            dateTime = line[dateIdx] + line[timeIdx];

        //dateTime.chop(7);

        QDateTime time = QDateTime::fromString(dateTime, dateTimeformat);
        time.setTimeSpec(Qt::UTC);

        //qDebug() << time;

        //some bad sensor formats have two year dates
        if(time.date().year() < 2000)
            time = time.addYears(100);

        //TODO fix Y2.1K bug
        if(!time.isValid() || time.date().year() < 2000 || time.date().year() > 2100)
        {
            qWarning() << "skipped line with invalid date line number = " << lineNum << ": " << line;
            continue;
        }

        //convert back to seconds
        seconds.append((double)(time.toMSecsSinceEpoch()/1000.0));
        //qDebug() << "time" << seconds.last();

        //check for gaps once we are past the first line of the file
        if(fileDetectGaps && seconds.size() > 1 && seconds.at(seconds.size()-1) - seconds.at(seconds.size()-2) > fileAllowableGapSeconds * fileWinnowFactor)
        {
            //TODO constants
            gaps.append(1);
            gapEnd = true;
            //qDebug() << "gap time" << seconds.last();
            foreach(EmDataSeries* s, sensors)
            {

                s->seconds.append(seconds.last());
                if(s->type == EmDataSeries::LineType::Shade)
                    s->values.append(0); //shades must have a value
                else
                    s->values.append(NAN);


            }
        }
        else
        {
            //no gap
            if(gapEnd)
            {
                gaps.append(1);
                gapEnd = false;
            }
            else
                gaps.append(NAN);

            foreach(EmDataSeries* s, sensors)
            {
                s->seconds.append(seconds.last());

                double val = line[s->fieldIndex].toDouble();

                if(s->stringMap.count() > 0)
                    val = s->stringMap.value(line[s->fieldIndex]);

                if(s->expression != "")
                {
                    qDebug() << s->expression << val;
                    QString expression = QString(s->expression);
                    expression.replace("{x}",QString::number(val, 'f', 10));
                    val = engine.evaluate(expression).toNumber();
                    qDebug() << expression << val;
                }

                if(s->factor != 1)
                    val = val * s->factor;

                s->values.append(val);
            }
        }



        if(latIdx != -1 && lonIdx != -1)
        {
            double lat = line.at(latIdx).toDouble();
            double lon = line.at(lonIdx).toDouble();

            //qDebug() << lat << "/" << lon;

            //if lat and lon are not zero, add a map segment
            if(lat != 0 && lon != 0)
            {
                //load map segment colors (gaps will be addressed below)
                rApp->emData->mapSegmentColorByStart.insert(seconds.last(), line[mapColorFieldIdx].toDouble() < mapColorThreshold ? mapTrackColor : mapColor);
                //load the list used for map sync from timeline
                rApp->emData->mapSegmentsByStart.insert(seconds.last(), new QGeoCoordinate(lat, lon));
                //load the spatial index for timeline sync from map
                rApp->emData->spatialIndex.insert(std::pair<Constants::Coordinates, long>(Constants::Coordinates(lat, lon), seconds.last()));

            }
        }

        //make video link objects
        foreach(int x, videoFileNameIdxs)
        {
           if(line.size() - 1 < x)
                break;

           QString fn = line.at(x);

           //add a record if we havn't seen this file name
           if(!rApp->emData->videosByName.contains(fn))
           {
               EmDataVideoDuration* vd = new EmDataVideoDuration;
               vd->fileName = fn;
               vd->startSeconds = seconds.last() - line[videoOffsetIdx].toInt(); //convert offset into seconds since epoch
               qDebug() << "video duration start: " << vd->startSeconds << " last seconds: " << seconds.last() << " offset: " << line[videoOffsetIdx].toInt();
               vd->videoChannel = (x - videoFileNameIdxs[0]); //offset from start of line
               rApp->emData->videosByName.insert(vd->fileName, vd);
               rApp->emData->videosByChannel.insert(vd->videoChannel, vd);
               rApp->emData->videosByStart.insert(vd->startSeconds, vd);
           } else
               rApp->emData->videosByName[fn]->endSeconds = seconds.last();

        }



    } //csv read complete

    //add all of the series
    foreach(EmDataSeries* s, sensors)
        rApp->emData->series.append(s);


    qDebug() << "em data objects created";

    //create the qvariant list used by the QML map, test the distance from the last point, mark gaps
    QGeoCoordinate *lastCoord = NULL;
    foreach(long sec, rApp->emData->mapSegmentsByStart.keys())
    {
        QGeoCoordinate *coord = rApp->emData->mapSegmentsByStart.value(sec);
        QString segmentColor = rApp->emData->mapSegmentColorByStart.value(sec);
        QVariantMap vesselSegment;
        //short property names here make a big difference in performance
        vesselSegment["y"] = coord->latitude();
        vesselSegment["x"] = coord->longitude();
        vesselSegment["c"] = segmentColor;

        if(mapGapDistanceMeters > 0 && lastCoord != NULL && coord->distanceTo(*lastCoord) > mapGapDistanceMeters)
            vesselSegment["c"] = "red";

        //load the variants to be passed to qml map
        rApp->emData->mapSegmentVariants.append(vesselSegment);

        lastCoord = coord;

    }
     qDebug() << "map objects created";


    //add gaps if we are looking and found some
    if(fileDetectGaps) //&& *std::max_element(gaps.constBegin(), gaps.constEnd()) > 0
    {
        EmDataSeries* gapSeries = new EmDataSeries();
        //TODO constants
        gapSeries->name = "Gaps";
        gapSeries->color = QColor("red");
        gapSeries->seconds = seconds;
        gapSeries->values = gaps;
        gapSeries->type = EmDataSeries::LineType::Shade;
        gapSeries->zOrder = 0;
        gapSeries->thickness = 1;
        rApp->emData->series.append(gapSeries);
    }

}






void EmDataLoad::loadSecondaryMultiSensorLogFile(int fileNumber, QSettings *tmplt, QList<QList<QString>> csv)
{


    //skip lines that have fewer values
    int minimumFieldCount =  tmplt->value(QString("file/%1/minimum_field_count").arg(fileNumber),-1).toInt();

    //get the format string for the date time value
    QString dateTimeformat = tmplt->value(QString("file/%1/date_time_format_string").arg(fileNumber)).toString();

    //build list of fields to look for, if the field doesn't exist in the template, set the field index to -1

    //date & time (optionally specified in one or two columns
    int dateIdx = tmplt->value(QString("file/%1/date_field_index").arg(fileNumber),-1).toInt();
    int timeIdx = tmplt->value(QString("file/%1/time_field_index").arg(fileNumber),-1).toInt();
    int dateTimeIdx = tmplt->value(QString("file/%1/date_time_combined_field_index").arg(fileNumber),-1).toInt();

    //all sensor plot types
    QList<EmDataSeries*> sensors;
    for(int z = 1;;z++)
        if(tmplt->contains(QString("file/%1/sensor_plot/%2/field_index").arg(fileNumber).arg(z)))
        {
            EmDataSeries* s = new EmDataSeries();
            s->fieldIndex = tmplt->value(QString("file/%1/sensor_plot/%2/field_index").arg(fileNumber).arg(z)).toInt();
            s->name = tmplt->value(QString("file/%1/sensor_plot/%2/name").arg(fileNumber).arg(z)).toString();
            s->color = QColor(tmplt->value(QString("file/%1/sensor_plot/%2/color").arg(fileNumber).arg(z)).toString());
            //default this to opaque
            int alpha = tmplt->value(QString("file/%1/sensor_plot/%2/alpha").arg(fileNumber).arg(z),255).toInt();
            s->color.setAlpha(alpha);
            s->zOrder = tmplt->value(QString("file/%1/sensor_plot/%2/zorder").arg(fileNumber).arg(z)).toInt();
            s->factor = tmplt->value(QString("file/%1/sensor_plot/%2/factor").arg(fileNumber).arg(z),1).toDouble();
            s->expression = tmplt->value(QString("file/%1/sensor_plot/%2/expression").arg(fileNumber).arg(z),"").toString();


            //remap strings to numbers
            QStringList rawMaps = tmplt->value(QString("file/%1/sensor_plot/%2/string_map").arg(fileNumber).arg(z)).toStringList();
            foreach(QString m, rawMaps)
            {
                QStringList mp = m.split(":");
                qDebug() << "adding string->double map" << mp;
                s->stringMap.insert(mp.at(0), mp.at(1).toDouble());
            }
            //TODO move line styles to constants
            QString lineType = tmplt->value(QString("file/%1/sensor_plot/%2/style").arg(fileNumber).arg(z)).toString();
            if(lineType == "LINE")
                s->type = EmDataSeries::LineType::Line;
            else if (lineType == "STEP_LINE")
                s->type = EmDataSeries::LineType::StepLine;
            else if (lineType == "SHADE")
                s->type = EmDataSeries::LineType::Shade;

            s->thickness = tmplt->value(QString("file/%1/sensor_plot/%2/width").arg(fileNumber).arg(z),1).toInt();



            bool existingSeries = false;
            foreach(EmDataSeries* es, rApp->emData->series)
                if(es->name == s->name)
                    existingSeries = true;

            if(!existingSeries)
                sensors.append(s);
        }
        else
            break;

    //used to eval "expression" sensor plot types
    QScriptEngine engine;

    //these two vectors are used by all file types
    QVector<double> seconds;

    int lineNum = 0;
    foreach(QList<QString> line, csv) {

        lineNum++;
        //qDebug() << lineNum;

        if(line.size() < minimumFieldCount)
        {
            qWarning() << "skipped malformed line " << lineNum << ": " << line;
            continue;
        }

        //concat date time if necesarry
        QString dateTime;
        if(dateTimeIdx >= 0)
            dateTime = line[dateTimeIdx];
        else
            dateTime = line[dateIdx] + line[timeIdx];

        //qDebug() << "loading date time" << dateTime;

        QDateTime time = QDateTime::fromString(dateTime, dateTimeformat);
        time.setTimeSpec(Qt::UTC);

         //qDebug() << "into" << time.toString();

        //some bad sensor formats have two year dates
        if(time.date().year() < 2000)
            time = time.addYears(100);

        //TODO fix Y2.1K bug
        if(!time.isValid() || time.date().year() < 2000 || time.date().year() > 2100)
        {
            qWarning() << "skipped line with invalid date line number = " << lineNum << ": " << line;
            continue;
        }

        //convert back to seconds
        seconds.append((double)time.toTime_t());



        foreach(EmDataSeries* s, sensors)
        {
            s->seconds.append(seconds.last());

            double val = line[s->fieldIndex].toDouble();

            if(s->stringMap.count() > 0)
                val = s->stringMap.value(line[s->fieldIndex]);

            if(s->expression != "")
            {
                qDebug() << s->expression << val;
                QString expression = QString(s->expression);
                expression.replace("{x}",QString::number(val, 'f', 10));
                val = engine.evaluate(expression).toNumber();
                qDebug() << expression << val;
            }

            if(s->factor != 1)
                val = val * s->factor;

            s->values.append(val);
        }

    } //csv read complete

    //add all of the series
    foreach(EmDataSeries* s, sensors)
        rApp->emData->series.append(s);


}








//load sensor event data keyed by seconds
void EmDataLoad::loadSensorDurationEventFile(int fileNumber, QSettings *tmplt, QList<QList<QString>> csv)
{

    //skip lines that have fewer values
    int minimumFieldCount =  tmplt->value(QString("file/%1/minimum_field_count").arg(fileNumber),-1).toInt();

    //get the index for the start and end of each duration
    int eventBeginSecondsIdx = tmplt->value(QString("file/%1/duration_begin_seconds_field_index").arg(fileNumber)).toInt();
    int eventEndSecondsIdx = tmplt->value(QString("file/%1/duration_end_seconds_field_index").arg(fileNumber)).toInt();
    int eventValueIdx = tmplt->value(QString("file/%1/sensor_event_value_field_index").arg(fileNumber)).toInt();

    QString eventName = tmplt->value(QString("file/%1/sensor_event_name").arg(fileNumber)).toString();
    QColor eventColor = QColor(tmplt->value(QString("file/%1/sensor_event_color").arg(fileNumber)).toString());
    EmDataSensorDurationEvent::DurationMarkerStyle style;
    if (tmplt->value(QString("file/%1/sensor_event_style").arg(fileNumber)).toString() == "SHADE")
        style = EmDataSensorDurationEvent::DurationMarkerStyle::Shade;
    else if (tmplt->value(QString("file/%1/sensor_event_style").arg(fileNumber)).toString() == "LINE")
        style = EmDataSensorDurationEvent::DurationMarkerStyle::Line;

    foreach(QList<QString> line, csv) {

        if(line.size() < minimumFieldCount){
            qDebug() << "skipped malformed line: " << line;
            continue;
        }

        EmDataSensorDurationEvent* event = new EmDataSensorDurationEvent();
        event->name = eventName;
        event->color = eventColor;
        event->durationMarkerStyle = style;
        event->beginSeconds = line.at(eventBeginSecondsIdx).toDouble();
        event->endSeconds = line.at(eventEndSecondsIdx).toDouble();
        event->value = line.at(eventValueIdx).toDouble();
        //qDebug() << "sensor value " << event->value;

        for(int z = 1;;z++)
            if(tmplt->contains(QString("file/%1/sensor_event_data_element/%2/name").arg(fileNumber).arg(z)))
            {
               int elementIdx = tmplt->value(QString("file/%1/sensor_event_data_element/%2/field_index").arg(fileNumber).arg(z)).toInt();
               if(tmplt->value(QString("file/%1/sensor_event_data_element/%2/type").arg(fileNumber).arg(z)) == "DISPLAY")
                    event->displayData.insert(tmplt->value(QString("file/%1/sensor_event_data_element/%2/name").arg(fileNumber).arg(z)).toString(),line.at(elementIdx));
               if(tmplt->value(QString("file/%1/sensor_event_data_element/%2/type").arg(fileNumber).arg(z)) == "TIMELINE_LINK")
                   event->relatedEventOffsets.insert(tmplt->value(QString("file/%1/sensor_event_data_element/%2/name").arg(fileNumber).arg(z)).toString(),line.at(elementIdx).toDouble());
            }
            else
                break;

        rApp->emData->sensorEvents.append(event);
    }
}


//load event log, keyed by seconds or by date and time
void EmDataLoad::loadEventLogFile(int fileNumber, QSettings *tmplt, QList<QList<QString>> csv)
{

    //get the format string for the date time value
    QString dateTimeformat = tmplt->value(QString("file/%1/date_time_format_string").arg(fileNumber)).toString();

    //date & time
    int dateTimeIdx = tmplt->value(QString("file/%1/date_time_field_index").arg(fileNumber),-1).toInt();
    int evntIdx = tmplt->value(QString("file/%1/event_field_index").arg(fileNumber),-1).toInt();

    //event properties
    //QColor defaultEvntColor = QColor(tmplt->value(QString("file/%1/event_default_color").arg(fileNumber),"black").toString());
    //QString defaultEvntSymbol = tmplt->value(QString("file/%1/event_default_symbol").arg(fileNumber),"DIMOND").toString();
    float pos = tmplt->value(QString("file/%1/event_position").arg(fileNumber),.1).toFloat();
    QStringList rawMap = tmplt->value(QString("file/%1/event_map").arg(fileNumber)).toStringList();
    QMap<QString, QColor> eventColors;
    QMap<QString, EmDataEnumEvent::Symbol> eventSymbols;
    QMap<QString, float> eventSize;
    foreach(QString m, rawMap)
    {
        QStringList mp = m.split(":");
        QString k = mp.at(0);
        QString s = mp.at(1);
        QString sz = mp.at(2);
        QString c = mp.at(3);
        EmDataEnumEvent::Symbol sym;
        if(s == "DIMOND")
            sym = EmDataEnumEvent::Symbol::Dimond;
        else if (s == "SQUARE")
            sym = EmDataEnumEvent::Symbol::Square;
        else if (s == "TRIANGLE")
            sym = EmDataEnumEvent::Symbol::Triangle;
        else if (s == "TRIANGLE_INVERTED")
            sym = EmDataEnumEvent::Symbol::TriangleInverted;
        else if (s == "CIRCLE")
            sym = EmDataEnumEvent::Symbol::Circle;
        else if (s == "DISC")
            sym = EmDataEnumEvent::Symbol::Disc;
        else
            sym = EmDataEnumEvent::Symbol::Dimond;

        eventSymbols.insert(k,sym);
        eventColors.insert(k,QColor(c));
        eventSize.insert(k,sz.toFloat());
    }

    foreach(QList<QString> line, csv) {

        QDateTime time = QDateTime::fromString(line[dateTimeIdx], dateTimeformat);
        time.setTimeSpec(Qt::UTC);
        QString evtText = line[evntIdx];
        if(eventColors.contains(evtText))
        {
            EmDataEnumEvent* evt = new EmDataEnumEvent();
            evt->seconds = (double)(time.toMSecsSinceEpoch()/1000.0);
            evt->text = evtText;
            evt->color = eventColors.value(evtText);
            evt->symbol = eventSymbols.value(evtText);
            evt->size = eventSize.value(evtText);
            evt->position = pos;
            rApp->emData->enumEvents.append(evt);
        }


    }
}



//load event log, keyed by epoch seconds
void EmDataLoad::loadSystemEventLogFile(int fileNumber, QSettings *tmplt, QList<QList<QString>> csv)
{

    QString eventName = tmplt->value(QString("file/%1/event_name").arg(fileNumber)).toString();
    QColor eventColor = QColor(tmplt->value(QString("file/%1/event_color").arg(fileNumber)).toString());
    float eventHeight = tmplt->value(QString("file/%1/event_height").arg(fileNumber),5).toFloat();
    int secondsIdx = tmplt->value(QString("file/%1/event_seconds_index").arg(fileNumber),-1).toInt();
    int descIdx = tmplt->value(QString("file/%1/event_desc_index").arg(fileNumber),-1).toInt();

    foreach(QList<QString> line, csv) {
        double startSeconds = line.at(secondsIdx).toDouble();
        EmDataSensorDurationEvent* event = new EmDataSensorDurationEvent();
        event->name = eventName;
        event->color = eventColor;
        event->durationMarkerStyle = EmDataSensorDurationEvent::DurationMarkerStyle::Line;
        event->beginSeconds = startSeconds;
        event->endSeconds = startSeconds;
        event->value = eventHeight;
        event->displayData.insert("Description",line.at(descIdx));
        rApp->emData->sensorEvents.append(event);
    }
}

void EmDataLoad::loadPsmfcEventLogFile(int fileNumber, QSettings *tmplt, QList<QList<QString>> csv)
{

    //get the format string for the date time value
    QString dateTimeformat = tmplt->value(QString("file/%1/date_time_format_string").arg(fileNumber)).toString();

    //date & time
    int dateIdx = tmplt->value(QString("file/%1/date_field_index").arg(fileNumber),-1).toInt();
    int timeIdx = tmplt->value(QString("file/%1/time_field_index").arg(fileNumber),-1).toInt();
    int evntIdx = tmplt->value(QString("file/%1/event_field_index").arg(fileNumber),-1).toInt();
    int evntDescIdx = tmplt->value(QString("file/%1/event_description_field_index").arg(fileNumber),-1).toInt();

    //event properties
    //QColor defaultEvntColor = QColor(tmplt->value(QString("file/%1/event_default_color").arg(fileNumber),"black").toString());
    //QString defaultEvntSymbol = tmplt->value(QString("file/%1/event_default_symbol").arg(fileNumber),"DIMOND").toString();
    float pos = tmplt->value(QString("file/%1/event_position").arg(fileNumber),.1).toFloat();
    QStringList rawMap = tmplt->value(QString("file/%1/event_map").arg(fileNumber)).toStringList();
    QMap<QString, QColor> eventColors;
    QMap<QString, EmDataEnumEvent::Symbol> eventSymbols;
    QMap<QString, float> eventSize;
    foreach(QString m, rawMap)
    {
        QStringList mp = m.split(":");
        QString k = mp.at(0);
        QString s = mp.at(1);
        QString sz = mp.at(2);
        QString c = mp.at(3);
        EmDataEnumEvent::Symbol sym;
        if(s == "DIMOND")
            sym = EmDataEnumEvent::Symbol::Dimond;
        else if (s == "SQUARE")
            sym = EmDataEnumEvent::Symbol::Square;
        else if (s == "TRIANGLE")
            sym = EmDataEnumEvent::Symbol::Triangle;
        else if (s == "TRIANGLE_INVERTED")
            sym = EmDataEnumEvent::Symbol::TriangleInverted;
        else if (s == "CIRCLE")
            sym = EmDataEnumEvent::Symbol::Circle;
        else if (s == "DISC")
            sym = EmDataEnumEvent::Symbol::Disc;
        else
            sym = EmDataEnumEvent::Symbol::Dimond;

        eventSymbols.insert(k,sym);
        eventColors.insert(k,QColor(c));
        eventSize.insert(k,sz.toFloat());
    }

    foreach(QList<QString> line, csv) {

        QString dateTime  = line[dateIdx] + line[timeIdx];

        QDateTime time = QDateTime::fromString(dateTime, dateTimeformat);
        time.setTimeSpec(Qt::UTC);
        time = time.addYears(100);
        QString evtCode = line[evntIdx];
        QString evtText = line[evntDescIdx];
        if(eventColors.contains(evtCode))
        {
            EmDataEnumEvent* evt = new EmDataEnumEvent();
            evt->seconds = (double)(time.toMSecsSinceEpoch()/1000.0);
            evt->text = evtText;
            evt->color = eventColors.value(evtCode);
            evt->symbol = eventSymbols.value(evtCode);
            evt->size = eventSize.value(evtCode);
            evt->position = pos;
            rApp->emData->enumEvents.append(evt);
        }


    }
}



//load event log, keyed by seconds or by date and time
void EmDataLoad::loadSecondarySensorLogFile(int fileNumber, QSettings *tmplt, QList<QList<QString>> csv)
{
    EmDataSeries *s = new EmDataSeries();
    s->name = tmplt->value(QString("file/%1/sensor_name").arg(fileNumber)).toString();
    s->color = QColor(tmplt->value(QString("file/%1/sensor_color").arg(fileNumber)).toString());
    //default this to opaque
    int alpha = tmplt->value(QString("file/%1/sensor_alpha").arg(fileNumber),255).toInt();
    s->color.setAlpha(alpha);
    s->factor = tmplt->value(QString("file/%1/sensor_factor").arg(fileNumber)).toDouble();
    s->zOrder = tmplt->value(QString("file/%1/sensor_zorder").arg(fileNumber)).toInt();
    QString lineType = tmplt->value(QString("file/%1/sensor_style").arg(fileNumber)).toString();

    if(lineType == "LINE")
        s->type = EmDataSeries::LineType::Line;
    else if (lineType == "STEP_LINE")
        s->type = EmDataSeries::LineType::StepLine;
    else if (lineType == "SHADE")
        s->type = EmDataSeries::LineType::Shade;

    foreach(QList<QString> line, csv) {
        s->seconds.append(line.at(0).toDouble());
        s->values.append(line.at(1).toDouble() * s->factor);
    }

    rApp->emData->series.append(s);


}







/* this code uses VLC to determine durration, but needs a timeout or other way to detect failed media open
VlcInstance *instance = new VlcInstance(VlcCommon::args(), this);
VlcMediaPlayer *player = new VlcMediaPlayer(instance);
VlcMedia *media;
foreach(VideoDurration vd, videos)
{
    media = new VlcMedia(workingDir.absolutePath() + "/" + vd.filename, true, instance);
    player->open(media);
    //if the media can be opened, wait until the length is poplated
    while(player->length() <= 0)
            QCoreApplication::processEvents();
    qDebug() << vd.filename << " " << player->length();
    vd.end = vd.start + player->length()/1000.0;

}
delete player;
delete instance;
*/
