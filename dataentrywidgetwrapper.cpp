#include "dataentrywidgetwrapper.h"

#include <QComboBox>
#include <QCloseEvent>
#include <QDebug>
#include <QLineEdit>
#include <QListWidget>
#include <QListWidgetItem>
#include <QDateTimeEdit>


DataEntryWidgetWrapper::DataEntryWidgetWrapper(QObject *parent) : QObject(parent)
{

}

void DataEntryWidgetWrapper::setDefaults()
{
    widget->setStyleSheet("");

    if(sticky) //don't set defaults for sticky controls
        return;

    //TODO constants
    if(widgetType == WidgetType::Text)
        qobject_cast<QLineEdit*>(widget)->setText(defaultValue);
    else if(widgetType == WidgetType::TextBox)
        qobject_cast<QTextEdit*>(widget)->setText(defaultValue);
    else if(widgetType == WidgetType::Select)
        if(defaultIds.size() > 0)
            qobject_cast<QComboBox*>(widget)->setCurrentIndex(qobject_cast<QComboBox*>(widget)->findData(defaultIds[0]));
        else
            qobject_cast<QComboBox*>(widget)->setCurrentIndex(-1);
    else if(widgetType == WidgetType::MultiSelect)
    {
        //qDebug() << "defaults:" << defaultIds;
        //TODO role constant
        foreach(QListWidgetItem* item, qobject_cast<QListWidget*>(widget)->findItems("*", Qt::MatchWildcard))
        {
            //qDebug() << "setting multislect check state for:" << item->data(Qt::UserRole).toString();
            if(defaultIds.contains(item->data(Qt::UserRole).toString()))
            {
                //qDebug() << "true";
                item->setCheckState(Qt::Checked);
            }
            else
            {
                 //qDebug() << "false";
                 item->setCheckState(Qt::Unchecked);
            }
        }
    }
    else if(widgetType == WidgetType::DateTime)
        qobject_cast<QDateTimeEdit*>(widget)->setDateTime(QDateTime::currentDateTimeUtc());
}



 bool DataEntryWidgetWrapper::compare(DataEntryWidgetWrapper *w1, DataEntryWidgetWrapper *w2)
 {
     //qDebug() << w1->order << w2->order;
     return w1->order < w2->order;

 }


void DataEntryWidgetWrapper::setAutoIncrement()
{
    if(autoIncrement == 0 || widgetType != WidgetType::Text)
        return;

    QLineEdit* w = qobject_cast<QLineEdit*>(widget);
    double val = w->text().toDouble() + autoIncrement;
    w->setText(QString::number(val));
}



void DataEntryWidgetWrapper::setControlValues(QVariant val)
{
    if(widgetType == WidgetType::Text)
        qobject_cast<QLineEdit*>(widget)->setText(val.toString());
    else if(widgetType == WidgetType::TextBox)
        qobject_cast<QTextEdit*>(widget)->setText(val.toString());
    else if(widgetType == WidgetType::Select)
         qobject_cast<QComboBox*>(widget)->setCurrentIndex(qobject_cast<QComboBox*>(widget)->findData(val));
    else if(widgetType == WidgetType::MultiSelect)
    {
        QStringList idList = val.toStringList();
        foreach(QListWidgetItem* item, qobject_cast<QListWidget*>(widget)->findItems("*", Qt::MatchWildcard))
            if(idList.contains(item->data(Qt::UserRole).toString()))
                item->setCheckState(Qt::Checked);
            else
                 item->setCheckState(Qt::Unchecked);
    }
    else if(widgetType == WidgetType::DateTime)
        qobject_cast<QDateTimeEdit*>(widget)->setDateTime(QDateTime::fromMSecsSinceEpoch(val.toDouble()*1000));
}

void DataEntryWidgetWrapper::fixup()
{
    //attempt to to clean up the value in the text field according to the validator
    if(widgetType == WidgetType::Text &&  qobject_cast<QLineEdit*>(widget)->validator())
    {
        QString t = qobject_cast<QLineEdit*>(widget)->text();
        qobject_cast<QLineEdit*>(widget)->validator()->fixup(t);
        qobject_cast<QLineEdit*>(widget)->setText(t);
    }
}

//check for missing required controls
bool DataEntryWidgetWrapper::validate()
{
    //clear any errors
    widget->setStyleSheet("");

    if(widgetType == WidgetType::Text && qobject_cast<QLineEdit*>(widget)->text().isEmpty())
    {
        widget->setStyleSheet("border: 1px solid red");
        return false;
    }
    else if(widgetType == WidgetType::Select && qobject_cast<QComboBox*>(widget)->currentIndex() < 0)
    {
        widget->setStyleSheet("border: 1px solid red");
        return false;
    }
    else if(widgetType == WidgetType::MultiSelect)
    {
        //TODO rework this
        bool checked = false;
        foreach(QListWidgetItem* item, qobject_cast<QListWidget*>(widget)->findItems("*", Qt::MatchWildcard))
            if(item->checkState() == Qt::Checked)
                checked = true;

        if(!checked)
        {
            widget->setStyleSheet("border: 1px solid red");
            return false;
        }
    }

    return true;
}


void DataEntryWidgetWrapper::getControlValues(DataEntryEventData *eventData)
{
    //add values to qvariant map keyed on control id
    if(widgetType == WidgetType::Text)
        eventData->data.insert(id, qobject_cast<QLineEdit*>(widget)->text());
    if(widgetType == WidgetType::TextBox)
        eventData->data.insert(id, qobject_cast<QTextEdit*>(widget)->document()->toPlainText());
    else if(widgetType == WidgetType::Select)
    {
        if(qobject_cast<QComboBox*>(widget)->currentIndex() == -1)
            eventData->data.insert(id,  QString());
        else
            eventData->data.insert(id, qobject_cast<QComboBox*>(widget)->currentData());
    }
    else if(widgetType == WidgetType::MultiSelect)
    {
        QStringList idList;
        //TODO role constant
        foreach(QListWidgetItem* item, qobject_cast<QListWidget*>(widget)->findItems("*", Qt::MatchWildcard))
            if(item->checkState() == Qt::Checked)
                idList.append(item->data(Qt::UserRole).toString());

        eventData->data.insert(id, idList);
    }
    else if(widgetType == WidgetType::DateTime)
        eventData->data.insert(id, qobject_cast<QDateTimeEdit*>(widget)->dateTime().toMSecsSinceEpoch()/1000);
}
