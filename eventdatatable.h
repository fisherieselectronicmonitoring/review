#ifndef EVENTDATATABLE_H
#define EVENTDATATABLE_H

#include <QWidget>
#include "dataentryeventtype.h"
#include "dataentryeventdata.h"

namespace Ui {
class EventDataTable;
}

class EventDataTable : public QWidget
{
    Q_OBJECT

public:
    explicit EventDataTable(QWidget *parent = 0);
    ~EventDataTable();
    DataEntryEventType *eventType;

private:
    Ui::EventDataTable *ui;
    bool headersPrinted;
    void printHeaders(QList<QString>);

public slots:
    void load();
    void addRow(DataEntryEventData *);
    void updateRow(DataEntryEventData *);
    void selectRow(DataEntryEventData *);
    void deleteRow(DataEntryEventData *);

private slots:
    void tableDblClick(int, int);


protected:
virtual void keyPressEvent(QKeyEvent *);

};
#endif // EVENTDATATABLE_H









