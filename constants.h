#ifndef REVIEWCONSTANTS_H
#define REVIEWCONSTANTS_H

#ifndef Q_MOC_RUN  // See: https://bugreports.qt-project.org/browse/QTBUG-22829
    #include <boost/geometry/geometries/point.hpp>
    #include <boost/geometry/core/cs.hpp>
#endif

namespace Constants {

    static const int VIDEO_WINDOW_OFFSETS = 200;

    static const char* CONFIG_FILE = "Review.ini";

    typedef boost::geometry::model::point<float, 2, boost::geometry::cs::cartesian> Coordinates;

    enum class Coordinate {
        Latitude,
        Longitude
    };

    enum class ExportFormat {
        CSV,
        JSON
    };

    static int DECIMAL_MINUTES_PRECISION = 3;

    static char FLOATING_POINT_FORMAT = 'f';

    static const char* DATE_TIME_FORMAT = "MM/dd/yyyy hh:mm:ss Z";
    static const char* FILE_DATE_TIME_FORMAT = "yyyy-MM-dd_hh-mm-ss_Z";

    static const double MINIMUM_DURATION = 1;








}
#endif // REVIEWCONSTANTS_H

