import QtQuick 2.5
import QtLocation 5.5
import QtPositioning 5.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2



ApplicationWindow {
    id:mapWindow
    signal coordinateClick(double lat, double lon)
    height: 600
    width: 800
    visible: true
    title: "Chart"
    //flags: Qt.Window | Qt.WindowTitleHint | Qt.WindowMinimizeButtonHint | Qt.WindowMaximizeButtonHint | Qt.WindowCloseButtonHint
    property var track: null

    statusBar: StatusBar {
            RowLayout {
                anchors.right: parent.right
                anchors.left: parent.left
                Label {
                    id: distanceText
                    text: ""
                    anchors.left: parent.left
                }
                Label {
                    id: statusText
                    text: ""
                    anchors.right: parent.right
                }
            }
    }

    //don't allow this to close
    onClosing: {
        close.accepted = false;
    }


    Map {
        id: map
        property variant mousePosition
        property variant previousZoom
        property variant lastRightClick
        width: parent.width
        height: parent.height


        //default to Mount Suribachi
        center: QtPositioning.coordinate(24.750278, 141.288889)

        minimumZoomLevel: 2
        maximumZoomLevel: 18

        onWidthChanged:
        {
            //console.log("width changed");
            if(map.contains(track)){
                map.removeMapItem(track);
                map.addMapItem(track);
            }
        }

        onHeightChanged:
        {
            //console.log("height changed");
            if(map.contains(track)){
                map.removeMapItem(track);
                map.addMapItem(track);
            }

        }

        onZoomLevelChanged:
        {
            if(mousePosition)
            {
                var factor = Math.abs(zoomLevel - previousZoom);
                if(factor > 0 && !lockCursor.checked)
                    map.pan((mousePosition.x - width/2) * factor, (mousePosition.y - height/2) * factor);
            }
            previousZoom = zoomLevel;
        }

        Component.onCompleted: {
            //console.log(mapConfig.getHost());
            var plugin = Qt.createQmlObject ('import QtLocation 5.5; Plugin{ name: "flosm"; '+
                                             'PluginParameter { name: "flosm.mapping.image.format"; value: "'+mapConfig.getImageFormat()+'"} ' +
                                             'PluginParameter { name: "flosm.mapping.host"; value: "'+mapConfig.getHost()+'"} }', mapWindow)



            map.plugin = plugin;

            //set custom map type
            for(var s = 0; s < map.supportedMapTypes.length; s++){
                if(map.supportedMapTypes[s].style === MapType.CustomMap){
                   map.activeMapType = map.supportedMapTypes[s];

                }
            }
        }
        Button {
            id: findTrack
            /*
            style: ButtonStyle {
                label: Text {
                    color: "black"
                    text: "Zoom to Track"
                }
            }
            */
            text: "Fit Track"
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: 10

            z: 100
            onClicked: {
                map.fitViewportToMapItems();
            }

        }

        CheckBox {
            id: lockCursor

            style: CheckBoxStyle {
                        label: Text {
                            color: "white"
                            text: "Center Current Position"
                        }
                    }

            anchors.top: findTrack.bottom
            anchors.left: parent.left
            anchors.margins: 10
            checked: false
            z: 100
            onCheckedChanged: {
                findTrack.enabled = !lockCursor.checked;
                if(lockCursor.checked) {
                    map.gesture.acceptedGestures = MapGestureArea.PinchGesture;
                } else {
                    map.gesture.acceptedGestures = MapGestureArea.PanGesture | MapGestureArea.FlickGesture | MapGestureArea.PinchGesture;
                }
            }
        }

        MapPolyline {
               id:distance
               line.width: 1
               line.color: 'green'
               path: []
               visible: false


           }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            hoverEnabled: true

            onPositionChanged: {
                  map.mousePosition = Qt.point(mouse.x, mouse.y)
            }

            onClicked: {
                 if (mouse.button === Qt.RightButton) {
                     var currentRightClick = map.toCoordinate(Qt.point(mouse.x, mouse.y));
                     if(map.lastRightClick){
                         distanceText.text = roundNumber(currentRightClick.distanceTo(map.lastRightClick)/1000,3) + " km";
                         //console.log(roundNumber(currentRightClick.distanceTo(map.lastRightClick)/1000,3) + " km");
                         distance.path = [currentRightClick,map.lastRightClick];
                         distance.visible = true;
                         map.addMapItem(distance);
                     }
                     map.lastRightClick = currentRightClick;
                 } else {
                     distance.visible = false;
                     distanceText.text = "";
                     map.lastRightClick = null;

                 }
            }


            onDoubleClicked:{
                if (mouse.button === Qt.LeftButton) {
                    var cl = map.toCoordinate(Qt.point(mouse.x, mouse.y))
                    console.log(cl)
                    mapWindow.coordinateClick(cl.latitude, cl.longitude)

               }

            }
            /* zoom in and out
            onDoubleClicked: {
                map.center = map.toCoordinate(Qt.point(mouse.x, mouse.y))
                if (mouse.button === Qt.LeftButton) {
                    map.zoomLevel++
                } else if (mouse.button === Qt.RightButton) {
                    map.zoomLevel--
                }
            }
            */
        }
    }

    //create outside of the map, add at runtime
    MapQuickItem {
            id:mapMarker
            anchorPoint.x: 6
            anchorPoint.y: 6
            z: 100

        sourceItem: Rectangle {
            width: 12
            height: 12
            color: "#00000000"
            border.color: "yellow"
            border.width: 2

        }
    }


    //use thresholds and only break the line when the color changes
    function setTrack(segments){
        for (var i=1; i<segments.length; i++){
            var component = Qt.createComponent("mappolyline.qml");
            var p = [];
            var c = segments[i].c;
            //path must have at least 2 elements
            while(i < segments.length && (segments[i-1].c === segments[i].c || p.length < 2)){
                p.push({ latitude: segments[i-1].y, longitude: segments[i-1].x });
                p.push({ latitude: segments[i].y, longitude: segments[i].x });
                i++;
            }
            i--;
            track = component.createObject(map, {"line.width": 1, "line.color": c, "path": p});
            map.addMapItem(track);

        }
        if(segments.length > 0) {
            setCursor(segments[0].y,segments[0].x);
            map.addMapItem(mapMarker);
            map.fitViewportToMapItems();
        }
    }

    function clearMap(){
        map.clearMapItems();
    }

    function setCursor(lat, lon){
        mapMarker.coordinate = QtPositioning.coordinate(lat,lon);
        //this uses DMS
        //statusText.text = QtPositioning.coordinate(lat,lon).toString();
        //this uses DDM
        statusText.text = getDegreeDecimalMinutesFormat(lat, true) + "     " + getDegreeDecimalMinutesFormat(lon, false);
        if(lockCursor.checked) {
            map.center = QtPositioning.coordinate(lat,lon);
        }

    }




    function getDegreeDecimalMinutesFormat(decimalDeg, lat) {
       return Math.floor(Math.abs(decimalDeg)) + " " +  roundNumber(Math.abs(decimalDeg) % 1 * 60,5) + " " +
       (lat === true ? (decimalDeg > 0 ? "N" : "S") : (decimalDeg > 0 ? "E" : "W"));
    }

    function roundNumber(num, dec) {
         return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    }






}


