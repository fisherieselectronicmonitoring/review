#ifndef STILLIMAGEVIEW_H
#define STILLIMAGEVIEW_H

#include <QObject>
#include <QGraphicsView>

class StillImageView : public QGraphicsView
{
    Q_OBJECT
public:
  explicit StillImageView(QWidget *parent = 0);
  double currentScale;

protected:
    virtual void wheelEvent(QWheelEvent* event);


};

#endif // STILLIMAGEVIEW_H
