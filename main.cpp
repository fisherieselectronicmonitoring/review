#include "stackwalker.h"
#include <signal.h>
#include <QtCore/QCoreApplication>
#include <QtWidgets/QApplication>
#include <QtQuick/QQuickView>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QStandardPaths>
#include <QQmlContext>
#include <QSplashScreen>
#include <QResource>
#include <QThread>
#include <QApplication>
#include <QtDebug>
#include <QFile>
#include <QTextStream>
#include <QReadWriteLock>
#include <VLCQtCore/Common.h>

#include "reviewapplication.h"
#include "videowindow.h"
#include "timelinewindow.h"
#include "constants.h"
#include "mapconfig.h"
#include "measurementwindow.h"







QFile *logFile;
QReadWriteLock lock;

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{

    QString typeDesc;
    switch (type) {
    case QtDebugMsg:
        typeDesc = "DEBUG";
        break;
    case QtWarningMsg:
        typeDesc = "WARN";
    break;
    case QtCriticalMsg:
        typeDesc = "CRITICAL";
    break;
    case QtFatalMsg:
        typeDesc = "FATAL";
    break;
    }

    QDateTime now = QDateTime::currentDateTime();
    QString nowStr = now.toString("yyyy-MM-dd hh:mm:ss");

    QWriteLocker locker(&lock);
    QTextStream fileout(logFile);
    QTextStream sout(stdout, QIODevice::WriteOnly);
    QString s;
    if(context.file)
        s = QString("%1 %2(%3:%4:%5) %6").arg(nowStr).arg(typeDesc).arg(context.file).arg(context.function).arg(context.line).arg(msg);
    else
        s = QString("%1 %2 %3").arg(nowStr).arg(typeDesc).arg(msg);
    sout << s << endl;
    fileout << s << endl;
}





class StackWalkerAdptr : public StackWalker
{
public:
  StackWalkerAdptr() : StackWalker() {}
protected:
  virtual void OnOutput(LPCSTR szText)
    {     QWriteLocker locker(&lock);
          QTextStream fileout(logFile);
          fileout << szText;
          //StackWalker::OnOutput(szText);
    }
};



void abortHandler( int signum )
{
   // associate each signal with a signal name string.
   QString name;
   switch( signum )
   {
   case SIGABRT: name = "SIGABRT";  break;
   case SIGSEGV: name = "SIGSEGV";  break;
   case SIGILL:  name = "SIGILL";   break;
   case SIGFPE:  name = "SIGFPE";   break;
   }

   qDebug() << "FATAL - CAUGHT SIGNAL" << (name.isEmpty() ? QString::number(signum) : name);

   //stack trace
   StackWalkerAdptr sw;
   sw.ShowCallstack();


   exit( signum );
}








int main(int argc, char *argv[])
{
    //handle crashes
    signal( SIGABRT, abortHandler );
    signal( SIGSEGV, abortHandler );
    signal( SIGILL,  abortHandler );
    signal( SIGFPE,  abortHandler );


    QCoreApplication::setApplicationName("Review");

    ReviewApplication app(argc, argv);

     //open log file
    QDir dir("logs");
    if (!dir.exists())
        dir.mkpath(".");
     logFile = new QFile(QString("logs/review-%1.log").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd")));
     if(!logFile->open(QIODevice::WriteOnly | QIODevice::Append))
         return -1;

    qInstallMessageHandler(messageHandler);

    //read the config file, create config object
    rApp->settings = new QSettings(Constants::CONFIG_FILE, QSettings::IniFormat);

    qDebug() << "main config read";


    qDebug() << "starting code from changeset:" << BUILD_CHANGESET;


    //version and build strings
    QFile ver(":/version.txt");
    ver.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream verStream(&ver);
    rApp->version = verStream.readAll().trimmed();

    //QFile bld(":/build.txt");
    //bld.open(QIODevice::ReadOnly | QIODevice::Text);
    //QTextStream bldStream(&bld);
    //rApp->build = bldStream.readAll().trimmed();
    rApp->build = BUILD_CHANGESET;
    rApp->build.append("\n");
    rApp->build.append(__DATE__);
    rApp->build.append(" ");
    rApp->build.append(__TIME__);

    //splash screen
    QPixmap pixmap(":/images/SplashSmall.png");
    QSplashScreen *splash = new QSplashScreen(pixmap);
    //read the message file
    QFile splsh(":/splashmessage.txt");
    splsh.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream splshStream(&splsh);
    splash->setWindowFlags(splash->windowFlags() | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    splash->setFont(QFont("helvetica",10));
    splash->showMessage(splshStream.readAll().arg(rApp->version).arg(rApp->build), Qt::AlignLeft,Qt::white);
    //save for use in "about" menu
    rApp->splash = splash;


    //VLC plugin path
    VlcCommon::setPluginPath(app.applicationDirPath() + "/plugins");


    //default the emdata object
    rApp->emData = new EmData();

    //check the config file for the last used template, load that one
    //TODO move config keys to constants
    if(rApp->settings->contains("template_path"))
    {
        rApp->currentTemplate = rApp->settings->value("template_path").toString();
        QFile test(rApp->currentTemplate);
        //make sure that the file exists
        if(!test.exists())
            rApp->currentTemplate.clear();

    }


    //clear the map cache
    //TODO move to menu item
    QDir tileCache(QStandardPaths::locate(QStandardPaths::GenericCacheLocation,"QtLocation",QStandardPaths::LocateDirectory));
    qDebug() << "clearing map tile cache at:" << tileCache.absolutePath();
    if(tileCache.absolutePath().contains("QtLocation"))
        tileCache.removeRecursively();



    //load map
    QQmlEngine engine;
    MapConfig *m = new MapConfig();


    //m->host = "https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer/tile/{z}/{y}/{x}"; //rApp->settings->value("mapping_host").toString();
    m->host = rApp->settings->value("mapping_host").toString();
    m->imageFormat = rApp->settings->value("mapping_image_format").toString();
    engine.rootContext()->setContextProperty("mapConfig", m);

    qDebug() << "mapping host:" << m->host;

    QQmlComponent component(&engine, QUrl("qrc:///mapwindow.qml"));
    rApp->map = component.create();


    //Timeline is the master control
    rApp->timeline = new TimelineWindow();
    rApp->timeline->show();

    //show the splash screen for 3 seconds
    splash->show();
    rApp->splash->raise();
    Sleep(2500);
    rApp->splash->finish(rApp->timeline);

    //create a window for data tables
    rApp->dataTableTabs = new EventDataWindow();
    rApp->dataTableTabs->setVisible(false);

    rApp->tree = new TreeViewWindow();
    rApp->tree->setVisible(false);

    //create the measurement window, but don't show
    rApp->measurement = new MeasurementWindow();
    rApp->measurement->setVisible(false);

    //rApp->measurement->initialize("Z:/Work/NFWF EM Review/measurementTests/IMG_4906.png");



/*
    //if there is a path passed as the first argument, load and export all of the sql lite in that dir
    if(argc > 2)
    {
       //this will close the splash automatically
       splash->finish(rApp->timeline);

       qDebug() << "beginning batch export";

       //set the timplate to the first arg
       rApp->currentTemplate = argv[1];

       QString sourceTopDir = argv[2];

       QString expBaseDir = QDir(sourceTopDir).path();



       QList<QString> dbPaths;

       QDirIterator dirIt(sourceTopDir, QDirIterator::Subdirectories);
       while (dirIt.hasNext()) {
           dirIt.next();
           if (QFileInfo(dirIt.filePath()).isFile())
               if (QFileInfo(dirIt.filePath()).suffix() == "sqlite3")
               {
                   qDebug() << "located database" << dirIt.filePath();
                   dbPaths.append(dirIt.filePath());
               }

       }

       //make an export directory
       QString expDir = QString("%1/batch-data-export_%2")
               .arg(expBaseDir)
               .arg(QDateTime::currentDateTimeUtc().toString(Constants::FILE_DATE_TIME_FORMAT));

       QDir(expBaseDir).mkdir(expDir);

       qDebug() << "exporting to" << expDir;

       //foreach database file in the source dirs
       foreach(QString dbPath, dbPaths)
       {
           //load the EM data
           rApp->currentEmDataDirectory = QFileInfo(dbPath).path();
           rApp->timeline->openEmFilesSilent();

           //make a subdir in the export location
           QString sourceEmDataDir = QFileInfo(dbPath).dir().dirName();
           QString expSubDir = QString("%1/%2")
                   .arg(expDir)
                   .arg(sourceEmDataDir);
           QDir(expDir).mkdir(sourceEmDataDir);

           rApp->timeline->exportDataBatchMode(expSubDir);

       }

       return 0;

    }
*/

    qDebug() << "app setup complete";




    int ret = app.exec();

    logFile->close();

    return ret;


}



