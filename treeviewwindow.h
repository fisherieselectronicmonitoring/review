#ifndef TREEVIEWWINDOW_H
#define TREEVIEWWINDOW_H

#include <QMainWindow>
#include "dataentryeventdata.h"

namespace Ui {
class TreeViewWindow;
}

class TreeViewWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TreeViewWindow(QWidget *parent = 0);
    ~TreeViewWindow();
    static const int START_SECONDS = Qt::UserRole+3;
    void selectItem(DataEntryEventData *);

private:
    Ui::TreeViewWindow *ui;
    QList<QTreeWidgetItem *> items;
    void addChildren(QTreeWidgetItem *);
    void createItem(DataEntryEventData *);
    int getParent(DataEntryEventData *);
    QList<QTreeWidgetItem*> getChildren(QTreeWidgetItem *);
    const int STORAGE_ID = Qt::UserRole+1;
    const int PARENT_STORAGE_ID = Qt::UserRole+2;
    static bool chronologicalCompare(QTreeWidgetItem *, QTreeWidgetItem *);

private slots:
    void itemClicked(QTreeWidgetItem*, int);


public slots:
    void load();
    void updateRow(DataEntryEventData *);
    void deleteRow(DataEntryEventData *);


};

#endif // TREEVIEWWINDOW_H
