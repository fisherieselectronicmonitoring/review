#ifndef EMDATASENSORDURATIONEVENT_H
#define EMDATASENSORDURATIONEVENT_H

#include "emdatasensorevent.h"


class EmDataSensorDurationEvent : public EmDataSensorEvent
{
        Q_OBJECT
public:
    EmDataSensorDurationEvent();
    enum class DurationMarkerStyle
      {
         Shade,
         Line
      };
    double beginSeconds;
    double endSeconds;
    DurationMarkerStyle durationMarkerStyle;
    void setMarkerStyle();
};

#endif // EMDATASENSORDURATIONEVENT_H
