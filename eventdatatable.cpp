#include "eventdatatable.h"
#include "ui_eventdatatable.h"
#include "reviewapplication.h"

EventDataTable::EventDataTable(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EventDataTable)
{
    ui->setupUi(this);
    connect(ui->tblEvent, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(tableDblClick(int, int)));


    headersPrinted = false;

    ui->tblEvent->setRowCount(0);

    ui->tblEvent->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
    ui->tblEvent->horizontalHeader()->setMaximumSectionSize(400);
    ui->tblEvent->horizontalHeader()->setMinimumSectionSize(0);

    ui->tblEvent->setWordWrap(true);
    ui->tblEvent->setEditTriggers(QTableWidget::NoEditTriggers);
}

EventDataTable::~EventDataTable()
{
    delete ui;
}


void EventDataTable::tableDblClick(int row, int col)
{
    //qDebug() << row << col;
    QVariant dateTime = ui->tblEvent->item(row, col)->data(Qt::UserRole);
    if(dateTime.type() == QVariant::Type::DateTime)
        rApp->timeline->jumpToDateTime(dateTime.toDateTime());

    QVariant id = ui->tblEvent->item(row, col)->data(Qt::UserRole+1);
    if(id.type() == QVariant::Type::Int)
        rApp->timeline->selectDataEntryEvent(id.toInt());



}


void EventDataTable::printHeaders(QList<QString> h)
{


    ui->tblEvent->setColumnCount(h.count());
    //ui->tblEvent->setHorizontalHeaderLabels(h);

    for(int x = 0; x < h.count(); x++)
    {
        QString s = h.at(x);
        s = s.replace(" ","\n");
        QTableWidgetItem *hdr = new QTableWidgetItem(s);
        ui->tblEvent->setHorizontalHeaderItem(x, hdr);
    }
    headersPrinted = true;
}

void EventDataTable::load()
{
    //zero the table
    ui->tblEvent->setRowCount(0);

    //get the events that belong on this table
    foreach(DataEntryEventData *d, rApp->eventData)
        if(d->eventType == this->eventType)
            addRow(d);


    ui->tblEvent->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    ui->tblEvent->resizeRowsToContents();

    //sort by chronological order
    ui->tblEvent->sortByColumn(0, Qt::SortOrder::AscendingOrder);


}



void EventDataTable::addRow(DataEntryEventData *d)
{
    if(!headersPrinted)
        printHeaders(d->getHeaderList());

    ui->tblEvent->setSortingEnabled(false);

    //load variants directly
    ui->tblEvent->insertRow(ui->tblEvent->rowCount());
    QList<QVariant> r = d->toVariantList();
    qDebug() << r;
    for(int x = 0; x < r.count(); x++)
    {
         QTableWidgetItem *itm = new QTableWidgetItem(r.at(x).toString());
         itm->setData(Qt::UserRole, r.at(x));
         itm->setData(Qt::UserRole+1, d->storageId);
         ui->tblEvent->setItem(ui->tblEvent->rowCount()-1, x, itm);

    }

    ui->tblEvent->setSortingEnabled(true);

}




void EventDataTable::selectRow(DataEntryEventData *d)
{
    for(int z = 0; z < ui->tblEvent->rowCount(); z++)
    {
        QTableWidgetItem *i = ui->tblEvent->item(z,0);
        if(i && i->data(Qt::UserRole+1).toInt() == d->storageId) //check the user data in column 0
        {
            ui->tblEvent->selectRow(z);
            break;
        }
    }
}




void EventDataTable::updateRow(DataEntryEventData *d)
{
    ui->tblEvent->setSortingEnabled(false);

    for(int z = 0; z < ui->tblEvent->rowCount(); z++)
    {
        //check the user data in column 0
        QTableWidgetItem *i = ui->tblEvent->item(z,0);
        if(i && i->data(Qt::UserRole+1).toInt() == d->storageId)
        {
            QList<QVariant> r = d->toVariantList();
            for(int x = 0; x < r.count(); x++)
            {
                 QTableWidgetItem *itm = new QTableWidgetItem(r.at(x).toString());
                 itm->setData(Qt::UserRole, r.at(x));
                 itm->setData(Qt::UserRole+1, d->storageId);
                 ui->tblEvent->setItem(z, x, itm);
            }
            break;
        }
    }

    ui->tblEvent->setSortingEnabled(true);
}




void EventDataTable::deleteRow(DataEntryEventData *d)
{
    ui->tblEvent->setSortingEnabled(false);

    for(int z = ui->tblEvent->rowCount(); z >= 0; z--)
    {
        QTableWidgetItem *i = ui->tblEvent->item(z,0);

        if(i && i->data(Qt::UserRole+1).toInt() == d->storageId)
        {
            ui->tblEvent->removeRow(z);
            break;
        }
    }
    ui->tblEvent->setSortingEnabled(true);
}









void EventDataTable::keyPressEvent(QKeyEvent *event)
{
   //if there is a control-C event copy data to the global clipboard
   if(event->key() == Qt::Key_C && event->modifiers() & Qt::ControlModifier)
   {
       QAbstractItemModel *abmodel = ui->tblEvent->model();
       QItemSelectionModel *model = ui->tblEvent->selectionModel();
       QModelIndexList list = model->selectedIndexes();

       //qSort(list);

       if(list.size() < 1)
           return;

       QString copy_table;
       QModelIndex previous = list.first();

       list.removeFirst();

       for(int i = 0; i < list.size(); i++)
       {
           QVariant data = abmodel->data(previous);
           QString text = data.toString();

           QModelIndex index = list.at(i);
           copy_table.append(text);

           if(index.row() != previous.row())

           {
               copy_table.append('\n');
           }
           else
           {
               copy_table.append('\t');
           }
           previous = index;
       }

       copy_table.append(abmodel->data(list.last()).toString());
       copy_table.append('\n');

       QClipboard *clipboard = QApplication::clipboard();
       clipboard->setText(copy_table);
   }
}

