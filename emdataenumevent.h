#ifndef EMDATAENUMEVENT_H
#define EMDATAENUMEVENT_H

#include <QObject>
#include <QColor>
#include <QString>

#include "qcustomplot.h"

class EmDataEnumEvent : public QObject
{
        Q_OBJECT
public:
    enum class Symbol
      {
         Dimond,
         Square,
         Triangle,
         TriangleInverted,
         Circle,
         Disc
      };


    EmDataEnumEvent();

    double seconds;
    Symbol symbol;
    QColor color;
    QString text;
    float position;
    float size;
    QCPGraph* marker;
    QString getSummary();
};

#endif // EMDATAENUMEVENT_H
