#include "reviewapplication.h"
#include "dataentryeventdata.h"
#include "dataentrydialog.h"
#include "ui_dataentrydialog.h"
#include "databaseutility.h"

#include <QLabel>
#include <QComboBox>
#include <QCloseEvent>
#include <QDebug>
#include <QLineEdit>
#include <QListWidget>
#include <QListWidgetItem>
#include <QAbstractButton>
#include <QPushButton>
#include <QDateTimeEdit>

DataEntryDialog::DataEntryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DataEntryDialog)
{
    ui->setupUi(this);
    setModal(true);
    foreach(QAbstractButton *button,  ui->buttonBox->buttons())
        qobject_cast<QPushButton *>(button)->setAutoDefault(false);

    lastEntrySaved = true;

    scrollAreaWidget = new QWidget();
    scrollAreaWidget->setLayout(new QFormLayout());
    ui->scrollArea->setWidget(scrollAreaWidget);



}

DataEntryDialog::~DataEntryDialog()
{
    delete ui;
}


//TODO move to widget wrapper?
void DataEntryDialog::addMultiSelectControl(int id, QString name, QString description, bool required, float order, bool sticky, QList<QPair<QString, QString>> values, QList<QString> defaultKeys)
{
    DataEntryWidgetWrapper *wrapper = new DataEntryWidgetWrapper();
    wrapper->id = id;
    wrapper->name = name;
    wrapper->description = description;
    wrapper->required = required;
    wrapper->order = order;
    wrapper->sticky = sticky;
    QListWidget *w = new QListWidget();
    wrapper->widget = w;
    wrapper->widgetType = DataEntryWidgetWrapper::WidgetType::MultiSelect;

    QPair<QString,QString> p;
    foreach (p, values)
    {
        QListWidgetItem* item = new QListWidgetItem();
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setData(Qt::UserRole, p.first);
        item->setText(p.second);
        w->addItem(item);
    }

    foreach(QString s, defaultKeys)
        wrapper->defaultIds.append(s);

    addWidget(wrapper);

}


//TODO move to widget wrapper?
void DataEntryDialog::addSelectControl(int id, QString name, QString description, bool required, float order, bool sticky, QList<QPair<QString, QString>> values, QString defaultKey)
{

    //TODO constructor
    DataEntryWidgetWrapper *wrapper = new DataEntryWidgetWrapper();
    wrapper->id = id;
    wrapper->name = name;
    wrapper->description = description;
    wrapper->required = required;
    wrapper->order = order;
    wrapper->sticky = sticky;
    QComboBox *w = new QComboBox();
    wrapper->widget = w;
    wrapper->widgetType = DataEntryWidgetWrapper::WidgetType::Select;
    QPair<QString,QString> p;
    foreach (p, values)
        w->addItem(p.second, p.first);

    //use this to add an "unset" option
    //if(!wrapper->required)
    //    w->insertItem(-1,"","");

    wrapper->defaultIds.append(defaultKey);

    w->setAutoCompletion(true);

    addWidget(wrapper);
/*

    DataEntryWidgetWrapper *wrapper = new DataEntryWidgetWrapper();
    wrapper->id = id;
    wrapper->name = name;
    wrapper->required = required;
    wrapper->order = order;
    wrapper->sticky = sticky;
    QComboBox *w = new QComboBox();
    wrapper->widget = w;
    wrapper->widgetType = DataEntryWidgetWrapper::WidgetType::Select;
    wrapper->defaultIds.append(defaultKey);

    QStringListModel *model = new QStringListModel();
    QPair<QString,QString> p;
    foreach (p, values)
    {
        w->addItem(p.second, p.first);
        model->insertRow(model->rowCount());
        model->setData(model->index(model->rowCount() - 1, 0), p.second, Qt::DisplayRole);
    }

    //setup completer
    w->setEditable(true);
    QCompleter *c = new QCompleter(model);
    c->setMaxVisibleItems(10);
    c->setCompletionMode(QCompleter::PopupCompletion);
    c->setWrapAround(true);
    c->setCompletionRole(Qt::EditRole);
    w->setCompleter(c);
    connect(c, static_cast<void(QCompleter::*)(const QString &)>(&QCompleter::activated),
        [=](const QString &text){ w->setCurrentText(text); });
    addWidget(wrapper);
*/

  /*
    this could be bound to a hidden text edit field
    QSortFilterProxyModel * proxy = new QSortFilterProxyModel;
    proxy->setSourceModel(model);

    w->setModel(proxy);
    w->setEditable(true);
    w->setForc
    w->setCompleter(0);

    // When the edit text changes, use it to filter the proxy model.
    connect(w, SIGNAL(currentTextChanged(QString)), proxy, SLOT(setFilterWildcard(QString)));

 */

}




//TODO move to widget wrapper?
void DataEntryDialog::addTextControl(int id, QString name, QString description, bool required, float order, bool sticky, int maxLength, QString defaultValue, int autoIncrement, QValidator *validator)
{

    //TODO constructor
    DataEntryWidgetWrapper *wrapper = new DataEntryWidgetWrapper();
    wrapper->id = id;
    wrapper->name = name;
    wrapper->description = description;
    wrapper->required = required;
    wrapper->order = order;
    wrapper->sticky = sticky;
    wrapper->autoIncrement = autoIncrement;
    wrapper->defaultValue = defaultValue;
    QLineEdit *w = new QLineEdit();
    if(maxLength > 0)
        w->setMaxLength(maxLength);
    wrapper->widget = w;
    wrapper->widgetType = DataEntryWidgetWrapper::WidgetType::Text;
    if(validator)
        w->setValidator(validator);
    addWidget(wrapper);


}


void DataEntryDialog::addTextBoxControl(int id, QString name, QString description, bool required, float order, bool sticky, QString defaultValue, int autoIncrement, QValidator *validator)
{

    //TODO constructor
    DataEntryWidgetWrapper *wrapper = new DataEntryWidgetWrapper();
    wrapper->id = id;
    wrapper->name = name;
    wrapper->description = description;
    wrapper->required = required;
    wrapper->order = order;
    wrapper->sticky = sticky;
    wrapper->autoIncrement = autoIncrement;
    wrapper->defaultValue = defaultValue;
    QTextEdit *w = new QTextEdit();
    w->setAcceptRichText(false);
    w->setTabChangesFocus(true);
    wrapper->widget = w;
    wrapper->widgetType = DataEntryWidgetWrapper::WidgetType::TextBox;
    //if(validator)
    //    w->setValidator(validator);
    addWidget(wrapper);


}


//TODO move to widget wrapper?
void DataEntryDialog::addDateTimeControl(int id, QString name, QString description, bool required, float order, bool sticky)
{

    //TODO constructor
    DataEntryWidgetWrapper *wrapper = new DataEntryWidgetWrapper();
    wrapper->id = id;
    wrapper->name = name;
    wrapper->description = description;
    wrapper->required = required;
    wrapper->order = order;
    wrapper->sticky = sticky;
    QDateTimeEdit *w = new QDateTimeEdit();
    w->setDisplayFormat("MM/dd/yyyy hh:mm:ss Z");
    wrapper->widget = w;
    wrapper->widgetType = DataEntryWidgetWrapper::WidgetType::DateTime;
    addWidget(wrapper);


}


void DataEntryDialog::initialize(DataEntryEventData *e, Mode m, bool autoConfim, QList<DataEntryFieldValue> hotkeyPresets)
{
    currentEventData = e;
    mode = m;

    foreach(DataEntryWidgetWrapper *w, controls)
        w->setDefaults();



    //for each control look for the data in the eventData object
    if(mode == Mode::Update)
        foreach(DataEntryWidgetWrapper *w, controls)
            w->setControlValues(currentEventData->data.value(w->id));
    else if (mode == Mode::Create && lastEntrySaved) //set autoincrement only when previous entry was saved
        foreach(DataEntryWidgetWrapper *w, controls)
            w->setAutoIncrement();


    //set hotkey presets if they are present
    foreach(DataEntryFieldValue fv, hotkeyPresets)
    {
        /*
         MultiSelect,
         Select,
         Text,
         DateTime,
         TextBox
         */
        //TODO check that different ordering of fields doesn't break this
        DataEntryWidgetWrapper *w;
        foreach(DataEntryWidgetWrapper *wx, controls)
            if(wx->id == fv.key)
            {
                w = wx;
                break;
            }
        if(w != NULL) //we found a control with this id
        {
            QVariant var = QVariant();
            if(w->widgetType == DataEntryWidgetWrapper::WidgetType::Select ||
               w->widgetType == DataEntryWidgetWrapper::WidgetType::Text ||
               w->widgetType == DataEntryWidgetWrapper::WidgetType::TextBox)
                var.setValue(fv.value); //will be a string key in the select, and text fields can be set directly
            else if(w->widgetType == DataEntryWidgetWrapper::WidgetType::MultiSelect)
            {
                //TODO parse out array
                QStringList rawValues = fv.value.mid(fv.value.indexOf("[") + 1,fv.value.indexOf("]")-1).split(",");
                qDebug() << fv.value << rawValues;
                var.setValue(rawValues);
            }

            if(!var.isNull())
            {
                qDebug() << "setting hotkey preset of " + var.toString() + " for field " + w->name;
                w->setControlValues(var);
            }
            else
                 qDebug() << "unable to set hotkey preset for field " + w->name;
        }
    }


    bool autoConfirmOk = false;
    if(autoConfim) //only works with no-attribute events or ones with defaults
        autoConfirmOk = acceptDialog();


    if(!autoConfirmOk) //if we don't want autoconfirm, or it failed
    {
        show();
        if(controls.size() > 0)
            controls.at(0)->widget->setFocus();

    }
}

void DataEntryDialog::setControlValue(int id, QVariant value)
{
    foreach(DataEntryWidgetWrapper *w, controls)
       if(w->id == id){
           w->setControlValues(value);
           w->fixup();
       }

}


void DataEntryDialog::generateForm()
{
    qSort(controls.begin(), controls.end(), DataEntryWidgetWrapper::compare);

    foreach(DataEntryWidgetWrapper *wrapper, controls)
    {   QLabel* l = new QLabel(wrapper->name);
        if(!wrapper->description.isEmpty())
            l->setToolTip(wrapper->description);
        scrollAreaWidget->layout()->addWidget(l);
        scrollAreaWidget->layout()->addWidget(wrapper->widget);
    }
/*
    int maxWindowHeight = rApp->primaryScreen()->geometry().height() - (0.2 * rApp->primaryScreen()->geometry().height());
    this->resize(this->width(), this->sizeHint().height());
    if(this->height() > maxWindowHeight)
        this->resize(this->width(), maxWindowHeight);
*/

}


void DataEntryDialog::addWidget(DataEntryWidgetWrapper *wrapper)
{

    controls.append(wrapper);
    if(wrapper->required)
        requiredControls.append(wrapper);
}


void DataEntryDialog::closeEvent (QCloseEvent* event)
{
    reject();
}

void DataEntryDialog::accept()
{
    acceptDialog();
}

bool DataEntryDialog::acceptDialog()
{
    bool validationErrors = false;
    foreach(DataEntryWidgetWrapper *w, requiredControls)
       if(!w->validate())
           validationErrors = true;

    if(validationErrors)
        return false;

    foreach(DataEntryWidgetWrapper *w, controls)
        w->getControlValues(currentEventData);


    if(mode == Mode::Create)
    {
        try
        {
            currentEventData->storageId = DatabaseUtility::store(currentEventData->toInternalJson());
        }
        catch (const std::exception &exc)
        {
            QMessageBox messageBox;
            messageBox.critical(0,"Error",QString("An error occured saving this event: %1").arg(QString(exc.what())));
            messageBox.show();
            return false;
        }
        currentEventData->setMarkerStyle();
        rApp->eventData.append(currentEventData);
        rApp->tables.value(currentEventData->eventType->id)->addRow(currentEventData);
        rApp->tree->load();
    }
    else
    {
        currentEventData->createdByAutomation = false;
        try
        {
            DatabaseUtility::update(currentEventData->storageId, currentEventData->toInternalJson());
        }
        catch (const std::exception &exc)
        {
            QMessageBox messageBox;
            messageBox.critical(0,"Error",QString("An error occured saving this event: %1").arg(QString(exc.what())));
            messageBox.show();
            return false;
        }
        rApp->tables.value(currentEventData->eventType->id)->updateRow(currentEventData);

    }
    QDialog::accept();
    lastEntrySaved = true;
    toggleApplicationShortcuts(true);

    return true;

}

void DataEntryDialog::reject()
{
    if(mode == Mode::Create)
        rApp->timeline->deleteMarker(currentEventData->marker);
    QDialog::reject();
    lastEntrySaved = false;
    toggleApplicationShortcuts(true);
}

void DataEntryDialog::show()
{
   toggleApplicationShortcuts(false);
   QDialog::show();
}

void DataEntryDialog::toggleApplicationShortcuts(bool on)
{
    foreach(QShortcut *q, rApp->applicationShortcuts)
        q->setEnabled(on);
}

/*
void DataEntryDialog::keyPressEvent(QKeyEvent *evt)
{
    evt->accept();
}
*/
