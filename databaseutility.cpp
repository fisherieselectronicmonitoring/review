#include "databaseutility.h"
#include "reviewapplication.h"

#include <QSqlQuery>
#include <QSqlError>

QSqlDatabase DatabaseUtility::db;
QReadWriteLock DatabaseUtility::lock;

DatabaseUtility::DatabaseUtility(QObject *parent) : QObject(parent)
{

}


void DatabaseUtility::openDatabase()
{

        if(!db.isOpen())
        {
            if(!db.open())
            {
                QSqlError er = db.lastError();
                qDebug() << "failure opening database" << er.text();
                throw std::exception("database could not be opened");
            }
        }

    //qDebug() << "opened db: " << rApp->currentDatabaseFile;
}



void DatabaseUtility::initializeDatabase()
{
    QWriteLocker locker(&lock);

    if(db.isValid()){
        QString connection = db.connectionName();
        db.close();
        db = QSqlDatabase();
        db.removeDatabase(connection);
        qDebug() << "removed existing db connection";

    }

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(rApp->currentDatabaseFile);
    qDebug() << "initialized db: " << rApp->currentDatabaseFile;


    openDatabase();

    //if the database is new, create the object storage table and the metadata table
    if (!db.tables().contains("event_data"))
    {
        QSqlQuery query1;
        query1.exec("create table event_data (id integer primary key, event_json text)");
        qDebug() << "created event_data table in db: " << rApp->currentDatabaseFile;

        QSqlQuery query2;
        query2.exec("create table metadata (key text primary key, value text)");
        qDebug() << "created metadata table in db: " << rApp->currentDatabaseFile;

    }

    closeDatabase();
}

int DatabaseUtility::store(QJsonDocument json)
{
    QWriteLocker locker(&lock);
    openDatabase();

    QSqlQuery query;
    query.prepare("insert into event_data (event_json) values (:json)");
    query.bindValue(":json", json.toJson(QJsonDocument::JsonFormat::Compact));

    if(!query.exec())
    {
        QSqlError er = query.lastError();
        qDebug() << "failure inserting to database" << er.text();
        closeDatabase();
        throw std::exception("database insert failed");
    }


    int id = query.lastInsertId().toInt();
    closeDatabase();

    return id;
}


QList<int> DatabaseUtility::batchStore(QList<QJsonDocument> jsonDocs)
{
    QWriteLocker locker(&lock);
    openDatabase();
    db.transaction();
    QList<int> ids;
    foreach(QJsonDocument json, jsonDocs)
    {
        QSqlQuery query;
        query.prepare("insert into event_data (event_json) values (:json)");
        query.bindValue(":json", json.toJson(QJsonDocument::JsonFormat::Compact));
        query.exec();
        ids.append(query.lastInsertId().toInt());
    }

    db.commit();
    closeDatabase();

    return ids;
}

void DatabaseUtility::batchRemove(QList<int> ids)
{
    QWriteLocker locker(&lock);
    openDatabase();
    db.transaction();
    foreach(int id, ids)
    {
        QSqlQuery query;
        query.prepare("delete from event_data where id = :id");
        query.bindValue(":id", id);
        query.exec();
    }
    db.commit();
    closeDatabase();
}


void DatabaseUtility::storeMetadata(QString key, QString value, bool overwrite)
{
    QWriteLocker locker(&lock);
    openDatabase();

    QSqlQuery query1;
    query1.setForwardOnly(true);
    query1.prepare("select 1 from metadata where key = :key");
    query1.bindValue(":key", key );
    query1.exec();
    if(!query1.next())
    {
        QSqlQuery query2;
        query2.prepare("insert into metadata (key, value) values (:key,:value)");
        query2.bindValue(":key", key );
        query2.bindValue(":value", value );
        query2.exec();
    }
    else if(overwrite) //key exists, but we will overwrite
    {
        QSqlQuery query3;
        query3.prepare("update metadata set value = :value where key = :key ");
        query3.bindValue(":key", key );
        query3.bindValue(":value", value );
        query3.exec();
    }

    closeDatabase();
}




QString DatabaseUtility::retrieveMetadata(QString key)
{
    QWriteLocker locker(&lock);
    openDatabase();

    QSqlQuery query;
    query.setForwardOnly(true);
    query.prepare("select value from metadata where key = :key");
    query.bindValue(":key", key );
    query.exec();
    QString val;
    if(query.first())
        val = query.value(0).toString();
    else
        val = "";

    closeDatabase();

    return val;
}



void DatabaseUtility::update(int id, QJsonDocument json)
{
    QWriteLocker locker(&lock);
    openDatabase();

    QSqlQuery query;
    query.prepare("update event_data set event_json = :json where id = :id");
    query.bindValue(":id", id);
    query.bindValue(":json", json.toJson(QJsonDocument::JsonFormat::Compact));

    if(!query.exec())
    {
        QSqlError er = query.lastError();
        qDebug() << "failure updating database" << er.text();
        closeDatabase();
        throw std::exception("database update failed");
    }

    closeDatabase();
}

void DatabaseUtility::remove(int id)
{
    QWriteLocker locker(&lock);
    openDatabase();

    QSqlQuery query;
    query.prepare("delete from event_data where id = :id");
    query.bindValue(":id", id);
    query.exec();

    closeDatabase();
}

void DatabaseUtility::removeAll()
{
    QWriteLocker locker(&lock);
    openDatabase();

    QSqlQuery query;
    query.prepare("delete from event_data");
    query.exec();

    closeDatabase();
}



void DatabaseUtility::closeDatabase()
{
    if(db.isOpen())
        db.close();
}

QMap<int, QJsonDocument> DatabaseUtility::retrieveAll()
{

    QWriteLocker locker(&lock);
    openDatabase();

    QMap<int, QJsonDocument> json;
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec("select id, event_json from event_data");
    while(query.next())
        json.insert(query.value(0).toInt(), QJsonDocument::fromJson(query.value(1).toByteArray()));
    closeDatabase();

    return json;
}

/*
QMap<int, QJsonDocument> DatabaseUtility::retrieveAllFromFile(QString dbFile)
{
    QSqlDatabase dbb;

    if(dbb.isValid()){
        QString connection = dbb.connectionName();
        dbb.close();
        dbb = QSqlDatabase();
        dbb.removeDatabase(connection);
    }

    dbb = QSqlDatabase::addDatabase("QSQLITE");
    dbb.setDatabaseName(dbFile);
    dbb.open();
    QMap<int, QJsonDocument> json;
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec("select id, event_json from event_data");
    while(query.next())
        json.insert(query.value(0).toInt(), QJsonDocument::fromJson(query.value(1).toByteArray()));
    dbb.close();
    return json;
}
*/
