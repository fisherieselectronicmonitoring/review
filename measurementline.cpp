#include "measurementline.h"
#include "measurementview.h"
#include "reviewapplication.h"

#include <QGraphicsScene>


MeasurementLine::MeasurementLine(QObject *parent) : QObject(parent)
{
  lineLengthCm = 0;

}



void MeasurementLine::update()
{
    //we check to see if there is an actual change before updating position or labels
    //so that we don't get spurious scene change signals
    foreach(QGraphicsLineItem* ll, lines)
    {
        QPointF p1 = points[lines.indexOf(ll)]->scenePos();
        QPointF p2 = points[lines.indexOf(ll)+1]->scenePos();
        //only redraw if there is a change
        if(ll->line().p1() != p1 || ll->line().p2() != p2)
            ll->setLine(p1.x(),p1.y(),p2.x(),p2.y());

    }

    if(points.length() > 1)
    {
        //put the label in between the points
        double x = 0, y = 0;
        foreach(QGraphicsEllipseItem* e, points)
        {
            x += e->scenePos().x();
            y += e->scenePos().y();
        }

        QPointF newPos(x/points.length() - label->sceneBoundingRect().width()/2,
                       y/points.length() - label->sceneBoundingRect().height()/2);

        if(label->scenePos() != newPos)
            label->setPos(newPos);

        if(!label->isVisible())
            label->show();
    }
    else
        if(label->isVisible())
            label->hide();

    if(lineLengthCm > 0) {
        //TODO constants for length
        QString newLabel = QString("%1 cm").arg(QString::number(lineLengthCm,'f',rApp->measurementDecimals));
        //only update if there is actually a change
        if(newLabel != label->toPlainText())
            label->setPlainText(newLabel);
    }




}


void MeasurementLine::setVisibility(bool visible)
{
    foreach(QGraphicsEllipseItem* e, points)
        e->setVisible(visible);

    foreach(QGraphicsLineItem* e, lines)
        e->setVisible(visible);

    label->setVisible(visible);

}


void MeasurementLine::removeFromScene(QGraphicsScene* s)
{
    foreach(QGraphicsEllipseItem* e, points)
    {
        s->removeItem(e);
        delete e;
    }
    foreach(QGraphicsLineItem* e, lines)
    {
        s->removeItem(e);
        delete e;
    }
    s->removeItem(label);
    delete label;
}


double MeasurementLine::lineLengthPx()
{
    double length = 0;
    foreach(QGraphicsLineItem* ll, lines)
        length += ll->line().length();
    return length;

}


