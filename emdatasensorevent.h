#ifndef EMDATASENSOREVENT_H
#define EMDATASENSOREVENT_H

#include "qcustomplot.h"
#include <QObject>

class EmDataSensorEvent : public QObject
{
    Q_OBJECT
public:
    explicit EmDataSensorEvent(QObject *parent = 0);
    QColor color;
    QString name;
    QMap<QString, QVariant> displayData;
    QMap<QString, double> relatedEventOffsets;
    double value;
    QCPAbstractItem *marker;
    QString getSummary();
    virtual void setMarkerStyle();
signals:

public slots:
};

#endif // EMDATASENSOREVENT_H
